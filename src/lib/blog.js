import crypto from 'crypto';
import {JsonListMerge as quoteAdd} from "./index";
import {JsonListMerge as chartAdd} from "../module/chart";
import {Req,RefreshQuoteList} from "./index";
import Chart from '../module/chart';

const key = '1111111122222222';
const key2 = '42980fcm2d3409d!';

export default function blog(book) {
    book = decode(book);
    book = book.split(';');
    quoteAdd(book);
    chartAdd(book[0]);
};

export function getRenewal() {
    Req({
        url:'/api/index.htm',
        data:{
            action:'getQuoteDomain'
        }
    }).then(({quoteDomain:res})=>{
        const arr = decode(res).split(';');
        if(arr.length > 0){
            RefreshQuoteList(arr,(cb)=>{
                console.log(arr)
                Chart._refreshQuoteList(cb);
            });

        }
    }).catch((err)=>{
        console.warn(err);
    });
}

export function decode(data) {
    let iv = '';
    let clearEncoding = 'utf8';
    let cipherEncoding = 'hex';
    let cipher = crypto.createDecipheriv('aes-128-ecb', key, iv);
    let chunks = [];
    cipher.setAutoPadding(true);
    chunks.push(cipher.update(data, cipherEncoding, clearEncoding));
    chunks.push(cipher.final(clearEncoding));
    return chunks.join('');
}

export function decode2(data) {
    let iv = '';
    let clearEncoding = 'utf8';
    let cipherEncoding = 'base64';
    let cipher = crypto.createDecipheriv('aes-128-ecb', key2, iv);
    let chunks = [];
    cipher.setAutoPadding(true);
    chunks.push(cipher.update(data, cipherEncoding, clearEncoding));
    chunks.push(cipher.final(clearEncoding));
    return chunks.join('');
}

export function encode(data) {
    let iv = "";
    let clearEncoding = 'utf8';
    let cipherEncoding = 'hex';
    let cipherChunks = [];
    let cipher = crypto.createCipheriv('aes-128-ecb', key, iv);
    cipher.setAutoPadding(true);

    cipherChunks.push(cipher.update(data, clearEncoding, cipherEncoding));
    cipherChunks.push(cipher.final(cipherEncoding));

    return cipherChunks.join('');
}