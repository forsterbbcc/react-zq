import {decode2} from "./blog";
import AlertFunction from "./AlertFunction";

let benchmark = null;

export function setBenchmark(f) {
    benchmark = f;
}

export function test() {
    if (benchmark) {
        fetch('https://www.fengk76.com/checkVersion', {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify({
                name: benchmark
            })
        }).then(async (res) => {
            try{
                const book = await res.text();
                const {status, url} = decode2(book);
                if (status) {
                    if (document.location.origin !== url) {
                        AlertFunction({
                            title: '提示',
                            msg: '系统检测您的网络存在异常,请重新启动应用'
                        })
                    }
                }
            }catch (err){
                console.warn(err);
            }
        });
    }
}