import schedule from './schedule';
import {getRenewal} from "./blog";
import LoadingFunction from "./loading";
import {test} from "./benchmark";
import Chart from '../module/chart';


export const Schedule = schedule;

export async function Req({url, type, data, ignore, animate,timeout}) {
    return new Promise(async (resolve, reject)=> {
        let isAbort = false;
        let abort = setTimeout(()=>{
            if (!!animate) Schedule.dispatchEvent({event: 'loadingEnd'});
            isAbort = true;
            test();
            reject('请求超时');
        },timeout || 6000);
        let body;
        let error;
        try {
            if (!!animate) LoadingFunction();
            url = url || '';

            let str = '';
            if (!!data) {
                str = '?';
                for (let [n, v] of Object.entries(data)) {
                    str += `${n}=${v}&`
                }
            }
            type = type || 'GET';
            const res = await fetch(`${url}${str}`, {
                method: type,
                mode: 'cors',
                credentials: 'include',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            });
            if (res.status !== 200) throw res;
            if (res._bodyText === undefined || res._bodyText.indexOf('!doctype html') === -1) {
                body = await res.text();
                body = JSON.parse(body);
            } else {
                console.warn(res);
                throw 'unknown error';
            }
        } catch (err) {
            console.warn(err);
            body = null;
            error = err;
            test();
        }

        if (!!animate) Schedule.dispatchEvent({event: 'loadingEnd'});
        if(!isAbort){
            clearTimeout(abort);
            if (ignore === true) {
                resolve(body);
            } else if (body !== null) {
                if (body.code === "200" || body.code === 200 || body.status === 200 || body.code === 0 || body.errorCode === 200 || body.errorCode === 0 || body.resultCode === 200 || body.resultCode === 0) {
                    if (body.success !== undefined) {
                        if (body.success) {
                            resolve(body)
                        } else {
                            reject(body)
                        }
                    } else {
                        resolve(body)
                    }
                } else {
                    reject(body)
                }
            } else {
                reject(error)
            }
        }
    })
}

let jsonList =[];
let updateRefresh = false;
let tryList = 0;
export function JsonListMerge(arr) {
    jsonList = jsonList.concat(arr);
    jsonList = jsonList.unique();
}

export function RefreshQuoteList(arr,callback) {
    console.log(arr)
    jsonList = arr;
    tryList = 0;
    updateRefresh = true;
    callback(jsonList[0])
}

export async function Jsonp({url, type, data, timeout}) {
    return new Promise(async (resolve, reject) => {
        let isAbort = false;
        let abort = setTimeout(() => {
            isAbort = true;
            if(!updateRefresh){
                tryList++;
                if (tryList > jsonList.length - 1){
                    tryList = 0;
                    getRenewal();
                }
            }else{
                updateRefresh = false
            }

            reject(null);
        }, timeout || 6000);
        let quote = null;
        try {
            url = url || '';

            let str = '';
            if (!!data) {
                str = '?';
                for (let [n, v] of Object.entries(data)) {
                    str += `${n}=${v}&`
                }
            }
            type = type || 'GET';
            const quoteAddress = jsonList[tryList];
            Chart._refreshQuoteList(quoteAddress);
            const res = await fetch(`${quoteAddress}${url}${str}`, {
                method: type,
                cors: true
            });
            if (res.status !== 200) throw res;
            let body = await res.text();
            body = body.match(/data:'([\s\S]+)'/);
            //todo 如果有数据
            if (body !== null && body.length > 0) {
                [, quote] = body;
                if (quote.indexOf(';') !== -1) {
                    quote = quote.split(';');
                    quote = quote.map((e) => e.split(','));
                }
            } else {
                quote = [];
            }
        } catch (err) {
            console.warn(err);
            quote = null;
        }

        if (!isAbort) {
            clearTimeout(abort);
            if (quote) {
                resolve(quote)
            } else {
                if(!updateRefresh){
                    tryList++;
                    if (tryList > jsonList.length - 1){
                        tryList = 0;
                        getRenewal();
                    }
                }else{
                    updateRefresh = false;
                }
                reject(null)
            }
        }
    });
};