import {Schedule} from "./index";
//todo 银行卡图片
import ICBC from '../images/bankIcon/icbc.png'
import CMB from '../images/bankIcon/cmb.png'
import CCB from '../images/bankIcon/ccb.png'
import ABC from '../images/bankIcon/abc.png'
import BOC from '../images/bankIcon/boc.png'
import COMM from '../images/bankIcon/comm.png'
import CMBC from '../images/bankIcon/cmbc.png'
import SPDB from '../images/bankIcon/spdb.png'
import CITIC from '../images/bankIcon/citic.png'
import GDB from '../images/bankIcon/gdb.png'
import SZPAB from '../images/bankIcon/szpab.png'
import CIB from '../images/bankIcon/cib.png'
import HXB from '../images/bankIcon/hxb.png'
import CEB from '../images/bankIcon/ceb.png'
import PSBC from '../images/bankIcon/psbc.png'

export function getCloseTime(arr) {
    if (arr.length === 1) {
        return arr[0]
    } else {
        let o = new Date().getTime();
        return arr.find((e) => {
            return o < e;
        })
    }
}

export function formatDate(format, {date, isUTC}) {
    if (!format)
        return null;

    if (!date) {
        date = new Date();
    } else {
        date = new Date(date);
    }

    let y, m, d, h, i, s;

    if (isUTC) {
        y = date.getFullYear();
        m = completeNum(date.getUTCMonth() + 1);
        d = completeNum(date.getUTCDate());
        h = completeNum(date.getUTCHours());
        i = completeNum(date.getUTCMinutes());
        s = completeNum(date.getUTCSeconds());
    } else {
        y = date.getFullYear();
        m = completeNum(date.getMonth() + 1);
        d = completeNum(date.getDate());
        h = completeNum(date.getHours());
        i = completeNum(date.getMinutes());
        s = completeNum(date.getSeconds());
    }

    return format.replace('y', y).replace('m', m).replace('d', d).replace('h', h).replace('i', i).replace('s', s);
}

export function completeNum(num) {
    return num < 10 ? "0" + num : num;
}

export function getIdentity(len) {
    let SEED = '0Aa1Bb2Cc3Dd4Ee5Ff6Gg7Hh8Ii9Jj0Kk1Ll2Mm3Nn4Oo5Pp6Qq7Rr8Ss9Tt0Uu1Vv2Ww3Xx4Yy5Zz6789'.split('');
    let SIZE = SEED.length;
    let LEN = 20;
    if (!len || typeof len !== 'number') {
        len = LEN
    }

    let uid = '';
    while (len-- > 0) {
        uid += SEED[Math.random() * SIZE | 0]
    }

    return uid
}

/**
 * 获取当前平台名
 * @returns {string}
 */
export function getPlatform() {
    let result = '';
    let u = navigator.userAgent;
    if (u.match(/AppleWebKit.*Mobile.*/)) {
        if (u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
            result = 'ios'
        } else if (/(Android)/i.test(navigator.userAgent)) {
            result = 'android'
        } else {
            result = 'h5'
        }
    } else {
        result = 'pc'
    }
    return result;
}

/**
 * 科学的加法
 * @returns {*}
 */
Number.prototype.add = function (arg) {
    return addition(arg, this)
};

function addition(arg1, arg2) {

    let r1, r2, m, c;

    try {
        r1 = arg1.toString().split(".")[1].length
    } catch (e) {
        r1 = 0
    }

    try {
        r2 = arg2.toString().split(".")[1].length
    } catch (e) {
        r2 = 0
    }

    c = Math.abs(r1 - r2);
    m = Math.pow(10, Math.max(r1, r2));
    if (c > 0) {
        let cm = Math.pow(10, c);
        if (r1 > r2) {
            arg1 = Number(arg1.toString().replace(".", ""));
            arg2 = Number(arg2.toString().replace(".", "")) * cm;
        }
        else {
            arg1 = Number(arg1.toString().replace(".", "")) * cm;
            arg2 = Number(arg2.toString().replace(".", ""));
        }
    }
    else {
        arg1 = Number(arg1.toString().replace(".", ""));
        arg2 = Number(arg2.toString().replace(".", ""));
    }
    return (arg1 + arg2) / m

}

/**
 * 科学的减法
 * @param arg
 * @returns {*}
 */
Number.prototype.sub = function (arg) {
    return subtraction(arg, this)
};

function subtraction(arg1, arg2) {
    let r1, r2, m, n;
    try {
        r1 = arg1.toString().split(".")[1].length
    } catch (e) {
        r1 = 0
    }
    try {
        r2 = arg2.toString().split(".")[1].length
    } catch (e) {
        r2 = 0
    }
    m = Math.pow(10, Math.max(r1, r2));
    n = (r1 >= r2) ? r1 : r2;
    return Number(((arg2 * m - arg1 * m) / m).toFixed(n));
}

/**
 * 科学的乘法
 * @param arg
 * @returns {*}
 */
Number.prototype.mul = function (arg) {
    return multiplication(arg, this)
};

function multiplication(arg1, arg2) {

    let m = 0, s1 = arg1.toString(), s2 = arg2.toString();

    try {
        m += s1.split(".")[1].length
    } catch (e) {
    }

    try {
        m += s2.split(".")[1].length
    } catch (e) {
    }

    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)

}

/**
 * 科学的除法
 * @param arg
 * @returns {*}
 */
Number.prototype.div = function (arg) {
    return division(this, arg);
};

function division(arg1, arg2) {

    let t1 = 0, t2 = 0, r1, r2;

    try {
        t1 = arg1.toString().split(".")[1].length
    } catch (e) {
    }

    try {
        t2 = arg2.toString().split(".")[1].length
    } catch (e) {
    }


    r1 = Number(arg1.toString().replace(".", ""));

    r2 = Number(arg2.toString().replace(".", ""));

    return (r1 / r2) * Math.pow(10, t2 - t1);

}

Array.prototype.remove = function (val) {
    for (let i = 0; i < this.length;) {
        if (this[i] === val) {
            this.splice(i, 1);
        } else {
            i++;
        }
    }
};

Array.prototype.unique = function () {
    let r = [];
    for (let o of this) {
        if (!r.includes(o)) {
            r.push(o)
        }
    }
    return r;
};

/**
 * 隐藏姓名
 * */
export function nameMark(name) {
    return name.replace(/.(?=.)/g, '*');
}

/**
 * 隐藏电话
 * */
export function mobileMask(mobile) {
    return mobile.replace(/(\d{3})\d{4}(\d{4})/, "$1****$2");
}

/**
 * 隐藏银行卡或身份证
 * */
export function idMark(id) {
    return id.replace(/(\d{8})\d{4}(\d{6})/, "$1****$2");
}

/**
 * 隐藏银行卡或身份证
 * */
export function bankNumMark(id) {
    return id.replace(/(\d{4})(\d{4})(\d{4})(\d{4})/, `****  ****  ****  $1`);
}

export function getLocation() {
    let url = window.location.origin;
    return url.indexOf('localhost') !== -1 || url.indexOf('10.12.179.128') !== -1;
}

export function getObjectURL(file) {
    let url = null;
    if (window.createObjectURL !== undefined) { // basic
        url = window.createObjectURL(file);
    } else if (window.URL !== undefined) { // mozilla(firefox)
        url = window.URL.createObjectURL(file);
    } else if (window.webkitURL !== undefined) { // webkit or chrome
        url = window.webkitURL.createObjectURL(file);
    }
    return url;
}

export function bufferCallback(value, tag, callback, target) {
    if (value) {
        callback()
    } else {
        Schedule.addEventListener(tag, () => {
            callback();
            Schedule.removeEventListener(tag, target)
        }, target)
    }
}

/**
 * 获取查询字符串
 * @example ?t=1  返回{t:1}
 * @returns {{}}
 */
export function getSearch() {
    let search = {};
    let address = window.location.href;
    if (address.indexOf('?') !== -1) {
        [, address] = address.split('?');
        address = address.split('&');
        for (let o of address) {
            let [key, val] = o.split('=');
            search[key] = val;
        }
    }
    return search;
}

export function getState() {
    const origin = window.location.origin;
    let address = window.location.href, arrUrl = '';
    address = address.replace(origin,'');

    if(address.indexOf('/') !== -1){
        arrUrl = address.split('/')[1]
    }else{
        arrUrl = address.split('/')[0];
    }
    return  arrUrl
}

/**
 * 设置cookie
 * @param name
 * @param value
 * @param exp
 */
export function setCookie(name, value, exp) {
    let d = new Date();
    d.setTime(d.getTime() + (exp * 24 * 60 * 60 * 1000));
    document.cookie = `${name}=${value};expires=${d.toUTCString()};path=/`;
}

export function getCookie(name) {
    let v = window.document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}

/**
 *  判断字符长度
 *  @param info
 * */
export function getLenght(info) {
    let realLength = 0, len = info.length, charCode = -1;
    for (let i = 0; i < len; i++) {
        charCode = info.charCodeAt(i)
        if (charCode > 0 && charCode <= 128) {
            realLength++;
        } else {
            realLength += 2
        }
    }
    return realLength
}

export function getMoneyType(num) {
    switch (num){
        case 0 :
            return '元';
        case 1 :
            return '角';
        case 2 :
            return '分';
            break
    }
}

/**
 * 选取银行对应logo
 * */
export function chooseBankLogo(name) {
    switch (name) {
        case '工商银行':
            return ICBC;
        case '招商银行':
            return CMB;
        case '建设银行':
            return CCB;
        case '农业银行':
            return ABC;
        case '中国银行':
            return BOC;
        case '交通银行':
            return COMM;
        case '民生银行':
            return CMBC;
        case '浦发银行':
            return SPDB;
        case '中信银行':
            return CITIC;
        case '广发银行':
            return GDB;
        case '平安银行':
            return SZPAB;
        case '兴业银行':
            return CIB;
        case '华夏银行':
            return HXB;
        case '光大银行':
            return CEB;
        case '邮政储蓄':
            return PSBC;
    }
}

//todo 根据不同银行显示不同颜色
export function color(name) {
    switch (name) {
        case '工商银行':
            return 'redCard';
        case '招商银行':
            return 'redCard';
        case '建设银行':
            return 'blueCard';
        case '农业银行':
            return 'greenCard';
        case '中国银行':
            return 'redCard';
        case '交通银行':
            return 'blueCard';
        case '民生银行':
            return 'greenCard';
        case '浦发银行':
            return 'blueCard';
        case '中信银行':
            return 'redCard';
        case '广发银行':
            return 'redCard';
        case '平安银行':
            return 'redCard';
        case '兴业银行':
            return 'blueCard';
        case '华夏银行':
            return 'redCard';
        case '光大银行':
            return 'redCard';
        case '邮政储蓄':
            return 'greenCard';
    }
}