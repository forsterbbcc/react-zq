import {Req, Schedule} from "../lib";
import {Contracts, Data, Quotes} from "./index";
import {getSearch} from "../lib/tool";
import blog from '../lib/blog';

export default {
    initial: false,
    quoteList: [],
    domestic: {},
    domesticArray: [],
    stock: {},
    stockArray: [],
    foreign: {},
    foreignArray: [],
    self: {},
    selfArray: [],
    total: {},
    totalArray: [],
    hot: ['CL', 'IF', 'HSI', 'DAX', 'HG', 'RB'],
    new: ['SC', 'YM', 'NQ','NK'],
    selfList: ['IF','IC','IH','SC'],
    //todo 竞猜
    betArray: [],
    async init() {
        try {
            const res = await Req({url: '/api', data: getSearch()});
            let {quoteDomain, domesticCommds, foreignCommds, stockIndexCommds, contracts, quizCommodity} = res;
            blog(quoteDomain);
            // for (let e of domesticCommds) {
            //     if(e.code === 'SC'){
            //         this.self[e.code] = e;
            //         this.selfArray.push(e)
            //     }else{
            //         this.domestic[e.code] = e;
            //         this.domesticArray.push(e);
            //     }
            // }
            for (let e of domesticCommds) {
                this.foreign[e.code] = e;
                this.domesticArray.push(e);
            }
            for (let e of foreignCommds) {
                this.foreign[e.code] = e;
                this.foreignArray.push(e);
            }

            for (let e of stockIndexCommds) {
                this.foreign[e.code] = e;
                this.stockArray.push(e);
            }
            // for (let e of stockIndexCommds) {
            //     if(e.code === 'IF' || e.code === 'IC' || e.code ==='IH'){
            //         this.self[e.code] = e;
            //         this.selfArray.push(e)
            //     }else{
            //         this.stock[e.code] = e;
            //         this.stockArray.push(e);
            //     }
            // }


            this.betArray = quizCommodity || [];
            this.totalArray = [].concat(this.domesticArray, this.foreignArray, this.stockArray, this.selfArray);
            this.quoteList = JSON.parse(contracts);

            for (let [code, obj] of Object.entries(this.domestic)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.total[c] = obj;
                }
            }
            for (let [code, obj] of Object.entries(this.foreign)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.total[c] = obj;
                }
            }
            for (let [code, obj] of Object.entries(this.stock)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.total[c] = obj;
                }
            }
            for (let [code, obj] of Object.entries(this.self)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.total[c] = obj;
                }
            }



            this.quoteList = this.totalArray.map((c) => c.contract);

            this.updateSelf(true);

            Data.init();
            Quotes.init();
            this.initial = true;
            Schedule.dispatchEvent({event: 'contractsInitial', eventData: true})
        } catch (err) {
            console.warn(err);
            if (err.code !== undefined || err === '请求超时') {
                setTimeout(() => this.init(), 500)
            }
        }
    },

    isHot(contract) {
        let code = Contracts.total[contract].code;
        return this.hot.includes(code);
    },

    isNew(contract) {
        let code = Contracts.total[contract].code;
        return this.new.includes(code);
    },

    getContract(component) {
        if (this.initial) {
            let o = component.props.location.state.contract;
            if (!o) {
                [o] = Contracts.foreignArray;
                o = o.contract;
                component.props.location.state.contract = o;
            }
            return o;
        } else {
            return false;
        }
    },

    updateSelf(init){
        if (localStorage.getItem('self') !== null) {
            let ary = JSON.parse(localStorage.getItem('self'));
            this.selfArray = [];
            for (let item of this.totalArray) {
                let code = item.code;
                if (ary.includes(code)) {
                    this.selfArray.push(item)
                }
            }
            if(!init){
                Data.updateSelf()
            }
        }
    }
}