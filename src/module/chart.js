import {TradingView} from '../chartLib/charting_library.min';
import {DataFeeds} from "../chartLib/datafeed";

let jsonList = null;

export function JsonListMerge(address) {
    jsonList = address;
}

export default {
    study: [],
    table: null,
    widget: null,
    type: 'sline',
    init(options) {
        try{
            // console.log(options.dusk)
            if (!options.dom)
                return console.error('缺少chart容器ID');

            if (!options.code)
                return console.error('缺少初始商品ID');

            const RAISE = '#e34c4c';
            const FALL = '#00b38f';
            let widget = this.widget = new TradingView.widget({

                height:options.height,
                width:options.width,
                symbol: options.code,
                interval: '1',

                timezone: 'Asia/Hong_Kong',
                container_id: options.dom,
                datafeed: new DataFeeds.UDFCompatibleDatafeed("/api/tv/tradingView", jsonList, 1000),
                library_path: process.env.PUBLIC_URL+"/chartLab/",
                locale: 'zh',

                disabled_features: [
                    "use_localstorage_for_settings",
                    "header_symbol_search",
                    "header_saveload",
                    "header_screenshot",
                    "header_settings",
                    "header_compare",
                    "header_undo_redo",
                    "timeframes_toolbar",
                    "remove_library_container_border",
                    "border_around_the_chart",
                    "display_market_status",
                    "show_logo_on_all_charts",
                    "show_chart_property_page",
                    "panel_context_menu"
                ],
                enabled_features: [
                    "study_templates",
                    "keep_left_toolbar_visible_on_small_screens",
                    "side_toolbar_in_fullscreen_mode",
                    "property_pages",
                ],
                charts_storage_url: 'http://saveload.tradingview.com',
                charts_storage_api_version: "1.1",
                user_id: 'jailecoeu',
                overrides: {
                    'paneProperties.background':options.dusk? '#292837' :'#f8f8f8',
                    'paneProperties.vertGridProperties.color':options.dusk? "transparent" : '#f8f8f8',
                    'paneProperties.horzGridProperties.color':options.dusk? "transparent" : '#f8f8f8',

                    'paneProperties.legendProperties.showSeriesTitle': false,
                    // 'paneProperties.legendProperties.showStudyTitles': false,
                    // 'paneProperties.legendProperties.showStudyValues': false,
                    'paneProperties.legendProperties.showLegend': false,

                    'scalesProperties.lineColor':options.dusk? 'rgba(255,255,255, .5)' : '#666', //todo 刻度线颜色
                    'scalesProperties.textColor':options.dusk? 'rgba(255,255,255, .5)' : '#666', //todo 参数字体颜色

                    'mainSeriesProperties.candleStyle.upColor': RAISE,
                    'mainSeriesProperties.candleStyle.downColor': FALL,
                    'mainSeriesProperties.candleStyle.drawWick': true,
                    'mainSeriesProperties.candleStyle.drawBorder': false,
                    'mainSeriesProperties.candleStyle.borderUpColor': RAISE,
                    'mainSeriesProperties.candleStyle.borderDownColor': FALL,
                    'mainSeriesProperties.candleStyle.wickUpColor': RAISE,
                    'mainSeriesProperties.candleStyle.wickDownColor': FALL,
                    'mainSeriesProperties.candleStyle.barColorsOnPrevClose': false,
                    // LINESTYLE_SOLID = 0
                    // LINESTYLE_DOTTED = 1
                    // LINESTYLE_DASHED = 2
                    // LINESTYLE_LARGE_DASHED = 3

                    'mainSeriesProperties.areaStyle.color1':options.dusk? 'rgba(27,159,254, .5)' : '#444854', //todo 曲线图背景色
                    'mainSeriesProperties.areaStyle.color2':options.dusk?  'rgba(127,160,254, .1)' : '#ffffff',
                    'mainSeriesProperties.areaStyle.linecolor':options.dusk?  '#7F9FFE' : '#292837', //todo 曲线颜色

                    'mainSeriesProperties.areaStyle.linestyle': 1,
                    'mainSeriesProperties.areaStyle.linewidth': 3,
                    'mainSeriesProperties.areaStyle.priceSource': "close",
                    "symbolWatermarkProperties.color": "rgba(0, 0, 0, 0)"
                },

                preset: "mobile",
                studies_overrides: {
                    "volume.volume.color.0": FALL,
                    "volume.volume.color.1": RAISE,
                    "volume.volume.transparency": 30,
                }
            });

            widget.onChartReady(() => {
                if(this.widget === null)
                    return;
                this.table = this.widget.chart();
                // this.table.createStudy('Volume', false, false, [], (entityId) => {
                //     this.study.push(entityId);
                // }, {
                //     'volume ma.color': "rgba(132,170,213,0.7)"
                // });

                this.table.setChartType(3);
                this.table.onIntervalChanged().subscribe(null, (interval, obj) => {
                    if (interval === '1' && this.type === 'sline') {
                        this.table.setChartType(3);
                        if (this.study.length > 0) {
                            for (let i = 0, len = this.study.length; i < len; i++) {
                                let entityId = this.study.pop();
                                this.table.removeEntity(entityId);
                            }
                        }
                        // this.table.createStudy('Volume', false, false, [], (entityId) => {
                        //     this.study.push(entityId);
                        // }, {
                        //     'volume ma.color': "rgba(132,170,213,0.7)"
                        // });
                    } else {
                        this.table.setChartType(1);
                        if (this.study.length === 1) {
                            let entityId = this.study.pop();
                            this.table.removeEntity(entityId);
                            // this.table.createStudy('Moving Average', true, false, [5, "close", 0], (entityId) => {
                            //     this.study.push(entityId);
                            // }, {
                            //     'plot.color': "rgba(150,95,196,0.7)"
                            // });
                            // this.table.createStudy('Moving Average', true, false, [10, "close", 0], (entityId) => {
                            //     this.study.push(entityId);
                            // }, {
                            //     'plot.color': "rgba(132,170,213,0.7)"
                            // });
                            // this.table.createStudy('Moving Average', true, false, [20, "close", 0], (entityId) => {
                            //     this.study.push(entityId);
                            // }, {
                            //     'plot.color': "rgba(85,178,99,0.7)"
                            // });
                            // this.table.createStudy('Moving Average', true, false, [40, "close", 0], (entityId) => {
                            //     this.study.push(entityId);
                            // }, {
                            //     'plot.color': "rgba(183,36,138,0.7)"
                            // });
                            // this.table.createStudy('Volume', false, false, [], (entityId) => {
                            //     this.study.push(entityId);
                            // }, {
                            //     'volume ma.color': "rgba(132,170,213,0.7)"
                            // });
                        }
                    }
                })
            });
        }catch (err){err}

    },
    _refreshQuoteList(address){
        if(this.widget){
            this.widget.options.datafeed.updateQuoteUrl(address);
        }else{
            jsonList = address
        }
    },

    _addTechnicalIndicators(){
        if (this.study.length === 1) {
            let entityId = this.study.pop();
            this.table.removeEntity(entityId);
            // this.table.createStudy('Moving Average', true, false, [5, "close", 0], (entityId) => {
            //     this.study.push(entityId);
            // }, {
            //     'plot.color': "rgba(150,95,196,0.7)"
            // });
            // this.table.createStudy('Moving Average', true, false, [10, "close", 0], (entityId) => {
            //     this.study.push(entityId);
            // }, {
            //     'plot.color': "rgba(132,170,213,0.7)"
            // });
            // this.table.createStudy('Moving Average', true, false, [20, "close", 0], (entityId) => {
            //     this.study.push(entityId);
            // }, {
            //     'plot.color': "rgba(85,178,99,0.7)"
            // });
            // this.table.createStudy('Moving Average', true, false, [40, "close", 0], (entityId) => {
            //     this.study.push(entityId);
            // }, {
            //     'plot.color': "rgba(183,36,138,0.7)"
            // });
            // this.table.createStudy('Volume', false, false, [], (entityId) => {
            //     this.study.push(entityId);
            // }, {
            //     'volume ma.color': "rgba(132,170,213,0.7)"
            // });
        }else if(this.study.length > 0){
            for (let i = 0, len = this.study.length; i < len; i++) {
                let entityId = this.study.pop();
                this.table.removeEntity(entityId);
            }
            // this.table.createStudy('Volume', false, false, [], (entityId) => {
            //     this.study.push(entityId);
            // }, {
            //     'volume ma.color': "rgba(132,170,213,0.7)"
            // });
        }
    },


    swap(options) {
        if(!this.table)
            return;

        if (!options)
            return console.error('切换表格缺少参数 options');
        //todo 模式一 切换类型
        if (!!options.type && !options.code) {
            if(this.type === 'sline' && options.type === '1'){
                this.table.setChartType(1);
                this._addTechnicalIndicators();
                this.type = options.type;
            }else if(this.type === '1' && options.type === 'sline'){
                this.table.setChartType(3);
                this._addTechnicalIndicators();
                this.type = options.type;
            }else{
                this.type = options.type;
                const type = (options.type === 'sline' ? '1' : options.type);
                this.table.setResolution(type);
            }
        }

        //todo 模式二 切换合约
        if (!!options.code && !options.type) {
            this.table.setSymbol(options.code);
        }

        //todo 模式三 切换模式与合约
        if (!!options.code && !!options.type) {
            this.type = options.type;
            const type = (options.type === 'sline' ? '1' : options.type);
            this.widget.setSymbol(options.code, type);
        }
    },
    exit() {

        this.widget &&this.widget.options && this.widget.options.datafeed.unsubscribeAll();
        // if(!!this.widget){
        //     this.widget.remove();
        // }
        this.widget = null;
        this.table = null;
        this.type = 'sline';
    }
}