import React, {Component} from 'react';
import downArrow from '../../images/down-arrow.png';
import {Req} from "../../lib";
import {formatDate} from "../../lib/tool";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            data:[],
            selectedIndex:0
        }
    }

    render() {
        return (
            <div className={'noticeBox'}>
                <div className="main">
                    <ul>
                        {
                            this.state.data.map((item, i) => {
                                if (this.state.selectedIndex === i) {
                                    return (
                                        <li key={i} onClick={() => this.openNotice(i)}>
                                            <div className={'titleBox'}>
                                                <div className={'title'}>{item.title}</div>
                                                <div>
                                                    <img className={'up'} src={downArrow}/>
                                                </div>
                                            </div>
                                            <div className={'noticeInfo'}>
                                                <div dangerouslySetInnerHTML={{__html: item.content}}/>
                                                <div className={'time'}>{formatDate('y-m-d h:i:s',{date:item.time.time})}</div>
                                            </div>
                                        </li>
                                    );
                                } else {
                                    return (
                                        <li key={i} onClick={() => this.openNotice(i)}>
                                            <div className={'titleBox'}>
                                                <div className={'title'}>{item.title}</div>
                                                <div>
                                                    <img src={downArrow}/>
                                                </div>
                                            </div>
                                        </li>
                                    );
                                }
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.getNotice()
    }

    //todo 获取公告信息
    getNotice(){
        Req({
            url:'/api/discover/index.htm',
        }).then((result)=>{
            this.setState({
                data : result.notices
            })
        })
    }


    openNotice(index) {
        if (this.state.selectedIndex === index) {
            this.setState({
                selectedIndex: -1
            });
        } else {
            this.setState({
                selectedIndex: index
            });
        }
    }
}