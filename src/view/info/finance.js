import React, {Component} from 'react';
import {completeNum} from "../../lib/tool";
import {Req} from "../../lib";
import ReactSVG from "react-svg";
import empty from '../../images/svg/empty.svg';
import star from '../../images/svg/star.svg';
import unFill from '../../images/svg/unfillStar.svg';
import month from '../../images/svg/month.svg';

export default class App extends Component {
    today = null;
    month = null;
    length = null;

    constructor(props) {
        super(props);
        this.state = {
            news: [],
            show: this.diff(0).show,
            starArr:[1,2,3],
            thisMonth:null
        }
    }

    render() {
        return (
            <div className={'financeBox'}>
                <div className="top">
                    <div className="month">
                        <span>{this.state.thisMonth}月</span>
                        <ReactSVG path={month} className={'iconStyle month'} wrapperClassName={'monthWrap'}/>
                    </div>
                    <div className={'date'}>
                        {
                            this.getWeekTime().map((item, key) => {
                                return (
                                    <div className={this.state.show === item.show ? 'active' :''}
                                         onClick={()=>{
                                             this.getNewsInfo(item.val);
                                             this.setState({
                                                 show : item.show
                                             })
                                         }}
                                    >
                                        <div>{item.week.replace('星期','')}</div>
                                        <div><span>{item.show}</span></div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                <div className={'newsBox'}>
                    {
                        this.state.news.length!==0?(
                            this.state.news.map((item) => {
                                return (
                                    <div className={'newsInfo'}>
                                        <div className={'time'}>
                                            <p>{item.time_show}</p>
                                        </div>
                                        <div className={'info'}>
                                            <div className={'top'}>
                                                {/*<span>{item.country}</span>*/}
                                                <span className={item.status_name === '利空' ? '' : 'spanColor'}>
                                               {
                                                   item.status_name === '利空' ? '利空 金银 原油' : '利多 金银 原油'
                                               }
                                                </span>
                                                <div className="stars">
                                                    {
                                                        this.state.starArr.map((it,index)=>{
                                                            return (
                                                                <ReactSVG path={index<item.star?star:unFill} className={'starWrap'}/>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            </div>
                                            <p>{item.title}</p>
                                            <div className={'last'}>
                                                <div>前值&nbsp;<span>{item.unit === '%' ? (item.previous + item.unit) : item.previous}</span></div>
                                                <div>预期&nbsp;
                                                    <span>{item.consensus == null ? '--' : (item.unit === '%' ? (item.consensus + item.unit) : item.consensus)}</span>
                                                </div>
                                                <div>今值&nbsp;
                                                    <span>{item.actual == null ? '未公布' : (item.unit === '%' ? (item.actual + item.unit) : item.actual)}</span></div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                            ):(
                            <div className={'imgWrap1'}>
                                <ReactSVG path={empty} className={'placeholder'}/>
                            </div>
                        )
                    }
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.today = this.diff(0).val;
        this.month = this.diff(0).show;
        this.getNewsInfo(this.today);
        this.getWeekTime()
    }

    async getNewsInfo(time) {
        let result = await Req({
            url: '/api/news/calendar.htm?date=' + time,
            type: 'GET',
            animate: time
        });
        this.setState({
            news: result.news.newsData
        },()=>{
            if(!!this.state.news&&this.state.news.length!==0){
                const thisMonth = this.state.news[0].publictime.split(' ')[0].split('-')[1];
                this.setState({
                    thisMonth
                })
            }
        })
    }

    //todo 获取当天日期
    diff(v) {
        let now = new Date();
        let date = new Date(now.getTime() + v * 24 * 3600 * 1000);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        month = completeNum(month);
        let day = date.getDate();
        day = completeNum(day);
        //todo 计算当前月的时常

        let obj = {};
        obj.show = day;
        obj.val = year.toString() + month.toString() + day.toString();
        let week = ["日", "一", "二", "三", "四", "五", "六"];
        obj.week = '星期' + week[date.getDay()];
        obj.today = v === 0;
        obj.monthLength = new Date(year, month, 0).getDate();
        return obj;
    }

    //todo 获取一周内数据
    getWeekTime(){
        let ary = [], num = -7;
        for(let i =0 ;i < 7; i++){
            num ++;
            ary.push(this.diff(num))
        }
        return ary
    }
}