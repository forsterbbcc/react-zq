import React, {Component} from 'react'
// import Footer from './../common/footer'
import {Svg, Footer} from "../common";
import RouteWithSubRoutes from './../../routes/routeWithSubRoutes'
import {NavLink,Link} from 'react-router-dom'
import activity from '../../images/activity.png'
import recharge from '../../images/recharge.png'
import {Contracts, Cache} from "../../module";
import {Schedule} from "../../lib";

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isShowDetail:false,
            code: null,
            name:null,
            id:null,
        }
        if (Contracts.initial) {
            let [o] = Contracts.foreignArray;
            this.state.code = o.contract;
            this.state.name = o.name;
            this.state.id = o.code
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }
    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }
    updateContracts() {
        let [o] = Contracts.foreignArray;
        this.setState({
            code: o.contract,
            name: o.name,
            id:o.code
        })
    }

    render() {
        const a = this.props.routes;
        return (
            <div className='infoPage'>
                <div className={'top1'}>
                资讯
                </div>
                <div className={'linkBox'}>
                    <Link to={'/activity/freeGift'}><img src={activity}/></Link>
                    <Link to={'/recharge'}><img src={recharge}/></Link>
                    {/* <Link to={{pathname: '/trade',state:{simulate: false,
                        id: this.state.id,
                        name:this.state.name,
                        contract: this.state.code,
                        code:this.state.code}}}><img src={recharge}/></Link> */}
                </div>
                <div className={'nav'}>
                    <NavLink to={'/news/live'} activeClassName={'selected'}><div>快讯</div></NavLink>
                    <NavLink to={'/news/oil'} activeClassName={'selected'}><div>新闻</div></NavLink>
                    <NavLink to={'/news/finance'} activeClassName={'selected'}><div>财经</div></NavLink>
                    <NavLink to={'/news/notice'} activeClassName={'selected'}><div>公告</div></NavLink>
                </div>
                {a.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
                <div>
                    <Footer/>
                </div>
            </div>        
        );
    }
}
