import React, {Component} from 'react'
import ReactPullLoad, {STATS} from 'react-pullload'
import {Req} from '../../lib/index'
//星星图片
import oneStar from './../../images/1.png'
import twoStar from './../../images/2.png'
import threeStar from './../../images/3.png'
import fourStar from './../../images/4.png'
import fiveStar from './../../images/5.png'

export default class App extends Component {

    mount = true

    constructor(props) {
        super(props);
        this.state = {
            hasMore: true,
            data: [],
            action: STATS.init,
            date: {
                month:'',
                day:''
            }
        }
    }

    componentDidMount() {
        //this.update(0);
        this.handRefreshing();
    }

    componentWillUnmount(){
        this.mount = false
    }

    async update(id) {
        try {
            const result = await Req({
                url: '/api/news/expressList.htm',
                data: {
                    maxId: id
                }
            })
            if (result && result.code && result.code == 200) {
                let tempArr = result.newsList;
                let dataArr = []
                for (let i = 0; i < result.newsList.length; i++) {
                    let element = result.newsList[i];
                    let spliceResult = element.split('#');
                    let obj = null;
                    if (spliceResult.length == 12) {
                        obj = {
                            origin: element,
                            date: spliceResult[2],
                            content: spliceResult[3],
                            id: spliceResult[spliceResult.length - 1]
                        }
                    } else if (spliceResult.length == 14) {
                        obj = {
                            origin: element,
                            date: spliceResult[8],
                            content: spliceResult[2],
                            id: spliceResult[spliceResult.length - 2],
                            qz: '前值：' + spliceResult[3],
                            yq: '预期：' + spliceResult[4],
                            sj: '实际：' + spliceResult[5],
                            tag: spliceResult[7],
                            star: spliceResult[6],
                            country: 'https://res.6006.com/jin10/flag/' + spliceResult[9].substr(0, 2) + '.png'
                        }
                    }
                    dataArr.push(obj);
                }
                if (dataArr && dataArr[0] && dataArr[0].date) {
                    let objDate = dataArr[0].date;
                    let tempDate = objDate.split(' ')[0];
                    let dateArr = tempDate.split('-');
                    let month = dateArr[1];
                    let day = dateArr[2];

                    if (this.mount === false){
                        return;
                    }

                    this.setState({
                        date:{
                            month:month,
                            day:day
                        }
                    });
                }

                if (this.mount === false){
                    return;
                }

                if (id != 0) {
                    this.setState({
                        data: this
                            .state
                            .data
                            .concat(dataArr),
                        action: STATS.reset
                    });
                } else {
                    if (STATS.refreshing == this.state.action) {
                        this.setState({data: dataArr, action: STATS.refreshed});
                    } else {
                        this.setState({data: dataArr});
                    }
                }
            }
        } catch (err) {
            //console.log(err);
        }
    }

    handleAction = (action) => {
        if (action === this.state.action) {
            return false
        }

        if (action === STATS.refreshing) {
            this.handRefreshing();
        } else if (action === STATS.loading) {
            this.handLoadMore();
        } else {
            //DO NOT modify below code
            if (this.mount === false) {
                return;
            }
            this.setState({action: action})
        }
    }

    handRefreshing = () => {
        if (STATS.refreshing === this.state.action) {
            return false
        }

        this.update(0)
        if (this.mount === false){
            return;
        }
        this.setState({action: STATS.refreshing})
    }

    handLoadMore = () => {
        if (STATS.loading === this.state.action) {
            return false
        }
        //无更多内容则不执行后面逻辑
        if (!this.state.hasMore) {
            return;
        }

        let lastIndex = this.state.data.length;
        let lastObj = this.state.data[lastIndex - 1];
        let id = lastObj.id;
        this.update(id);
        if (this.mount === false){
            return;
        }
        this.setState({action: STATS.loading})
    }

    getStarImageFrom(level) {
        if (level == 1) {
            return oneStar;
        } else if (level == 2) {
            return twoStar;
        } else if (level == 3) {
            return threeStar;
        } else if (level == 4) {
            return fourStar;
        } else if (level == 5) {
            return fiveStar;
        }
    }

    renderItem() {
        let content = this
            .state
            .data
            .map((item, i) => {
                if (item.tag) {
                    return (
                        <tr key={i} className={'liveListItem'}>
                            <td>
                                <div>{item.date}</div>
                                <div>
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: item.content
                                    }}/>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>{item.qz}</td>
                                        <td>{item.yq}</td>
                                        <td>{item.sj}</td>
                                        {/* <td rowSpan='2'>
                                            <img src={item.country}/>
                                        </td> */}
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src={this.getStarImageFrom(item.star)}/>
                                        </td>
                                        <td colSpan='2'>
                                            <div className={item.tag == '利多' ? 'liduo' : 'likong'}>{item.tag}</div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            </td>
                        </tr>
                    );
                } else {
                    return (
                        <tr key={i} className={'liveListItem'}>
                            <td>
                                <div>{item.date}</div>
                                <div dangerouslySetInnerHTML={{
                                    __html: item.content
                                }}/>
                            </td>
                        </tr>
                    );
                }
            });
        return content;
    }

    render() {

        return (
            <div>
                <ReactPullLoad
                    downEnough={50}
                    action={this.state.action}
                    handleAction={this.handleAction}
                    hasMore={true}
                    style={{
                    backgroundColor: "#f4f4f4",
                    paddingTop:12
                }}
                    distanceBottom={1500}>
                    <table className='live'>
                        <tbody>
                            {this.renderItem()}
                        </tbody>
                    </table>
                </ReactPullLoad>
                <div className='dateComponent'>
                    {/*<div>{this.state.date.day}</div>*/}
                    <div>{this.state.date.month+'月'}</div>
                </div>
            </div>

        );
    }
}