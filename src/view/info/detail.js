import React, {Component} from 'react'
import Header from '../common/header'
import {Req} from './../../lib/index'

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            content:null
        }
    }

    render() {
        return (
            <div className={'infodetail'}>
                <Header title={'资讯详情'} {...this.props}/>
                <div>
                    <h2>{this.state.title}</h2>
                    <p>{this.state.date}</p>
                </div>
                <div dangerouslySetInnerHTML={{__html:this.state.content}}></div>
            </div>
        );
    }

    componentDidMount(){
        let id = this.props.match.params.id;
        this.getDetail(id);
    }

    async getDetail(id) {
        try {
          const result = await Req({
            url: '/api/news/newsDetail.htm',
            data: {
              id: id
            },
              animate:true
          });
          this.setState({
              content:result.news.content,
              date:result.news.date,
              title:result.news.title
          });
        } catch (err) {}
      }
}