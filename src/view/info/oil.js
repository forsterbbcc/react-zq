import React, {Component} from 'react'
import ReactPullLoad, {STATS} from 'react-pullload'
import {Req} from "../../lib/index";
import '../../../node_modules/react-pullload/dist/ReactPullLoad.css';
import {Link} from 'react-router-dom'

let loadMoreLimitNum = 10;
let hasMore = true;

export default class App extends Component {
    mount = true
    constructor(props) {
        super(props);
        this.state = {
            hasMore: true,
            data: [],
            action: STATS.init,
            index: loadMoreLimitNum, //loading more test time limit
            isShowDetail: false
        }
    }
    componentDidMount() {
        this.handRefreshing();
    }

    componentWillUnmount(){
        this.mount = false
    }

    async update(type, date) {
        try {
            const result = await Req({
                url: '/api/news/newsList.htm',
                data: {
                    type: type,
                    date: date
                }
            })
            if (result && result.code && result.code == 200) {
                if (this.mount === false) {
                    return;
                }
                if (!date) {
                    if (STATS.refreshing == this.state.action) {
                        this.setState({data: result.newsList, action: STATS.refreshed});
                    } else {
                        this.setState({data: result.newsList});
                    }
                } else {
                    this.setState({
                        data: this
                            .state
                            .data
                            .concat(result.newsList),
                        action: STATS.reset,
                        index: this.state.index - 1
                    });
                }
            }
        } catch (err) {}
    }

    handleAction = (action) => {
        // console.info(action, this.state.action, action === this.state.action);
        if (action === this.state.action) {
            return false
        }

        if (action === STATS.refreshing) {
            this.handRefreshing();
        } else if (action === STATS.loading) {
            this.handLoadMore();
        } else {
            //DO NOT modify below code
            if (this.mount === false) {
                return;
            }
            this.setState({action: action})
        }
    };

    handRefreshing = () => {
        if (STATS.refreshing === this.state.action) {
            return false
        }

        this.update(0);
        if (this.mount === false) {
            return;
        }
        this.setState({action: STATS.refreshing})
    };

    handLoadMore = () => {
        if (STATS.loading === this.state.action) {
            return false
        }
        //无更多内容则不执行后面逻辑
        if (!this.state.hasMore) {
            return;
        }

        let dataLength = this.state.data.length;
        let lastDate = this.state.data[dataLength - 1].date;
        this.update(0, lastDate);
        if (this.mount === false) {
            return;
        }
        this.setState({action: STATS.loading})
    };

    renderItem() {
        let content = this.state.data.map((item, i) => {
                return (
                    <tr key={i}>
                        <td>
                            <Link to={'/detail/'+item.id}>
                                <div>
                                    <h2>{item.title}</h2>
                                    <p>{item.date}</p>
                                </div>
                                <div>
                                    <img src={item.thumb}/>
                                </div>
                            </Link>
                        </td>
                    </tr>
                );
            });
        return content;
    }

    render() {
        return (
            <div>
                <ReactPullLoad
                    downEnough={50}
                    action={this.state.action}
                    handleAction={this.handleAction}
                    hasMore={hasMore}
                    style={{
                        backgroundColor: "#f4f4f4",
                        // paddingTop:51
                    }}
                    distanceBottom={300}>
                    <table className='oil'>
                        <tbody>
                        {this.renderItem()}
                        </tbody>
                    </table>
                </ReactPullLoad>
            </div>

        );
    }
}