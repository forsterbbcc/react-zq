import React, { Component } from 'react'
import {formatDate} from "../../lib/tool";
import {Cache, Contracts} from "../../module/index";
import {NavLink,Link} from 'react-router-dom';
import ReactSvg from "react-svg";

import back from '../../images/svg/back.svg'

export default class accountSet extends Component {
    constructor(props){
        super(props);
        this.state={
            simulate:this.props.location.state?this.props.location.state.simulate:null,
            list:this.props.location.state?this.props.location.state.list:null,
            charge:null,
            contract:null,
            darkType:false
        }
    }

    componentWillMount(){
        this.setState({
            darkType:JSON.parse(localStorage.getItem('bgc'))
        })
        //根据名字查找对象,再获取现在的商品号contCode
        const nowContract = Object.values(Cache.tradeList).filter((item,index)=>{
            return item.commName === this.state.list.commodity
        });
        const o = nowContract[0].contCode;
        const scheme = Cache.tradeList[o];
        const charge = scheme.chargeUnit.mul(this.state.list.volume).mul(this.state.list.moneyType === 0 ? 1 :(this.state.list.moneyType === 1? 0.1 : 0.01));
        this.setState({
            charge,
            contract:o
        })
    }
    render() {
        const state={
            simulate:this.state.simulate,
            contract:this.state.contract
        };
        return (
            <div className={`position_detail ${this.state.darkType?'dark':''}`}>
                {/*<Header title={this.state.list.commodity} {...this.props}/>*/}
                <div className="header">
                    <Link to={{pathname:'/position/settlement',state:state,where:this.props.location.state.where}}>
                        <ReactSvg path={back} className={'returnarrow'} wrapperClassName={'out'}/>
                    </Link>
                    <span>{this.state.list.commodity}</span>
                </div>
                <div className="content">
                    <div className="top">
                        <span>盈亏</span>
                        <p className={this.state.list.income<0?'lose':''}>{this.state.list.income<0?'':'+'}{this.state.list.income}元</p>
                    </div>
                    <div className="mid">
                        <ul>
                            <li>
                                <span>订单编号:</span>
                                <span>{this.state.list.id}</span>
                            </li>
                            <li>
                                <span>交易模式:</span>
                                <span>{this.state.list.moneyType===0?'元模式':(this.state.list.moneyType===1?'角模式':'分模式')}</span>
                            </li>
                            <li>
                                <span>买入价:</span>
                                <span>{this.state.list.opPrice.toFixed(this.state.list.priceDigit)}</span>
                            </li>
                            <li>
                                <span>卖出价:</span>
                                <span>{this.state.list.cpPrice.toFixed(this.state.list.priceDigit)}</span>
                            </li>
                            <li>
                                <span>止损价:</span>
                                <span>{this.state.list.stopLoss}</span>
                            </li>
                            <li>
                                <span>止盈价:</span>
                                <span>{this.state.list.stopProfit}</span>
                            </li>
                            <li>
                                <span>交易手数:</span>
                                <span>{this.state.list.volume}手</span>
                            </li>
                            <li>
                                <span>交易综合费:</span>
                                <span>{this.state.charge}元</span>
                            </li>
                        </ul>
                    </div>
                    <div className="bot">
                        平仓时间:&nbsp;&nbsp;{formatDate('y/m/d h:i:s', {date: this.state.list.tradeTime.time})}
                    </div>
                </div>
            </div>
        )
    }
}
