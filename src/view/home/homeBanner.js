import React, {Component} from 'react';
import ReactSwipe from 'react-swipe';
import {Link} from 'react-router-dom';
import {Store} from "../../module/index";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            banners: []
        }
    }

    render() {
        return (
            <div className={'bannerBox'}>
                <ReactSwipe swipeOptions={{continuous: true, speed: 500, startSlide: 0, auto: 2000}}
                            key={this.state.banners.length}>
                    {
                        this.state.banners.map((item) => {
                            if (item.key.indexOf('/') === -1) {
                                return (
                                    <a>
                                        <img
                                            src={process.env.NODE_ENV === 'development' ? `https://fk.76bao.hk/${item.url}` : item.url}
                                            alt={item.mcname}/>
                                    </a>
                                )
                            } else {
                                return (
                                    <Link to={item.key}>
                                        <img
                                            src={process.env.NODE_ENV === 'development' ? `https://fk.76bao.hk/${item.url}` : item.url}
                                            alt={item.mcname}/>
                                    </Link>
                                )
                            }

                        })
                    }
                </ReactSwipe>
            </div>
        )
    }

    componentDidMount() {
        this.mounted = true;
        Store.banner.get().then((data) => {
            if (this.mounted) {
                this.setState({
                    banners: data
                })
            }
        })
    }

    componentWillUnmount() {
        this.mounted = false;
    }
}