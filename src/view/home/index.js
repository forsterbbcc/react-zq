import React, {Component} from 'react';
import {Link} from 'react-router-dom';
// import ReactSvg from 'react-svg';
import {Req, Schedule} from "../../lib";
import {Svg, Footer} from "../common";
import Notice from './homeNotices';
import {Contracts, Data} from "../../module";
import {Cache} from "../../module";

//todo 图片引入
import test from '../../images/test.png'
import position from '../../images/position.png'
import notice from '../../images/svg/notice.svg'
import greenLine from '../../images/greenLine.png';
import redLine from '../../images/redLine.png';
import xiu from '../../images/xiu.png';

import CL from '../../images/goods/CL.svg'
import GC from '../../images/goods/GC.svg'
import SI from '../../images/goods/SI.svg'
import HG from '../../images/goods/HG.svg'
import XAG from '../../images/goods/XAG.svg'
import CN from '../../images/goods/CN.svg'
import DAX from '../../images/goods/DAX.svg'
import MHI from '../../images/goods/MHI.svg'
import YM from '../../images/goods/YM.svg'
import NQ from '../../images/goods/NQ.svg'
import MDAX from '../../images/goods/MDAX.svg'
import IF from '../../images/goods/IF.svg'
import IH from '../../images/goods/IH.svg'
import IC from '../../images/goods/IC.svg'
import NK from '../../images/goods/NK.svg'
import NI from '../../images/goods/NI.svg'

import BP from '../../images/goods/BP.svg'
import EC from '../../images/goods/EC.svg'
import XAU from '../../images/goods/XAU.svg'
import QM from '../../images/goods/QM.svg'
import RBB from '../../images/goods/RBB.svg'
import SR from '../../images/goods/SR.svg'
import CU from '../../images/goods/CU.svg'
import RB from '../../images/goods/RB.svg'
import PP from '../../images/goods/PP.svg'
import HC from '../../images/goods/HC.svg'
import SM from '../../images/goods/SM.svg'
import RU from '../../images/goods/RU.svg'
import SC from '../../images/goods/SC.svg'
import AG from '../../images/goods/AG.svg'
import AU from '../../images/goods/AU.svg'
import NG from '../../images/goods/NG.svg'
import HSI from '../../images/goods/HSI.svg'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            userName: '未登录',
            money: '',
            game: '',
            idNumber: Cache.idNumber,
            gender: true,
            type: 1,
            contract: '',
            notices: [],
            foreignArray: [],
            stockArray: [],
            domesticArray: [],
            hot: [],
            news: [],
            code:'',
            id:'',
            name:'',
        };

        if (Contracts.initial) {
            this.state.foreignArray = Data.foreignBrief;
            this.state.stockArray = Data.stockBrief;
            this.state.domesticArray = Data.domesticBrief;
            let [o] = Contracts.foreignArray;
            this.state.contract = o.contract;
            this.state.hot = Contracts.hot;
            this.state.news = Contracts.new;
            this.state.name = o.name;
            Data.start('updateBrief');
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }

    render() {
        let hotArray = [];
        let stockArray = this.state.stockArray;
        let foreignArray = this.state.foreignArray;
        if(stockArray.length>=2){
            hotArray = [];
            let a1,a2,b1,b2,c1,c2;
            for(let i=0; i<foreignArray.length;i++){
                if(foreignArray[i].name=='美原油'){
                    a1 = foreignArray[i];
                }else if(foreignArray[i].name=='美黄金'){
                    a2 = foreignArray[i];
                }else if(foreignArray[i].name=='欧元'){
                    c2 = foreignArray[i];
                }
            }
            for(let i=0; i<stockArray.length;i++){
                if(stockArray[i].name=='恒指'){
                    b1 = stockArray[i];
                }else if(stockArray[i].name=='小纳指'){
                    c1 = stockArray[i];
                }else if(stockArray[i].name=='德指'){
                    b2 = stockArray[i];
                }
            }
            hotArray[0] = a1?a1:(a2?a2:foreignArray[0]);
            hotArray[1] = b1?b1:(b2?b2:stockArray[0]);
            hotArray[2] = c1?c1:(c2?c2:stockArray[1]);
        }
        return (
            <div className={'home'}>
                <main className={'mainTop'}>
                    {/*顶部导航*/}
                    <nav>
                        <Link to={{
                            pathname: '/trade',
                            state: {
                                simulate: true,
                                // code: this.state.code,
                                contract: this.state.contract,
                                // name: this.state.name,
                            }
                        }}>
                            <img src={test} alt=""/>
                            <div>模拟练习</div>
                        </Link>
                        <Link to={{
                            pathname: '/position',
                            state: {
                                simulate: false
                            }}}
                        >
                            <img src={position} alt=""/>
                            <div>实盘持仓</div>
                        </Link>
                        {/*<Link to={'/'}>*/}
                            {/*<img src={promotions} alt=""/>*/}
                            {/*<div>优惠活动</div>*/}
                        {/*</Link>*/}
                    </nav>
                    {/*公告*/}
                    <div className={'noticeBox'}>
                        <div>
                            <Svg path={notice}/>
                        </div>
                        <Link to={'/news/notice'}>
                            <Notice/>
                        </Link>
                    </div>
                    {/*热门*/}
                    <div className={'hotBox'}>
                        <div className={'hotTitle'}>
                            <div>🔥热门商品</div>
                        </div>
                        <div className={'hotMain'}>
                            {
                                hotArray.map((item, key) => {
                                        return (
                                            <Link key={key} to={{
                                                pathname: '/trade',
                                                state: {
                                                    simulate: false,
                                                    code: item.code,
                                                    name: item.name,
                                                    id:item.id,
                                                    contract: item.code,
                                                }
                                            }}
                                                  className={'hotGoods'}>
                                                <div className={'top'}>
                                                    {item.name}
                                                    {/* <span className={'hot'}>HOT</span> */}
                                                </div>
                                                <div className={'middle'}>
                                                    {item.price || '- -'}
                                                </div>
                                                <div className={'percent'}>
                                                    {item.isOpen ? item.rate : '休市'}
                                                </div>
                                            </Link>
                                        )
                                })
                            }
                        </div>
                    </div>
                </main>
                <div className={'mainBottom'}>
                    <div className={`navTitle`}>
                        <div className={this.state.type === 1 ? 'active' : ''}
                            onClick={() => {
                                 this.setState({type: 1})
                             }}>
                            国际期货
                        </div>
                        <div className={this.state.type === 2 ? 'active' : ''}
                             onClick={() => {
                                 this.setState({type: 2})
                             }}>
                            股指期货
                        </div>
                        <div className={this.state.type === 3 ? 'active' : ''}
                             onClick={() => {
                                 this.setState({type: 3})
                             }}>
                            国内期货
                        </div>
                    </div>
                    <div className={'mainGoods'}>
                        {
                            this.state.type === 1 ? (
                                <Position array={this.state.foreignArray}/>
                            ) : ('')
                        }
                        {
                            this.state.type === 2 ? (
                                <Position array={this.state.stockArray}/>
                            ) : ('')
                        }
                        {
                            this.state.type === 3 ? (
                                <Position array={this.state.domesticArray}/>
                            ) : ('')
                        }
                    </div>
                </div>
                {/*申明*/}
                <div className={'declare'}>
                        <p>风险告知</p>
                        <p>期货市场有风险，用户投资需谨慎</p>
                        <p>平台交易与纽约交易所、芝加哥交易所及港交所等实盘对接。</p>
                    </div>
                <Footer/>
            </div>
        )
    }

    componentDidMount() {
        // if (Cache.initial) {
        //     this.loginCallback()
        // } else {
        //     Schedule.addEventListener('cacheInitial', this.loginCallback, this)
        // }

        // if (Cache.userId !== '') {
        //     this.getInfoCallback();
        // } else {
        //     Schedule.addEventListener('getUserInfo', this.getInfoCallback, this)
        // }
        Schedule.addEventListener('updateBrief', this.updateBrief, this)
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
        Data.end('updateBrief');
    }

    // loginCallback() {
    //     this.setState({
    //         isLogin: Cache.isLogin()
    //     })
    // }
    // getInfoCallback() {
    //     this.setState({
    //         userId: Cache.userId,
    //         userName: Cache.username,
    //         money: Cache.realBalance,
    //         game: Cache.gameBalance,
    //         idNumber: Cache.idNumber
    //     });

    //     let num = this.state.idNumber.slice(16, 17) % 2;
    //     num === 0 ? this.setState({gender: true}) : this.setState({gender: false});
    // }

    updateContracts() {
        let [o] = Contracts.foreignArray;
        this.setState({
            code: o.contract,
            id:o.code,
            name: o.name,
            contract: o.contract,
            foreignArray: JSON.parse(JSON.stringify(Data.foreignBrief)),
            stockArray: JSON.parse(JSON.stringify(Data.stockBrief)),
            domesticArray: JSON.parse(JSON.stringify(Data.domesticBrief)),
            hot: Contracts.hot
        });
        Data.start('updateBrief');
    }

    updateBrief() {
        this.setState((state)=>{
            Data.foreignBrief.map((newItem,newIndex)=>{
                state.foreignArray.map((item,index)=>{
                    if(index === newIndex){
                        if(parseFloat(newItem.price) - parseFloat(item.price) >0){
                            newItem['up'] = true;
                        }else if(parseFloat(newItem.price) - parseFloat(item.price)<0){
                            newItem['up'] = false;
                        }
                    }
                })
            })
            Data.stockBrief.map((newItem,newIndex)=>{
                state.stockArray.map((item,index)=>{
                    if(index === newIndex){
                        if(parseFloat(newItem.price) - parseFloat(item.price) >0){
                            newItem['up'] = true;
                        }else if(parseFloat(newItem.price) - parseFloat(item.price)<0){
                            newItem['up'] = false;
                        }
                    }
                })
            })
            Data.domesticBrief.map((newItem,newIndex)=>{
                state.domesticArray.map((item,index)=>{
                    if(index === newIndex){
                        if(parseFloat(newItem.price) - parseFloat(item.price) >0){
                            newItem['up'] = true;
                        }else if(parseFloat(newItem.price) - parseFloat(item.price)<0){
                            newItem['up'] = false;
                        }
                    }
                })
            })
            return{
                foreignArray:JSON.parse(JSON.stringify(Data.foreignBrief)),
                stockArray: JSON.parse(JSON.stringify(Data.stockBrief)),
                domesticArray: JSON.parse(JSON.stringify(Data.domesticBrief))
            }
        },()=>{
            setTimeout(()=>{
                this.setState((state)=>{
                    Data.foreignBrief.map((newItem,newIndex)=>{
                        newItem.up = null;
                    })
                    Data.stockBrief.map((newItem,newIndex)=>{
                        newItem.up = null;
                    })
                    Data.domesticBrief.map((newItem,newIndex)=>{
                        newItem.up = null;
                    })
                    return{
                        foreignArray:JSON.parse(JSON.stringify(Data.foreignBrief)),
                        stockArray: JSON.parse(JSON.stringify(Data.stockBrief)),
                        domesticArray: JSON.parse(JSON.stringify(Data.domesticBrief))
                    }
                });
            },900)
        });
    }

    
}

function goods(a){
    switch (a) {
        case 'CL':
        return CL;
        case 'GC':
        return GC;
        case 'SI':
        return SI;
        case 'HG':
        return HG;
        case 'XAG':
        return XAG;
        case 'CN':
        return CN;
        case 'DAX':
        return DAX;
        case 'MHI':
        return MHI;
        case 'YM':
        return YM;
        case 'QM':
        return QM;
        case 'NQ':
        return NQ;
        case 'MDAX':
        return MDAX;
        case 'IF':
        return IF;
        case 'IH':
        return IH;
        case 'IC':
        return IC;
        case 'NK':
        return NK;
        case 'BP':
        return BP;
        case 'EC':
        return EC;
        case 'XAU':
        return XAU;
        case 'RBB':
        return RBB;
        case 'SR':
        return SR;
        case 'CU':
        return CU;
        case 'RB':
        return RB;
        case 'PP':
        return PP;
        case 'HC':
        return HC;
        case 'SM':
        return SM;
        case 'RU':
        return RU;
        case 'SC':
        return SC;
        case 'AG':
        return AG;
        case 'AU':
        return AU;
        case 'NG':
        return NG;
        case 'NI':
        return NI;
        case 'HSI':
        return HSI;
    }
}

//todo 行情模块
class Position extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        // console.log(this.props.array)
        return (
            <ul>
                {
                    this.props.array.map((item, key) => {
                        if(item.isOpen){
                        return (
                            <Link key={key} to={{
                                pathname: '/trade',
                                state: {
                                    code: item.code,
                                    contract: item.code,
                                    name: item.name,
                                    goodsCode: item.goodsCode,
                                    simulate:false,
                                }
                            }}>
                                <div className={'box'}>
                                    <div className={'code'}>
                                        <Svg path={goods(item.goodsCode)}/>
                                        {/* <div>{item.id}</div> */}
                                    </div>
                                    <div className={'name'}>
                                        <div>{item.name}</div>
                                        <p>{item.code}</p>
                                    </div>
                                    
                                    <div className={'number'}>
                                        {/* <div className={`price ${item.isUp ? 'red' : 'green'}`}>
                                            {item.price || '- -'}
                                        </div> */}
                                        <div className={item.isUp ? 'red' : 'green'}>
                                            {item.price || '- -'}
                                        </div>
                                    </div>

                                    <div className={'codeimg'}>
                                        <div className={'change'}>
                                            <div
                                                className={item.isOpen ? item.isUp ? 'red' : 'green' : 'rest'}>
                                                {item.isOpen ? item.rate : '休市'}
                                            </div>
                                        </div>
                                        {
                                            item.isUp ? (
                                                <img src={redLine} alt=""/>
                                            ):(
                                                <img src={greenLine} alt=""/>
                                            )
                                        }
                                        <div className={`point ${item.isUp ? 'red' : 'green'}`}>
                                        {(Number(item.price)*0.01*Number((item.rate||'').split('%')[0])).toFixed(Contracts.total[item.code].priceDigit)}
                                        {/* {item.rate.split('%')[0]} */}
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        )
                    }
                    })
                }
                {
                    this.props.array.map((item, key) => {
                        if(!item.isOpen){
                        return (
                            <Link key={key} to={{
                                pathname: '/trade',
                                state: {
                                    code: item.code,
                                    contract: item.code,
                                    name: item.name,
                                    goodsCode: item.goodsCode,
                                    simulate:false,
                                }
                            }}>
                                <div className={'box'}>
                                    <div className={'code'}>
                                        <Svg path={goods(item.goodsCode)}/>
                                        {/* <div>{item.id}</div> */}
                                    </div>
                                    <div className={'name'}>
                                        <div>{item.name}</div>
                                        <p>{item.code}</p>
                                    </div>
                                    
                                    <div className={'number'}>
                                        {/* <div className={`price ${item.isUp ? 'red' : 'green'}`}>
                                            {item.price || '- -'}
                                        </div> */}
                                        <div className={'rest'}>
                                            {item.price || '- -'}
                                        </div>
                                    </div>

                                    <div className={'codeimg'}>
                                        <div className={'change'}>
                                            <div
                                                className={'rest'}>
                                                {item.isOpen ? item.rate : '休市'}
                                            </div>
                                        </div>
                                        <img src={xiu} alt=""/>
                                        {/* {
                                            item.isUp ? (
                                                <img src={redLine} alt=""/>
                                            ):(
                                                <img src={greenLine} alt=""/>
                                            )
                                        } */}
                                        <div className={'point rest'}>
                                        {(Number(item.price)*0.01*Number((item.rate||'').split('%')[0])).toFixed(Contracts.total[item.code].priceDigit)}
                                        {/* {item.rate.split('%')[0]} */}
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        )
                    }
                    })
                }
            </ul>
        )
    }
}