import React, {Component} from 'react';
import ReactSwipe from 'react-swipe';
import {Store} from "../../module/index";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            notices:[]
        }
    }

    render() {
        return (
            <div>
                <ReactSwipe className="noticesBox"
                            swipeOptions={{continuous: true, speed: 500, startSlide: 0, auto: 1500}}
                            key={this.state.notices.length}>
                    {
                        this.state.notices.map((item,key) => {
                            return (
                                <p key={key}>{item.title}</p>
                            )
                        })
                    }
                </ReactSwipe>
            </div>
        )
    }

    componentDidMount(){
        this.mounted = true;
        Store.homeNotices.get().then((data)=>{
            if(this.mounted){
                this.setState({
                    notices:data
                })
            }
        })
    }
    componentWillUnmount(){
        this.mounted = false;
    }
}