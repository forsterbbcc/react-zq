import React,{Component} from 'react'
import {Req} from './../../lib/index'
import Header from './../common/header4'
import cache from './../../module/cache'
import {Link} from 'react-router-dom'
import {Cache} from "../../module";
import {formatDate} from "../../lib/tool";
import AlertFunction from '../../lib/AlertFunction';
import ReactDOM from 'react-dom';

// import send from './../../images/svg/send.svg'

export default class App extends Component{
    mount = true;
    constructor(props){
        super(props);

        this.state = {
            content:'',
            conversation:[],
            idNumber:Cache.idNumber,
            gender:true,
            time:'',
            init:true,
            csList:[],
            newCsList:[],
            top:null,
            darkType:false
        }
    }

    componentWillMount(){
        this.setState({
            darkType:JSON.parse(localStorage.getItem('bgc'))
        })
    }

    componentDidMount() {
        if (Cache.isLogin()) {
            this.requestMessage();
        }
        if(Cache.idNumber){
            let num = this.state.idNumber.slice(16,17) % 2;
            num === 0 ? this.setState({gender:true}):this.setState({gender:false});
        }
    }

    componentWillUnmount(){
        this.mount = false;
        clearTimeout(this.timer);
        clearTimeout(this.timer0);
    }

    //todo 每秒刷新后 回到最底端
    componentDidUpdate(){
        const messagesContainer = ReactDOM.findDOMNode(this.view);
        if(messagesContainer.scrollTop===0&&this.state.init){
            this.timer0 = setTimeout(() => {
                messagesContainer.scrollTop = messagesContainer.scrollHeight;
                this.setState({
                    init:false
                })
            }, 300);
        }
    }

    scrollToBottom(){
        const messagesContainer = ReactDOM.findDOMNode(this.view);
        messagesContainer.scrollTop = messagesContainer.scrollHeight;
    };

    async requestMessage () {
        try {
            let result = await Req({
                url: 'api/home/kefu.htm',
                type: 'GET',
                data: {
                    action: 'more',
                    size: 50,
                    _: new Date()
                }
            });

            if (this.mount) {
                const csList0 = result.data.filter((item,index)=>{
                    return item.status === 4 || item.status === 3
                });
                const csList = csList0.map((item,index)=>{
                    return item.time.time
                })
                this.setState({
                    conversation:result.data,
                    csList
                });
                if(result.data.length!==this.state.conversation.length){
                    this.scrollToBottom()
                };
            }
        } catch (err) {}

        this.timer = setTimeout(() => {
            this.requestMessage()
        }, 1000);
    }

    async sendMessage () {

        if (this.state.content === '') {
            return;
        }

        try {
            let result = await Req({
                url: 'api/home/kefu.htm',
                type: 'POST',
                data: {
                    action: 'send',
                    content:this.state.content.replace(/[^\u4e00-\u9fa5a-zA-Z\d,\.，。?~？！= （）@ ￥ 《》|、：；]+/,'')
                }
            });
            if (this.mount){
                this.setState({content:''});
            }
        } catch (err) {
            if(err.errorMsg==='消息发送过快，请稍后再发'){
                AlertFunction({title: '提示', msg:'您的消息发送过快,请休息一下.'});
            }

            if(err.errorMsg === '内容不能为空'){
                AlertFunction({title: '提示', msg:'请输入规范字符'});
                this.setState({
                    content:''
                })
            }
        }

        this.timer = setTimeout(() => {
            this.requestMessage();
            this.scrollToBottom();
        }, 1000);
        this.timer0 = setTimeout(() => {
            this.scrollToBottom();
        }, 500);
    }


    renderItem(){
        if (cache.isLogin()) {
            let content = this.state.conversation.map((item,i)=>{
                if (item.status === 3 || item.status === 4) {
                    return(
                        <div className="wrap">
                            <p className={'time'}>{formatDate('y/m/d h:i:s',{date:item.time.time})}</p>
                            <div className={'cs-cs'} key={i}>
                                <div className="head"></div>
                                <div>
                                    <div>
                                        <label>{item.content}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }else{
                    return(
                        <div className="wrap">
                            <p className={'time'}>{formatDate('y/m/d h:i:s',{date:item.time.time})}</p>
                            <div className={'cs-user'} key={i}>
                                <div>
                                    <div>
                                        <div>
                                            <label>
                                                {item.content}
                                            </label>
                                        </div>
                                    </div>
                                    <div className="head"></div>
                                </div>
                            </div>
                        </div>
                    );
                }
            });
            return content;
        }else{
            return(
                <div className={'cs-cs'}>
                    {/*<img src={cs} alt=""/>*/}
                    <div>
                        <div>
                            <label>请先<Link to={'/login'}>登录</Link>平台账号,即可进行留言或在线咨询服务</label>
                        </div>
                    </div>
                </div>
            );
        }
    }

    render(){
        return(
            <div className={`cs ${this.state.darkType?'dark':''}`}>
                <Header title={'在线客服'} {...this.props}/>
                <div ref={view=>this.view = view} className={'cs-content'}>
                    <div className="wrap" ref={'wrap'}>
                        {this.renderItem()}
                    </div>
                </div>
                <div  className={'cs-input'}>
                    <input type="text" value={this.state.content} onChange={this.onChangeText.bind(this)} placeholder={'您好, 请问一下 ...'}/>
                    <div className={'svgWrapper'} onClick={()=>this.submit()}>发送</div>
                </div>
            </div>
        );
    }

    onChangeText(e){
        let text = e.target.value;
        if (this.mount){
            // console.log('666');
            this.setState({
                content:text
            });
        }
    }

    submit(){
        this.sendMessage();
    }
}
