import React, {Component} from 'react';
import {Redirect, Link} from 'react-router-dom';
import {Cache} from "../../module";
import {Svg} from "../common";
import {Header} from "../common";
import AlertFunction from "../../lib/AlertFunction";

//todo 引入图片

import back from '../../images/svg/back.svg';
import open from '../../images/svg/openEye.svg';
import close from '../../images/svg/closeEye.svg';

export default class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            redirectToReferrer: false,
            username: '',
            password: '',
            open: false
        };
    }

    render() {
        const {from} = this.props.location.state || {from: {pathname: '/'}};
        const {redirectToReferrer} = this.state;
        if (redirectToReferrer) {
            return <Redirect to={from}/>
        }
        return (
            <div className={'login'}>
                <Header {...this.props}/>
                <div className={'main'}>
                    <h4></h4>
                    <div className={'loginInfo'}>
                        <div className={'passwordBox'}>
                            <input type="tel" placeholder={'请输入手机号'} value={this.state.username} maxLength={11}
                                   onChange={(e) => {
                                       this.setState({username: e.target.value})
                                   }}/>
                        </div>
                    </div>
                    <div className={'loginInfo'}>
                        {
                            this.state.open ? (
                                <div className={'passwordBox'}>
                                    <input maxLength={16}
                                        type="text" placeholder={'字母和数字组合'} value={this.state.password}
                                           onChange={(e) => {
                                               this.setState({password: e.target.value.replace(/[^\a-\z\A-\Z0-9]/g,'')})
                                           }}/>
                                    <div onClick={() => this.setState({open: false})}>
                                        <Svg path={open}/>
                                    </div>
                                </div>
                            ) : (
                                <div className={'passwordBox'}>
                                    <input maxLength={16}
                                        type="password" placeholder={'字母和数字组合'} value={this.state.password}
                                           onChange={(e) => {
                                               this.setState({password: e.target.value.replace(/[^\a-\z\A-\Z0-9]/g,'')})
                                           }}/>
                                    <div onClick={() => this.setState({open: true})}>
                                        <Svg path={close}/>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                    <div className={'toregister'}>
                        <Link to={{pathname: '/register'}} className={'button'}>用户注册</Link>
                        <Link to={{pathname: '/recoverPassword'}} className={'forgot'}>忘记密码？</Link>
                    </div>
                    {/* <div className={'none'}>
                        <Link to={{pathname: '/cs'}} className={' button'}>在线客服</Link>
                    </div> */}
                    <div className={'buttonBox'}>
                        <div onClick={() => this.setLogin()}>登录</div>
                    </div>
                </div>
            </div>
        )
    }


    setLogin() {
        if (this.state.username.length === 0) return AlertFunction({title: '警告', msg: '请输入手机号'});
        if (this.state.username.length !== 11) return AlertFunction({title: '警告', msg: '请输入正确的手机号'});
        if (this.state.password.length === 0) return AlertFunction({title: '警告', msg: '请输入密码'});

        const {from} = this.props.location.state || {from: null};
        Cache.setLogin(this.state.username, this.state.password, () => {
            if (from === null) {
                window.history.back();
            } else {
                this.setState({redirectToReferrer: true})
            }
        })
    }

}