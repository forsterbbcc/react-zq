import React, {Component} from 'react';
import {Schedule} from "../../lib/index";
import {Contracts} from "../../module/index";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: '-',
            close: '-',
            rate: '-',
            percent: '-',
            max: '-',
            min: '-',
            high_limit: '-',
            low_limit: '-',
            settle_price: '-',
            settle_price_yes: '-',
            hold_volume: '-',
            volume: '-',
            tend: 'raise',
            maxTend: 'raise',
            minTend: 'raise'
        };
    }

    render() {
        return (
            <div className={`dynamic ${this.props.className}`}>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <span>开盘</span>
                            <span>{this.state.open}</span>
                        </td>
                        <td>
                            <span>昨收</span>
                            <span>{this.state.close}</span>
                        </td>
                        <td>
                            <span>涨跌</span>
                            <span className={this.state.tend}>{this.state.rate}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>涨幅</span>
                            <span className={this.state.tend}>{this.state.percent}</span>
                        </td>
                        <td>
                            <span>最高</span>
                            <span className={this.state.maxTend}>{this.state.max}</span>
                        </td>
                        <td>
                            <span>最低</span>
                            <span className={this.state.minTend}>{this.state.min}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>涨停</span>
                            <span>{this.state.high_limit}</span>
                        </td>
                        <td>
                            <span>跌停</span>
                            <span>{this.state.low_limit}</span>
                        </td>
                        <td>
                            <span>今结</span>
                            <span>{this.state.settle_price}</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>昨结</span>
                            <span>{this.state.settle_price_yes}</span>
                        </td>
                        <td>
                            <span>持仓</span>
                            <span>{this.state.hold_volume}</span>
                        </td>
                        <td>
                            <span>总手</span>
                            <span>{this.state.volume}</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }

    componentDidMount() {
        Schedule.addEventListener('quoteUpdate', this.updateQuote, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }

    updateQuote(e) {
        const {priceDigit} = Contracts.total[e.code];
        const prev = e.settle_price_yes || e.close;
        let rate = e.price.sub(prev);
        let percent = rate.div(prev);
        let tend = rate >= 0 ? 'raise' : 'fall';
        rate = `${rate >= 0 ? '+' : ''}${rate.toFixed(priceDigit)}`;
        percent = `${rate >= 0 ? '+' : ''}${percent.mul(100).toFixed(2)}%`;
        this.setState({
            open: e.open ? e.open.toFixed(priceDigit) : '-',
            close: e.close ? e.close.toFixed(priceDigit) : '-',
            rate: rate,
            percent: percent,
            max: e.max ? e.max.toFixed(priceDigit) : '-',
            min: e.min ? e.min.toFixed(priceDigit) : '-',
            high_limit: e.high_limit ? e.high_limit.toFixed(priceDigit) : '-',
            low_limit: e.low_limit ? e.low_limit.toFixed(priceDigit) : '-',
            settle_price: e.settle_price ? e.settle_price.toFixed(priceDigit) : '-',
            settle_price_yes: e.settle_price_yes ? e.settle_price_yes.toFixed(priceDigit) : '-',
            hold_volume: e.hold_volume,
            volume: e.volume,
            tend: tend,
            maxTend: e.max > e.settle_price_yes ? 'raise' : 'fall',
            minTend: e.min > e.settle_price_yes ? 'raise' : 'fall'
        })
    }
}