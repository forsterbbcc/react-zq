import React, {Component} from 'react';
import {Chart, Contracts, Data, Quotes} from "../../module/index";
import {Schedule, Req} from "../../lib/index";
import ReactSvg from 'react-svg';

import {Link} from 'react-router-dom';
import Header from './header';
import QuoteBar from './quoteBar';
import ChartBar from './chartBar';
import StateBar from './stateBar';
import Dynamic from './dynamic';
import Rule from '../rule/index';
import Order from '../order/index';

import return2 from '../../images/svg/return2.svg';

export default class App extends Component {
    _ref = null;
    constructor(props) {
        super(props);
        this._init = false;
        this.state = {
            select: false,
            dynamic: 'hide',
            foreignArray: [],
            domesticArray: [],
            stockArray: [],
            rule:false,
            order:false,
            fuck:'',
            title:['国际期货','股指期货','国内期货'],
            choice:0,
            choiceArr:[],
            rate:'',
            percent:'',
            foreignArr:[],
            domesticArr:[],
            stockArr:[],
            contract:this.props.location.state?this.props.location.state.contract:null,
            closeFast:false,
            darkType:false
        };
        let c = Contracts.getContract(this);
        if(!!c){
            this.state.foreignArray = Contracts.foreignArray;
            this.state.domesticArray = Contracts.domesticArray;
            this.state.stockArray = Contracts.stockArray;
            this._init = true;
        }
        this.order = this.order.bind(this);
    }
    componentWillMount(){
        this.setState({
            darkType:JSON.parse(localStorage.getItem('bgc'))
        })
    }
    change(i){
        this.setState({choice:i});
        i===0?this.setState({choiceArr:this.state.foreignArr}):null;
        i===2?this.setState({choiceArr:this.state.domesticArr}):null;
        i===1?this.setState({choiceArr:this.state.stockArr}):null
    }
    showRule(){
        this.setState({rule:true});
    }
    render() {
        // console.log(this.state.contract);
        return (
            <div className={`trade ${this.state.darkType?'dark':''}`}>
                {
                    // 切换商品界面
                    this.state.select ? (
                        <div className={'mask'}>
                            <div className={'space'} onClick={this.closeSelect.bind(this)}>
                                <div className="back">
                                    <ReactSvg path={return2} className={'iconStyle return2'} wrapperClassName={'footIconWrap'}/>
                                </div>
                            </div>
                            <div className={'list'}>
                                <ul>
                                    <li>
                                        <div className={'title'}>国际期货</div>
                                        <div className={'group'}>
                                            {
                                                this.state.foreignArray.map((c) => {
                                                    return (
                                                        <div onClick={() => this.closeSelect()}>
                                                            <Link to={{
                                                                pathname: '/trade',
                                                                state: {
                                                                    contract: c.contract,
                                                                    simulate: this.props.location.state.simulate
                                                                }
                                                            }}>
                                                                {c.name}
                                                            </Link>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </li>
                                    <li>
                                        <div className={'title'}>股指期货</div>
                                        <div className={'group'}>
                                            {
                                                this.state.stockArray.map((c) => {
                                                    return (
                                                        <div onClick={() => this.closeSelect()}>
                                                            <Link to={{
                                                                pathname: '/trade',
                                                                state: {
                                                                    contract: c.contract,
                                                                    simulate: this.props.location.state.simulate
                                                                }
                                                            }}>
                                                                {c.name}
                                                            </Link>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </li>
                                    <li>
                                        <div className={'title'}>国内期货</div>
                                        <div className={'group'}>
                                            {
                                                this.state.domesticArray.map((c) => {
                                                    return (
                                                        <div onClick={() => this.closeSelect()}>
                                                            <Link to={{
                                                                pathname: '/trade',
                                                                state: {
                                                                    contract: c.contract,
                                                                    simulate: this.props.location.state.simulate
                                                                }
                                                            }}>
                                                                {c.name}
                                                            </Link>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    ) : ''
                }
                <Header {...this.props} active={()=>this.setState({rule:true})}
                        updateTV={this.startTradingView.bind(this)}
                        changeDark={this.changeDark.bind(this)}
                />
                <QuoteBar {...this.props} openSelect={() => this.openSelect()}/>
                <ChartBar {...this.props}
                          dynamic={(status) => this.dynamic(status)}
                          showRule={this.showRule.bind(this)}
                          close={()=>this.setState({order:false})}
                />
                <div className={'chart'}>
                    <Dynamic className={this.state.dynamic}/>
                    <div id={'tradingView'} ref={(e)=>this._ref = e}/>
                </div>
                <StateBar {...this.props} order={this.order} closeFast={this.state.closeFast}/>
                <Rule show={this.state.rule} dark = {this.state.darkType}
                close={()=>this.setState({rule:false})} {...this.props}/>
                <Order show={this.state.order}
                       close={()=>this.setState({order:false})}
                       dark = {this.state.darkType}
                       {...this.props}/>
            </div>
        )
    }

    componentDidMount() {
        Schedule.addEventListener('updateBrief', this.updateBrief, this);
        if(this._init){
            Quotes.start('quoteUpdate',this.props.location.state.contract);
            this.startTradingView();
        } else {
            let c = Contracts.getContract(this);
            if(!!c){
                Quotes.start('quoteUpdate', c);
                this.setState({
                    foreignArray: Contracts.foreignArray,
                    domesticArray: Contracts.domesticArray,
                    stockArray: Contracts.stockArray
                });
                this.startTradingView();
            }else {
                Schedule.addEventListener('contractsInitial', this.updateContracts, this);
                Schedule.addEventListener('contractsInitial', this.startTradingView, this);
            }

        }
    }
    changeDark(){
        //更新护眼模式
        this.setState({
            darkType:JSON.parse(localStorage.getItem('bgc'))
        });
    }
    componentWillUnmount() {
        Chart.exit();
        Quotes.end();
        Schedule.removeEventListeners(this);
        Data.end('updateBrief');
    }

    dynamic(status) {
        this.setState({dynamic: status})
    }

    openSelect() {
        this.setState({select: !this.state.select})
    }

    closeSelect() {
        this.setState({select: false})
    }

    updateContracts() {
        let c = Contracts.getContract(this);
        if(!!c){
            Quotes.start('quoteUpdate', c);
            this.setState({
                foreignArray: Contracts.foreignArray,
                domesticArray: Contracts.domesticArray,
                stockArray: Contracts.stockArray
            });
        };
        this.setState({
            foreignArr: Data.foreignBrief,
            stockArr: Data.stockBrief,
            domesticArr: Data.domesticBrief,
            choiceArr:Data.foreignBrief,
            // isUpData : Data.total[this.props.location.state?this.props.location.state.contract:this.state.contract]
    });
        Data.start('updateBrief');
    }
    updateBrief() {
        this.setState({
            foreignArr: Data.foreignBrief,
            stockArr: Data.stockBrief,
            domesticArr: Data.domesticBrief,
            // isUpData : Data.total[this.props.location.state?this.props.location.state.contract:this.state.contract]
        });
        // this.startTradingView();
        //更新护眼模式
        this.setState({
            darkType:JSON.parse(localStorage.getItem('bgc'))
        });
    }

    startTradingView() {
        const code = this.state.contract?Contracts.total[this.state.contract].code:Contracts.total[this.props.location.state.contract].code;
        // console.log(code,'code',this.state)
        Chart.init({
            dom: 'tradingView',
            code: code,
            height:this._ref.scrollHeight,
            width:this._ref.scrollWidth,
            darkType:JSON.parse(localStorage.getItem('bgc'))
            // isUp:this.state.isUpData.isUp
        });
    }

    order(isBuy){
        this.props.location.state.isBuy = isBuy;
        this.setState({
            order:true
        })
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.contract !== nextProps.location.state.contract) {
            this.setState({
                contract:nextProps.location.state.contract
            });
            Quotes.switch(nextProps.location.state.contract);
            const code = Contracts.total[nextProps.location.state.contract].code;
            Chart.swap({code: code});
            //更新护眼模式
            // this.setState({
            //     darkType:JSON.parse(localStorage.getItem('bgc'))
            // },()=>{
            //     Chart.changeBackground(!!this.state.darkType);
            // });
        }
    }
}