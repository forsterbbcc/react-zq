import React,{Component} from 'react';
import {Link,NavLink} from 'react-router-dom';
import ReactSvg from 'react-svg';
import back from '../../images/svg/back.svg';
// import dark from '../../images/svg/dark.svg';
// import sun from '../../images/svg/sun.svg';


export default class App extends Component{
    constructor(props){
        super(props);
        this.state = {
            simulate:this.props.location.state.simulate,
            openDark:false
        };
        const bgc = JSON.parse(localStorage.getItem('bgc'));
        if(!!bgc){
            this.state.openDark = bgc
        }
    }
    render(){
        return(
            <nav className={'t-header'}>
                <Link className={'backTag'} to={'/'}>
                    <ReactSvg path={back} className={'back'} wrapperClassName={'backWrap'}/>
                </Link>
                <div className={'group'}>
                    <div className={'cell'}>
                        <NavLink className={'order'} to={{pathname:'/trade',state:this.props.location.state}}>{this.state.simulate?'模拟':'实盘'}下单</NavLink>
                        <NavLink to={{
                            pathname:'/position',state:this.props.location.state,where:'tradePage'
                        }}
                                 isActive={(match,location)=>(location.pathname.indexOf('/position') !== -1)}>
                            {this.state.simulate?'模拟持仓':'实盘持仓'}
                        </NavLink>
                    </div>
                </div>
                <div className={'rule'} onClick={()=>this.props.active()}>规则</div>
                {/* <div className="darkWrap" onClick={()=>this.setDark()}>
                    <ReactSvg path={this.state.openDark?dark:sun} className={'dark0'} wrapperClassName={'darkWrap'}/>
                </div> */}
            </nav>
        )
    }
    setDark(){
        this.setState({openDark:!this.state.openDark},()=>{
            if(this.state.openDark){
                localStorage.setItem('bgc','true');
            }else{
                localStorage.setItem('bgc','false');
            }
            console.log(this.state.openDark);
            this.props.updateTV();
            this.props.changeDark();
        });
    }
    componentWillReceiveProps(nextProps) {
        if(this.props.location.state.simulate !== nextProps.location.state.simulate){
            this.setState({simulate:nextProps.location.state.simulate})
        }
    }
}