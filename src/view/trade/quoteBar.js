import React, {Component} from 'react';
import {Chart, Contracts, Quotes} from "../../module";
import {Schedule} from "../../lib";
import ReactSvg from 'react-svg';

import triangle from '../../images/svg/triangle.svg'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tend: 'raise',
            name: '获取中',
            code: '',
            price: '',
            rate: '',
            percent: '',
        };
        let c = Contracts.getContract(this);
        if(!!c){
            this.state.name = Contracts.total[c].name;
            this.state.code = c;
        }else{
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }

    render() {
        return (
            <div className={`quoteBar`}>
                <div className={'left'} onClick={this.props.openSelect}>{this.state.name}&nbsp;&nbsp;{this.state.code}&nbsp;&nbsp;
                    <div className={'triangle'}>
                        <ReactSvg path={triangle} className={'iconStyle home'} wrapperClassName={'footIconWrap'}/>
                    </div>
                </div>
                <div className={`right ${this.state.tend}`}>{this.state.price}&nbsp;&nbsp;{this.state.rate}&nbsp;&nbsp;{this.state.percent}</div>
            </div>
        )
    }

    componentDidMount() {
        Schedule.addEventListener('quoteUpdate', this.updateQuote, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }

    updateContracts() {
        let c = Contracts.getContract(this);
        if(!!c){
            this.setState({
                name: Contracts.total[c].name,
                code: c
            });
        }
    }

    updateQuote(e) {
        const {priceDigit} = Contracts.total[e.code];
        const prev = e.settle_price_yes || e.close;
        let rate = e.price.sub(prev);
        let percent = rate.div(prev);
        let tend = rate >=0?'raise':'fall';
        rate = `${rate >= 0 ? '+' : ''}${rate.toFixed(priceDigit)}`;
        percent = `${rate >= 0 ? '+' : ''}${percent.mul(100).toFixed(2)}%`
        this.setState({
            price: e.price.toFixed(priceDigit),
            rate: rate,
            percent: percent,
            tend:tend
        })
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location.state.contract !== nextProps.location.state.contract) {
            const o = Contracts.total[nextProps.location.state.contract];
            this.setState({
                name: o.name,
                code: o.contract,
                price: '',
                rate: '',
                percent: ''
            });
        }
    }
}