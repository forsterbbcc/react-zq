import React,{Component} from 'react';
import {Chart} from "../../module/index";

export default class App extends Component{
    team = [
        {
            name:'分时',
            key:'sline'
        },
        {
            name:'日线',
            key:'1D'
        },
        {
            name:'1分',
            key:'1'
        },
        {
            name:'5分',
            key:'5'
        },
        {
            name:'15分',
            key:'15'
        },
        {
            name:'盘口',
            key:'dynamic'
        }
    ];
    constructor(props){
        super(props);
        this.state = {
            select:'sline'
        }
    }
    render(){
        return(
            <div className={'chartBar'}>
                <ul>
                    {
                        this.team.map(({name,key})=>{
                            return(
                                <li>
                                    <div className={this.state.select===key?'active':''} onClick={()=>{this.selectType(key)}}>{name}</div>
                                </li>
                            )
                        })
                    }
                </ul>
                {/*<div className="rule" onClick={()=>{this.props.showRule();this.props.close()}}>*/}
                    {/*<span>规则</span>*/}
                {/*</div>*/}
            </div>
        )
    }
    selectType(key){
        if(key !== 'dynamic'){
            if(!Chart.table){
                return;
            }
            this.props.dynamic('hide');
            Chart.swap({type:key});
        }else{
            this.props.dynamic('show')
        }
        this.setState({select:key});
    }
}