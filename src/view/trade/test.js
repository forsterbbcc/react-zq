import React, {Component} from 'react';
import ReactSvg from 'react-svg';
import {Link} from 'react-router-dom';

import loading from '../../images/gif/loading.gif';
import flash from '../../images/svg/flash.svg';

import {Schedule, Req} from "../../lib/index";
import {Contracts, Cache, Rest} from "../../module/index";
import {formatDate, getCloseTime, getIdentity, getPlatform} from "../../lib/tool";
import AlertFunction from "../../lib/AlertFunction";
import {Switch} from "../common/index";
import List from "../order/list";


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasFastTrade: Cache.tradeQuick,
            fastTrade:false,
            rest: false,
            isLogin: null,
            balance: this.props.location.state.simulate ? Cache.gameBalance : Cache.realBalance,
            simulate: this.props.location.state.simulate,
            moneyType:0,
            hasMoneyType: 1,
            buyVolume: 0,
            buyPrice: 0,
            buyWidth: 1,
            sellVolume: 0,
            sellPrice: 0,
            sellWidth: 1,
            stopLossIndex: 0,
            stopLoss: [0],
            stopProfit: [0],
            volumeIndex: 0,
            volume: [0],
            buyPercent:0,
            sellPercent:0,
            chargeUnit: 0,
            newArr0:['高级']
        };
        if (Cache.initial) {
            this.state.isLogin = Cache.isLogin()
        }
        if (!!Cache.tradeList) {
            this.updateSchemeInfo(true);
        }
        let c = Contracts.getContract(this);
        if (!!c) {
            this.state.hasMoneyType = Contracts.total[c].moneyType;
            const newArr0 = this.state.newArr0;
            this.state.hasMoneyType===3||this.state.hasMoneyType===1?newArr0.push('标准'):null;
            this.state.hasMoneyType===3||this.state.hasMoneyType===2?newArr0.push('迷你'):null;
            this.state.newArr0 = newArr0;
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }
    render() {
        const charge = this.state.chargeUnit.mul(this.state.volume[this.state.volumeIndex]).mul(this.state.moneyType === 0 ? 1 :(this.state.moneyType === 1? 0.1: 0.01));
        return (
            <div className={`stateBar
            ${this.state.rest?(''):(this.state.fastTrade?'grow':'')}
            ${this.props.location.state.simulate?'short':''}
            `}>
                {/*top部分开始*/}
                {
                    this.state.rest?(null):(
                        this.state.isLogin?(
                            this.state.fastTrade?(
                                <div className={'rare'}>
                                    <ul>
                                        {this.state.simulate?(
                                            <li className={'double'}>
                                                <div className="top">
                                                    <span>模拟余额</span>
                                                    <div className="right sim">
                                                        <span>{this.state.balance}元</span>
                                                        <div className={'btn'}  onClick={() => this.addSimBalance()}>
                                                            一键加币
                                                        </div>
                                                        <Link to={{
                                                            pathname: '/trade',
                                                            state: {simulate: false, contract: this.props.location.state.contract}
                                                        }}
                                                              className={'btn alignment'}
                                                            // onClick={()=>this.setState({fastTrade:false})}
                                                        >
                                                            切换实盘
                                                        </Link>
                                                    </div>
                                                </div>
                                            </li>
                                        ):(
                                            <li>
                                                <span>账户余额</span>
                                                <div className="right">
                                                    <span>{this.state.balance}元</span>
                                                    <Link to={'/recharge'} className={'btn'}>
                                                        充值
                                                    </Link>
                                                </div>
                                            </li>
                                        )}
                                        {this.state.simulate?(null):(
                                            <li>
                                                <span>交易模式</span>
                                                <div className="right">
                                                    {
                                                        this.state.newArr0.map((item,index)=>{
                                                            return (
                                                                <p
                                                                    key={index}
                                                                    className={`volume ${this.state.moneyType===index?'active':''}`}
                                                                    onClick={()=>this.setState({moneyType:index})}
                                                                >{item}</p>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            </li>
                                        )}
                                        <li>
                                            <span>交易手数</span>
                                            <div className="right">
                                                {this.state.volume.map((item,index)=>{
                                                    return (
                                                        <p onClick={()=>this.setState({volumeIndex:index})}
                                                           key={index}
                                                           className={`volume ${this.state.volumeIndex===index?'active':''}`}>{item}手
                                                        </p>
                                                    )
                                                })}
                                            </div>
                                        </li>
                                        <li>
                                            <span>保证金</span>
                                            <div className="right">
                                                <List className={'btn-list pad-left'} list={this.state.stopLoss}
                                                      plus={this.state.volume[this.state.volumeIndex].mul(this.state.moneyType === 0 ? 1 :(this.state.moneyType === 1? 0.1 : 0.01))}
                                                      select={this.state.stopLossIndex}
                                                      unit={'元'}
                                                      pick={(key) => this.setState({stopLossIndex: key})}/>
                                            </div>
                                        </li>
                                        <li>
                                            <span>交易综合费</span>
                                            <div className="right">
                                                &nbsp;{charge}元
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            ):(null)
                        ):(null)
                    )
                }
                {/*按钮部分开始*/}
                {/* 买涨部分 */}
                <div className={'controller'}>
                    {
                        this.state.rest?(null):(
                            this.state.isLogin? (
                                <div className={`fastBtn ${this.state.fastTrade ? 'open' : 'close'}`}>
                                    <span>闪电交易</span>
                                    <Switch switch={this.state.fastTrade} usable={true}  tips={'test'} onChange={()=>this.switchFastTrade()}/>
                                </div>
                            ) : '')
                    }
                    {
                        this.state.rest?(
                            <div className={'rest'}>休市中,</div>
                        ):(
                            this.state.fastTrade ?
                                //是否快速模式
                                this.state.simulate?(
                                    this.state.moneyType===0?(
                                        <div className={'raise'}
                                            // 快速模式的买涨
                                             onClick={() => this.fastTrade(true)}><span>{this.state.buyPrice}&nbsp;&nbsp;涨</span>
                                        </div>
                                    ):(
                                        <div className={'raise'}>
                                            <span>{this.state.buyPrice}&nbsp;&nbsp;涨</span>
                                        </div>
                                    )
                                ):(
                                    <div className={'raise'}
                                        // 快速模式的买涨
                                         onClick={() => this.fastTrade(true)}><span>{this.state.buyPrice}&nbsp;&nbsp;涨</span>
                                    </div>
                                )
                                :
                                (
                                    this.state.isLogin ? (
                                        // 是否登录
                                        // 正常模式登录的买涨
                                        <div className={'raise'} onClick={() => this.props.order(true)}>
                                            <span>{this.state.buyPrice}&nbsp;&nbsp;涨</span>
                                        </div>
                                    ) : (
                                        //正常模式未登录的买涨
                                        <Link to={'/login'} className={'raise'}><span>{this.state.buyPrice}&nbsp;&nbsp;涨</span></Link>
                                    )
                                )
                        )
                    }
                    {
                        // 买跌部分
                        //快速交易的买跌
                        this.state.rest?(
                            <div className="rest">去看看其他的吧.</div>
                        ):(
                            this.state.fastTrade ?
                                this.state.simulate?(
                                    this.state.moneyType===0?(
                                        <div className={'fall'} onClick={() => this.fastTrade(false)}>
                                            <span>{this.state.sellPrice}&nbsp;&nbsp;跌</span>
                                        </div>
                                    ):(
                                        <div className={'fall'}>
                                            <span>{this.state.sellPrice}&nbsp;&nbsp;跌</span>
                                        </div>
                                    )
                                ):(
                                    <div className={'fall'} onClick={() => this.fastTrade(false)}>
                                        <span>{this.state.sellPrice}&nbsp;&nbsp;跌</span>
                                    </div>
                                )
                                :
                                (this.state.isLogin ? (
                                        // 是否登录
                                        // 正常模式已登录的买跌
                                        <div className={'fall'} onClick={() => this.props.order(false)}>
                                            <span>{this.state.sellPrice}&nbsp;&nbsp;跌</span>
                                        </div>
                                    ) : (
                                        //正常模式的未登录
                                        <Link to={'/login'} className={'fall'}><span>{this.state.sellPrice}&nbsp;&nbsp;跌</span></Link>
                                    )
                                )
                        )

                    }
                </div>
            </div>
        )
    }
    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback()
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this);
        }
        if (this.state.stopLoss.length === 1 && !!Cache.tradeList) {
            this.updateSchemeInfo();
        } else {
            Schedule.addEventListener('getSchemeInfo', this.updateSchemeInfo, this);
        }
        Schedule.addEventListener('loginCallback', this.loginCallback, this);
        Schedule.addEventListener('quoteUpdate', this.updateQuote, this);
        Schedule.addEventListener('getUserInfo', this.updateUserInfo, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }

    componentWillReceiveProps(nextProps) {
        if(!!this.props.location.state.simulate){
            this.setState({
                moneyType:0
            })
        }
        if (this.props.location.state.simulate !== nextProps.location.state.simulate) {
            this.setState({
                simulate: nextProps.location.state.simulate,
                balance: nextProps.location.state.simulate ? Cache.gameBalance : Cache.realBalance
            });
        }
        if (this.props.location.state.contract !== nextProps.location.state.contract) {
            const o = Contracts.total[nextProps.location.state.contract];
            this.setState({
                hasMoneyType: o.moneyType,
                moneyType: 0
            });
            if (Cache.isLogin()) {
                let o = nextProps.location.state.contract;
                if (!o) {
                    [o] = Contracts.foreignArray;
                    o = o.contract;
                }
                let scheme = Cache.tradeList[o];
                this.setState({
                    // hasFastTrade: Cache.tradeQuick,
                    volume: scheme.volumeList,
                    stopLoss: scheme.stopLossList,
                    stopProfit: scheme.stopProfitList,
                    chargeUnit: scheme.chargeUnit
                })
            };
            // this.updateContracts();
        }
    }

    switchFastTrade() {
        this.setState({fastTrade: !this.state.fastTrade})
    }

    switchMoneyType(v) {
        this.setState({moneyType: v})
    }

    /**
     * 更新成交量及价格
     * @param e
     */
    updateQuote(e) {
        const {priceDigit} = Contracts.total[e.code];
        const total = e.wt_buy_volume.add(e.wt_sell_volume);
        this.setState({
            rest: !Rest.isOpening(Contracts.total[e.code]),
            buyPrice: e.wt_sell_price.toFixed(priceDigit),
            buyVolume: e.wt_buy_volume,
            buyWidth: e.wt_buy_volume.div(total).mul(80),
            sellPrice: e.wt_buy_price.toFixed(priceDigit),
            sellVolume: e.wt_sell_volume,
            sellWidth: e.wt_sell_volume.div(total).mul(80),
            buyPercent:e.wt_buy_volume/(e.wt_buy_volume+e.wt_sell_volume)*100+'%',
            sellPercent:e.wt_sell_volume/(e.wt_buy_volume+e.wt_sell_volume)*100+'%'
        });
        this.updateContracts();
    }

    /**
     * 更新合约支持交易货币类型  元/角
     */
    updateContracts() {
        let c = Contracts.getContract(this);
        if (!!c) {
            this.setState({
                hasMoneyType: Contracts.total[c].moneyType
            });
            if(this.state.hasMoneyType===3){
                this.setState({
                    newArr0:['高级','标准','迷你']
                })
            }else if(this.state.hasMoneyType===2){
                this.setState({
                    newArr0:['高级','迷你']
                })
            }else if(this.state.hasMoneyType===1){
                this.setState({
                    newArr0:['高级','标准']
                })
            }
        }
    }

    /**
     * 更新购买信息以及是否有快速交易
     */
    updateSchemeInfo(init) {
        let c = Contracts.getContract(this);
        let scheme = Cache.tradeList[c];
        if (init) {
            this.state.hasFastTrade = Cache.tradeQuick;
            this.state.volume = scheme.volumeList;
            this.state.stopLoss = scheme.stopLossList;
            this.state.stopProfit = scheme.stopProfitList;
            this.state.chargeUnit = scheme.chargeUnit;
        } else {
            this.setState({
                hasFastTrade: Cache.tradeQuick,
                volume: scheme.volumeList,
                stopLoss: scheme.stopLossList,
                stopProfit: scheme.stopProfitList,
                chargeUnit: scheme.chargeUnit
            })
        }
    }

    /**
     * 更新用户信息
     */
    updateUserInfo() {
        this.setState({
            balance: this.state.simulate ? Cache.gameBalance : Cache.realBalance
        })
    }

    /**
     * 登录回调
     */
    loginCallback() {
        this.setState({
            isLogin: Cache.isLogin(),
            hasFastTrade: Cache.tradeQuick
        })
    }

    async addSimBalance() {
        try {
            const result = await Req({
                url: '/api/trade/addScore.htm',
                animate: true
            });
            Cache.getUserInfo();
            AlertFunction({title: '提示', msg: result.resultMsg});
        } catch (err) {
            AlertFunction({title: '错误', msg: err.resultMsg});
        }
    }

    /**
     * 快速交易
     */
    async fastTrade(isBuy) {
        try {
            const o = this.props.location.state.contract;
            const obj = Contracts.total[o];
            const scheme = Cache.tradeList[o];
            const handle = this.state.volume[this.state.volumeIndex];
            const result = await Req({
                url: '/api/trade/open.htm',
                type: 'POST',
                data: {
                    identity: getIdentity(16),
                    tradeType: this.props.location.state.simulate ? 2 : 1,//模拟交易2 实盘交易1
                    source: '下单',  // 买入来源（下单、反向、快捷)
                    commodity: obj.code,
                    contract: o,
                    isBuy: isBuy,
                    price: 0,
                    stopProfit: this.state.stopProfit[this.state.stopLossIndex].mul(handle).mul(this.state.moneyType === 0 ? 1 :(this.state.moneyType === 1? 0.1 : 0.01)),
                    stopLoss: this.state.stopLoss[this.state.stopLossIndex].mul(handle).mul(this.state.moneyType === 0 ? 1 :(this.state.moneyType === 1? 0.1 : 0.01)),
                    serviceCharge: scheme.chargeUnit.mul(handle).mul(this.state.moneyType === 0 ? 1 :(this.state.moneyType === 1? 0.1 : 0.01)),
                    eagleDeduction: 0,
                    volume: handle,
                    moneyType: this.state.moneyType,
                    platform: getPlatform()
                },
                animate: true
            });
            AlertFunction({title: '提示', msg: result.errorMsg});
            Cache.getUserInfo();
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg});
        }
    }

}