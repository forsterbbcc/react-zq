import React, {Component} from 'react';
import {Header, SelectBank} from "../../common";
import {Cache} from "../../../module";
import {Schedule} from "../../../lib";
import {nameMark} from "../../../lib/tool";
import ReactSvg from 'react-svg'
import {provinceData, cityeData} from "../../../lib/address";
import {Req} from "../../../lib";

import back from '../../../images/svg/back.svg'
import arrow from '../../../images/svg/right-arrow.svg'
import AlertFunction from "../../../lib/AlertFunction";
import {LinkTo} from "../../../lib/native";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            realName: Cache.realName,
            bankType: '请选择银行',
            bankId: '',
            idNum: '',
            select: 0,  //todo 0 未选择  1 选择银行  2 选择省份  3 选择城市
            mobile:'',
            cardNumber:'',
            cardNumberCfm:'',
            step:0,
            accountName:'',
            smsCode:''
        };

        this.selectBank = this.selectBank.bind(this)
    }

    //不需要的绑定银行卡时输入的验证码
    // renderVerification() {
    //     return (
    //         <div className={'addBank'}>
    //             <Header title={'银行卡快捷支付'} {...this.props}/>
    //             <div className={'main'}>
    //                 <h4>请输入银行卡注册手机号接收到的短信验证码</h4>
    //                 <ul>
    //                     <li>
    //                         <span>验证码</span>
    //                         <input type="text" placeholder={'请输入短信验证码'} value={this.state.smsCode} onChange={(e)=>this.setState({smsCode:e.target.value})} minLength={16} maxLength={19}/>
    //                     </li>
    //                 </ul>
    //                 <div className={'submit'} onClick={()=>this.submitAdd()}>确定</div>
    //             </div>
    //             {
    //                 this.state.select === 1 ? (
    //                     <SelectBank selectBank={this.selectBank}/>
    //                 ) : (null)
    //             }
    //         </div>
    //     )
    // }


    render() {
        return (
            <div className={'addBank'}>
                {/*<Header title={'银行卡快捷支付'} {...this.props}/>*/}
                <div className={'main'}>
                    {/*  <h4>为确保资金安全，只能添加"{nameMark(this.state.realName)}"的银行卡</h4>*/}
                    <ul>
                        <li>
                            <span>开户银行</span>
                            <div onClick={() => this.setState({select:1})}>{this.state.bankType}</div>
                        </li>
                        <li>
                            {/*<span>开户姓名</span>
                            <div>{nameMark(this.state.realName)}</div>*/}
                            <span>账户名</span>
                            <input type="text" placeholder={'请输入开户人真实姓名'} value={this.state.accountName} onChange={(e)=>this.setState({accountName:e.target.value})} minLength={2} maxLength={20}/>
                        </li>
                        <li>
                            <span>身份证号</span>
                            <input type="text" placeholder={'请输入身份证号'} value={this.state.idNum} onChange={(e)=>this.setState({idNum:e.target.value})} minLength={18} maxLength={18}/>
                        </li>
                        <li>
                            <span>银行卡号</span>
                            <input type="tel" placeholder={'请输入银行卡号'} value={this.state.cardNumber} onChange={(e)=>this.setState({cardNumber:e.target.value})} minLength={16} maxLength={19}/>
                        </li>
                        <li>
                            <span>确认卡号</span>
                            <input type="tel" placeholder={'请在此确认银行卡号'} value={this.state.cardNumberCfm} onChange={(e)=>this.setState({cardNumberCfm:e.target.value})} minLength={16} maxLength={19}/>
                        </li>
                        <li>
                            <span>手机号</span>
                            <input type="tel" placeholder={'请输入手机号'} value={this.state.mobile} onChange={(e)=>this.setState({mobile:e.target.value})} minLength={11} maxLength={11}/>
                        </li>
                    </ul>
                    <div className={'submit spe'} onClick={()=>this.props.hide()}>取消</div>
                    <div className={'submit spe'} onClick={()=>this.submitBind()}>确定</div>
                </div>
                {
                    this.state.select === 1 ? (
                        <SelectBank selectBank={this.selectBank}/>
                    ) : (null)
                }
            </div>
        )
    }
    loginCallback() {
        this.setState({isLogin: Cache.isLogin(), realName: Cache.realName})
    }

    //todo 选择银行
    selectBank(bank, type){
        this.setState({
            bankType:bank,
            select:type,
            bankId:this.getBankId(bank)
        });
    }

    submitBind(){
        if(this.state.bankType.length === 0 || this.state.bankType === '请选择银行') return AlertFunction({title:'警告', msg:'请选择开户银行'});
        if(this.state.cardNumber.length === 0 || this.state.cardNumber.length < 16 ) return AlertFunction({title:'警告', msg:'银行卡格式不正确'});
        if(this.state.cardNumberCfm.length === 0 || this.state.cardNumberCfm.length < 16 ) return AlertFunction({title:'警告', msg:'银行卡格式不正确'});
        if(this.state.accountName.length === 0  ) return AlertFunction({title:'警告', msg:'请输入账户名'});
        if(this.state.idNum.length === 0 || this.state.idNum.length < 18 ) return AlertFunction({title:'警告', msg:'身份证格式不正确'});
        if(this.state.mobile.length === 0 || this.state.mobile.length < 11 ) return AlertFunction({title:'警告', msg:'手机号格式不正确'});
        const bankInfoArr={
            bank: this.state.bankType,
            accountName: this.state.accountName,
            cardNumber: this.state.cardNumber,
            idNum: this.state.idNum,
            mobile: this.state.mobile
        };
        // this.props.history.push({
        //     pathname:'/',
        //     name:'harley'
        // })
        this.props.selectBank(bankInfoArr);
        // Req({
        //     url: '/api/mine/quickBankCardAdd.htm',
        //     type: 'POST',
        //     data: {
        //         action: 'bind',
        //         bank: this.state.bankType,
        //         bankId:this.state.bankId,
        //         accountName: this.state.accountName,
        //         idNum: this.state.idNum,
        //         cardNumber: this.state.cardNumber,
        //         cardNumberCfm: this.state.cardNumberCfm,
        //         mobile: this.state.mobile,
        //         payName:'YIDIAN_PAY'
        //     },
        //     animate:true,
        //     timeout:15000
        // }).then((data) => {
        //     if(data.success==true && data.bindNum!=null){
        //         this.setState({
        //             step: 1,
        //             bindNum:data.bindNum
        //         },()=>{
        //
        //         })
        //     }else {
        //         AlertFunction({title: '错误', msg: "网络异常，请重试"})
        //     }
        // }).catch((err) => {
        //     console.log(111);
        //     AlertFunction({title: '错误', msg: err.errorMsg})
        // })
    }

    // //todo 验证码添加银行卡到列表
    // submitAdd(){
    //     if(this.state.bankType.length === 0 || this.state.bankType === '请选择银行') return AlertFunction({title:'警告', msg:'请选择开户银行'});
    //     if(this.state.cardNumber.length === 0 || this.state.cardNumber.length < 16 ) return AlertFunction({title:'警告', msg:'银行卡格式不正确'});
    //     if(this.state.accountName.length === 0  ) return AlertFunction({title:'警告', msg:'请输入账户名'});
    //     if(this.state.idNum.length === 0 || this.state.idNum.length < 18 ) return AlertFunction({title:'警告', msg:'身份证格式不正确'});
    //     if(this.state.mobile.length === 0 || this.state.mobile.length < 11 ) return AlertFunction({title:'警告', msg:'手机号格式不正确'});
    //     if(this.state.smsCode.length === 0  ) return AlertFunction({title:'警告', msg:'请输入验证码'});
    //
    //     Req({
    //         url: '/api/mine/quickBankCardAdd.htm',
    //         type: 'POST',
    //         data: {
    //             action: 'add',
    //             bank: this.state.bankType,
    //             bankId:this.state.bankId,
    //             accountName: this.state.accountName,
    //             idNum: this.state.idNum,
    //             cardNumber: this.state.cardNumber,
    //             mobile: this.state.mobile,
    //             bindNum: this.state.bindNum,
    //             smsCode: this.state.smsCode,
    //             payName:'YIDIAN_PAY'
    //         },
    //         animate:true,
    //         timeout:15000
    //     }).then((data) => {
    //         if(data.success==true  ){
    //             //LinkTo(data.redirectURL)
    //             window.history.back()
    //         }else {
    //             AlertFunction({title: '错误', msg: "网络异常，请重试"})
    //         }
    //     }).catch((err) => {
    //         console.log(222);
    //         AlertFunction({title: '错误', msg: err.errorMsg})
    //     })
    // }



    getBankId(name){
        switch (name) {
            case '工商银行':
                return "ICBC";
            case '招商银行':
                return "CMB";
            case '建设银行':
                return "CCB";
            case '农业银行':
                return "ABC";
            case '中国银行':
                return "BOC";
            case '交通银行':
                return "COMM";
            case '民生银行':
                return "CMBC" ;
            case '浦发银行':
                return "SPDB";
            case '中信银行':
                return "CITIC";
            case '广发银行':
                return "CGB";
            case '平安银行':
                return "PINGANBK";
            case '兴业银行':
                return "CIB";
            case '华夏银行':
                return "HXB";
            case '光大银行':
                return "CEB" ;
            case '邮政储蓄':
                return "PSBC";
        }
    }

}