import React, {Component} from 'react';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value:''
        }
    }

    render() {
        return (
            <li className={'row'}>
                <label>{this.props.title}</label>
                <input className={'input'} type={this.props.style} placeholder={this.props.placeholder} maxLength={this.props.length} onChange={(e) => {
                    this.setState({value: e.target.value});
                    this.props.callback(e.target.value);
                }}/>
            </li>
        )
    }
}