import React, {Component} from 'react';
import {Header, Balance} from "../common";
import {Req} from "../../lib";
import AlertFunction from "../../lib/AlertFunction";
import Input from './input';
import Option from './option';
import Verify from "./verify";
import {LinkTo} from "../../lib/native";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.config = {
            title: 'QQ支付',
            bank: false,
            url: '/api/pay/general/pay.htm',
            param: {
                /**
                 * type 数据源类型 input-输入  select-选择  fixed-定值
                 * placeholder 占位符
                 * style input-only 输入框的类型 number tel text password
                 * length input-only 输入的长度限制  仅 text password tel生效
                 * float input-only 限制输入的内容是否包含小数点
                 * nonzero input-only 限制输入的内容最后一位是否不为0
                 * min input-only number-tel only 限制输入的数字的最小值
                 * max input-only number-tel only 限制输入的数字的最大值
                 * chn 中文限制 true-只能中文  false-非中文
                 */
                money: {
                    type: 'input',
                    title: '充值金额',
                    placeholder: '请输入金额',
                    value: '',
                    style: 'tel',
                    float: false,
                    min: 100,
                    max: 3000,
                    length: 4
                },
                payType: {
                    type: 'fixed',
                    value: 'qq_wallet'
                },
                client_ip:{
                    type: 'fixed',
                    value: '101.132.151.195'
                }
            },
            des: [
                '实时到账，0手续费',
                '单笔最低充值100元，最高3000元',
                '每日总额无上限，24小时可用',
                '如需帮助请联系人工客服 '//QQ:800837618
            ]
        };
    }

    render() {
        return (
            <div className={'rechargeStyle'}>
                <Header title={this.config.title} {...this.props}/>
                <div className={'main'}>
                    <Balance/>
                    <div className={'rechargeBody'}>
                        <ul className={'panel'}>
                            {
                                Object.entries(this.config.param).map(([key, o]) => {
                                    if (o.type === 'input') {
                                        return <Input {...o} callback={(e) => this.config.param[key].value = e}/>
                                    } else if (o.type === 'select') {
                                        return <Option {...o} callback={(e) => this.config.param[key].value = e}/>
                                    } else {
                                        return ''
                                    }
                                })
                            }
                        </ul>
                        <div className={'next'} onClick={() => this.submit()}>下一步</div>
                    </div>
                    <div className={'remark'}>
                        <h4>温馨提示</h4>
                        {
                            this.config.des.map((e, key) => {
                                return <p>{key + 1}&nbsp;&nbsp;{e}</p>
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }

    submit() {
        if (Verify(this.config.param)) {
            let o = {};
            for(let [key,{value}] of Object.entries(this.config.param)){
                if(key === 'money'){
                    o[key] = Number(value);
                }else{
                    if(value === 'origin') value = window.location.origin;
                    o[key] = value
                }
            }
            Req({
                url: this.config.url,
                type: 'POST',
                data: o,
                animate: true
            }).then((data) => {
                if(data.html){
                    document.write(data.html)
                }else if(data.redirectURL){
                    if(window.isSuperman){
                        LinkTo(data.redirectURL)
                    }else{
                        window.location.href = data.redirectURL
                    }
                }
            }).catch((err) => {
                AlertFunction({title: '错误', msg: err.resultMsg || err.errorMsg || '未知错误'})
            })
        }
    }
}