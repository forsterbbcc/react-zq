import React, {Component} from 'react';
import ReactSvg from 'react-svg';
import {Link} from 'react-router-dom'
import arrow from '../../../images/svg/right-arrow.svg';
import back from '../../../images/svg/back.svg'
import {Req} from "../../../lib";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bankList: []
        }
    }


    componentDidMount() {
        this.getAllCard();
    }

    componentWillUnmount() {
    }


    //todo 获取银行卡信息
    async getAllCard() {
        try {
            let result = await Req({
                url: '/api/mine/quickBankCard.htm',
                type: 'GET',
                data:{
                    payName:'SHUANGQIAN_PAY'
                }
            });
            if (result && result.code === 200) {
                this.setState({
                   bankList: result.bankCards
                });
            }
        } catch (e) {

            //var   bankCards =[  {bank:"工商银行",bindNum:"aaa"},  {bank:"建设银行",bindNum:"bbb"},  {bank:"中国银行",bindNum:"ccc"}  ];
            //this.setState({    bankList:  bankCards });

        }
    }

    render() {
        return (
            <div className={'select'}>
                <div className={'selectHeader'}>
                    <div className={'backTag'} onClick={()=>this.props.selectBank('',0)}>
                        <ReactSvg path={back} className={'arrow'} wrapperClassName={'out'}/>
                    </div>
                    <div className={'title'}>请选择充值银行</div>
                </div>
                <div className={'selectMain'}>
                    {this.state.bankList!=null && this.state.bankList.length>0?
                        <ul>
                            {
                                this.state.bankList.map((item) => {
                                    return (
                                        <li onClick={() => this.props.selectBank(item)}>{item.bank}
                                            <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                                    );
                                })
                            }
                        </ul>
                        :
                        <div className={"bindCardButton"}>
                            <Link to={'/bindSQCardList'}>绑定快捷支付银行卡</Link>
                        </div>
                    }
                </div>
            </div>
        )
    }
}