import React, {Component} from 'react';
import {Header} from "../../common";
import {Req, Schedule} from "../../../lib";
import {Link, Redirect} from 'react-router-dom';
import {idMark} from "../../../lib/tool";
import {Cache} from "../../../module";


//todo 图片的路径
import ICBC from '../../../images/bankIcon/icbc.png'
import CMB from '../../../images/bankIcon/cmb.png'
import CCB from '../../../images/bankIcon/ccb.png'
import ABC from '../../../images/bankIcon/abc.png'
import BOC from '../../../images/bankIcon/boc.png'
import COMM from '../../../images/bankIcon/comm.png'
import CMBC from '../../../images/bankIcon/cmbc.png'
import SPDB from '../../../images/bankIcon/spdb.png'
import CITIC from '../../../images/bankIcon/citic.png'
import GDB from '../../../images/bankIcon/gdb.png'
import SZPAB from '../../../images/bankIcon/szpab.png'
import CIB from '../../../images/bankIcon/cib.png'
import HXB from '../../../images/bankIcon/hxb.png'
import CEB from '../../../images/bankIcon/ceb.png'
import PSBC from '../../../images/bankIcon/psbc.png'
import AlertFunction from "../../../lib/AlertFunction";

function a(name) {
    switch (name) {
        case '工商银行':
            return ICBC;
        case '招商银行':
            return CMB;
        case '建设银行':
            return CCB;
        case '农业银行':
            return ABC;
        case '中国银行':
            return BOC;
        case '交通银行':
            return COMM;
        case '民生银行':
            return CMBC;
        case '浦发银行':
            return SPDB;
        case '中信银行':
            return CITIC;
        case '广发银行':
            return GDB;
        case '平安银行':
            return SZPAB;
        case '兴业银行':
            return CIB;
        case '华夏银行':
            return HXB;
        case '光大银行':
            return CEB;
        case '邮政储蓄':
            return PSBC;
    }
}

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bankList: [],
            bankIcon: '',
            goIdentityAuth: false
        }
    }

    render() {
        if (this.state.goIdentityAuth) {
            return (
                <Redirect to={'/realName'}/>
            );
        }
        return (
            <div className={'bankCardList'}>
                <Header title={'银行卡快捷支付'}  {...this.props}/>
                <div className={'main'}>
                    {
                        this.state.bankList.map((item) => {
                            return (
                                <div className={'bankBox'}>
                                    <div className={'bankImgBox'}>
                                        <img src={a(item.bank)} alt=""/>
                                    </div>
                                    <div className={'bankInfo'}>
                                        <Link to={{pathname: '/changeBankInfo', state: item}}>
                                            <div>{item.bank}</div>
                                            <div className={'bankNum'}>{idMark(item.cardNumber)}</div>
                                        </Link>
                                        <div className={'setting'}>
                                            {
                                                item.defaultCard === 1 ? (null) : (
                                                    <div onClick={() => this.setDefault(item.id)}>设为默认</div>
                                                )
                                            }
                                            <div onClick={() => this.deleteCard(item.id)}>删除卡片</div>
                                        </div>
                                    </div>
                                    {
                                        item.defaultCard === 1 ? (
                                            <div className={'mark'}>默认</div>
                                        ) : (null)
                                    }
                                </div>
                            )
                        })
                    }
                </div>
                <Link to={'/bindSQCardAdd'} className={'footBtn'}>
                    <div>添加绑定银行卡</div>
                </Link>
            </div>
        )
    }

    componentDidMount() {
        this.getAllCard();
    }

    componentWillUnmount() {
    }


    //todo 获取银行卡信息
    async getAllCard() {
        try {
            let result = await Req({
                url: '/api/mine/quickBankCard.htm',
                type: 'GET',
                data:{
                    payName:'SHUANGQIAN_PAY'
                }
            });
            if (result && result.code === 200) {
                this.setState({
                    bankList: result.bankCards
                });
            }
        } catch (e) {
            AlertFunction({title: '错误', msg: e.errorMsg})
        }
    }

    //todo 设置默认
    setDefault(id) {
        try {
            Req({
                url: '/api/mine/quickBankCardUpdate.htm',
                type: 'POST',
                data: {
                    action: 'setDefault',
                    id: id
                },
                animate: true,
                timeout:15000
            }).then((data) => {
                AlertFunction({title: '提示', msg: data.errorMsg, confirm: () => this.getAllCard()})
            });
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg})
        }
    }

    //todo 删除银行卡
    deleteCard(id) {
        AlertFunction({
            title: '警告',
            msg: '您确定要删除该银行卡？',
            hasCancel: true,
            confirm: () => {
                Req({
                    url: '/api/mine/quickBankCardUpdate.htm',
                    type: 'POST',
                    data: {
                        action: 'del',
                        id: id
                    },
                    animate: true,
                    timeout:15000
                }).then((data) => {
                    AlertFunction({title: '提示', msg: data.errorMsg, confirm: () => this.getAllCard()})
                    Cache.getUserInfo();
                })
            }
        })
    }
}