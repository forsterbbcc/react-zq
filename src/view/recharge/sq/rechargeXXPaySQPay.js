import React, {Component} from 'react';
import {Header, Balance,Cs} from "../../common/index";
import SelectBank from "./selectSQBank";
import {Req} from "../../../lib/index";
import AlertFunction from "../../../lib/AlertFunction";
import {getSearch} from "../../../lib/tool";
import {LinkTo} from "../../../lib/native";
import {Cache} from "../../../module";
import {Link} from 'react-router-dom';


export default class rechargeXXPayOnePointPay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectBankLimit:{},
            select: 0,
            bankName: '',
            bankId:'',
            money: '',

            accountName: '',
            accountNumber: '',
            idNumber: '',
            phoneNumber: '',
            paymentNo:'',
            payOrderId:'',
            smsCode:'',

            step:0,
            successful: false,
        };

        this.selectBank = this.selectBank.bind(this)
    }

    renderFirst() {
        return (
            <div className={'rechargeStyle'}>
                <Header title={'绑定银行卡'} {...this.props} right={'银行卡管理'} goTo={'/bindSQCardList'}/>
                        <div className={'main'}>
                            <Balance/>
                            <div className={'rechargeBody'}>
                               {/* <p>充值金额和银行转账金额必须一致，否则充值无法到账</p>*/}
                                <ul className={'panel'}>
                                    <li className={'row'}>
                                        <label>选择银行卡</label>
                                        <input type="text" className={'input'} placeholder={'请选择绑定的银行卡'}
                                               value={this.state.bankName} readOnly={true}
                                               onClick={() => this.setState({select: 1})}/>
                                    </li>
                                    <li className={'row'}>
                                        <label>充值金额</label>
                                        <input type="number" className={'input'} placeholder={'请输入充值金额'}
                                               value={this.state.money} maxLength={7}
                                               onChange={(e) => this.setState({money: e.target.value})}/>
                                    </li>

                                </ul>
                                <div className={'next'} onClick={() => this.submitBind()}>下一步</div>
                            </div>
                            <div className={'remark'}>
                                <h4>温馨提示：</h4>
                                <p>◆单笔充值{this.state.selectBankLimit.min}-{this.state.selectBankLimit.max}元</p>
                                <p>◆申请金额与转账金额必须一致，否则无法到账。</p>
                                <p>◆操作过程中如遇任何疑问，请<Link to={'/cs'}>联系客服</Link>。</p>
                            </div>
                        </div>

                {/*选择银行*/}
                {
                    this.state.select === 1 ? (
                        <SelectBank selectBank={this.selectBank}/>
                    ) : (null)
                }
            </div>
        )
    }


    renderVerification() {
        return (
            <div className={'addBank'}>
                <Header title={'银行卡快捷支付'} {...this.props}/>
                <div className={'main'}>
                    <h4>请输入银行卡注册手机号接收到的短信验证码</h4>
                    <ul>
                        <li>
                            <span>验证码</span>
                            <input type="text" placeholder={'请输入短信验证码'} value={this.state.smsCode} onChange={(e)=>this.setState({smsCode:e.target.value})} minLength={16} maxLength={19}/>
                        </li>
                    </ul>
                    <div className={'submit'} onClick={()=>this.submitPay()}>确定</div>
                </div>
                {
                    this.state.select === 1 ? (
                        <SelectBank selectBank={this.selectBank}/>
                    ) : (null)
                }
            </div>
        )
    }



    render() {
        if(this.state.step!=0){
            return this.renderVerification();
        }else {
            return this.renderFirst();
        }
    }


    //todo 提交
    submitBind() {
        if (this.state.money.length === 0) return AlertFunction({title: '警告', msg: '请填写充值金额！'});
        if (this.state.money < this.state.selectBankLimit.min || this.state.money > this.state.selectBankLimit.max ) return AlertFunction({
            title: '警告',
            msg: '单笔最低'+this.state.selectBankLimit.min +'元，最高'+this.state.selectBankLimit.max +'元！'
        });
        const search = getSearch();
        Req({
            url: '/api/pay/rechargeXXPay.htm',
            type: 'POST',
            data: {
                cardNumber: 0,
                type: search.type || 'dp',
                bank: this.state.bankName,
                bankId:this.state.bankId,
                money: Number(this.state.money),
                accountName: this.state.accountName,
                accountNumber: this.state.accountNumber,
                idNumber: this.state.idNumber,
                phoneNumber: this.state.phoneNumber,
                bindNo:this.state.bindNo,
                payName:'SHUANGQIAN_PAY'
            },
            animate: true,
            timeout:15000
        }).then((data) => {
            if(data.success==true && data.paymentNo!=null){
                this.setState({
                    step: 1,
                    paymentNo:data.paymentNo,
                    payOrderId:data.payOrderId
                })
            }
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
        })
    }


    //todo 提交
    submitPay() {
        if (this.state.smsCode.length === 0) return AlertFunction({title: '警告', msg: '请填写验证码！'});
        const search = getSearch();
        Req({
            url: '/api/pay/payConfirm.htm',
            type: 'POST',
            data: {
                cardNumber: 0,
                type: search.type || 'dp',
                bank: this.state.bankName,
                bankId:this.state.bankId,
                money: Number(this.state.money),
                accountName: this.state.accountName,
                accountNumber: this.state.accountNumber,
                idNumber: this.state.idNumber,
                phoneNumber: this.state.phoneNumber,
                bindNo:this.state.bindNo,
                paymentNo:this.state.paymentNo,
                payOrderId:this.state.payOrderId,
                smsCode:this.state.smsCode,
                payName:'SHUANGQIAN_PAY'
            },
            animate: true,
            timeout:15000
        }).then((data) => {
            if(data.success==true){
                AlertFunction({
                    title: '支付成功', msg: '支付成功，点击返回', confirm: () => {
                        window.location.href = "/mine"
                    }
                });
            }
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
        })
    }

    //todo 切换选择银行
    selectBank(item) {
        if(item!=undefined && item!=null && item.bank!=undefined) {
            this.setState({
                selectBankLimit: this.bankLimit(item.bank),
                select: 0,
                bankName: item.bank,
                bankId: this.bankLimit(item.bank).code,
                bindNo: item.bindNum,
                accountName: item.accountName,
                accountNumber: item.cardNumber,
                idNumber: item.idNum,
                phoneNumber: item.mobile,
                payName:'SHUANGQIAN_PAY'
            })
        }else {
            this.setState({
                select: 0,
            })
        }

    }


    bankLimit(name){
        switch (name) {
            case '工商银行':
                return {min:100,max: 20000,code:"ICBC"};
            case '招商银行':
                return {min:100,max: 5000,code:"CMB"};
            case '建设银行':
                return {min:100,max:10000 ,code: "CCB"};
            case '农业银行':
                return {min:100,max: 5000,code: "ABC"};
            case '中国银行':
                return {min:100,max: 5000,code: "BOC"};
            case '交通银行':
                return {min:100,max: 9999,code: "COMM"};
            case '民生银行':
                return {min:100,max: 20000,code:"CMBC" };
            case '浦发银行':
                return {min:100,max: 50000,code:"SPDB" };
            case '中信银行':
                return {min:100,max: 50000,code:"CITIC" };
            case '广发银行':
                return {min:100,max: 50000,code:"CGB" };
            case '平安银行':
                return {min:100,max: 10000,code: "PINGANBK"};
            case '兴业银行':
                return {min:100,max: 50000,code:"CIB" };
            case '华夏银行':
                return {min:0,max: 0,code:"HXB" };
            case '光大银行':
                return {min:100,max: 5000,code:"CEB" };
            case '邮政储蓄':
                return {min:0,max: 0,code: "PSBC"};
        }
    }

}