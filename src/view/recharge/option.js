import React, {Component} from 'react';
import {Select} from "../common";


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            select: false
        };
        this.list = this.props.store;
        if (this.props.store === 'bank') {
            this.list = {
                '工商银行': '工商银行',
                '建设银行': '建设银行',
                '农业银行': '农业银行',
                '招商银行': '招商银行',
                '中国银行': '中国银行',
                '交通银行': '交通银行',
                '邮政储蓄': '邮政储蓄',
                '民生银行': '民生银行',
                '浦发银行': '浦发银行',
                '兴业银行': '兴业银行',
                '华夏银行': '华夏银行',
                '光大银行': '光大银行',
                '广发银行': '广发银行',
                '中信银行': '中信银行',
                '平安银行': '平安银行'
            }
        }
    }

    render() {
        return (
            <li className={'row'}>
                <label>{this.props.title}</label>
                <input className={'input'} type={'text'} readOnly={'readOnly'} unselectable={'on'} onFocus={(e)=>e.currentTarget.blur()} placeholder={this.props.placeholder}
                       maxLength={this.props.length} value={this.state.value} onClick={() => this.setState({select: true})}/>
                {
                    this.state.select ? <Select list={this.list} callback={(key,value)=>{
                        this.setState({
                            select:false,
                            value:key
                        });
                        this.props.callback(value)
                    }} close={()=>this.setState({select:false})} title={this.props.placeholder}/> : ''
                }
            </li>
        )
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }
}