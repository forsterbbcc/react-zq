import React, {Component} from 'react';
import ReactSvg from 'react-svg';

import arrow from '../../images/svg/right-arrow.svg';
import back from '../../images/svg/back.svg'

export default class windBank extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className={'select'}>
                <div className={'selectHeader'}>
                    <div className={'backTag'} onClick={()=>this.props.selectBank('',0)}>
                        <ReactSvg path={back} className={'arrow'} wrapperClassName={'out'}/>
                    </div>
                    <div className={'title'}>请选择充值银行</div>
                </div>
                <div className={'selectMain'}>
                    <ul>
                        <li onClick={() => this.props.selectBank('邮政储蓄',0)}>邮政储蓄 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('工商银行',0)}>工商银行 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('建设银行',0)}>建设银行 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('中国银行',0)}>中国银行 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('农业银行',0)}>农业银行 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('光大银行',0)}>光大银行 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('民生银行',0)}>民生银行 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('北京银行',0)}>北京银行 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('上海银行',0)}>上海银行 <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                    </ul>
                </div>
            </div>
        )
    }
}