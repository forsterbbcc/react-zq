import AlertFunction from "../../lib/AlertFunction";

export default function (obj) {
    return Object.entries(obj).every(([key, o]) => {
        if (o.type === 'input') {
            if (o.value === '') {
                AlertFunction({title: '警告', msg: `请输入${o.title}`});
                return false;
            }
            if ((o.style === 'number' || o.style === 'tel') && o.min !== undefined && o.min > 0) {
                if (o.value < o.min) {
                    AlertFunction({title: '警告', msg: `${o.title}最低${o.min}`});
                    return false;
                }
            }
            if ((o.style === 'number' || o.style === 'tel') && o.max !== undefined && o.max > 0) {
                if (o.value > o.max) {
                    AlertFunction({title: '警告', msg: `${o.title}最高${o.max}`});
                    return false;
                }
            }
            if (o.float !== undefined) {
                if ((o.value.toString().indexOf('.') !== -1) !== o.float) {
                    AlertFunction({title: '警告', msg: `${o.title}${o.float ? '必须' : '不能'}含有小数`})
                    return false;
                }
            }
            if (o.nonzero !== undefined && o.nonzero) {
                if(o.value.toString().slice(-1) === '0'){
                    AlertFunction({title: '警告', msg: `${o.title}最后一位不能为0`});
                    return false;
                }
            }
            if (o.chn !== undefined) {
                o.value = o.value.replace(/\r\n/g, '');
                let pattern = /^([u4e00-u9fa5]|[ufe30-uffa0])*$/gi;
                let other = /\d/g;
                if (o.chn) {
                    console.log(other.test(o.value));
                    if (pattern.test(o.value)) {
                        AlertFunction({title: '警告', msg: `${o.title}请输入中文`});
                        return false
                    }
                } else {
                    if (!pattern.test(o.value)) {
                        AlertFunction({title: '警告', msg: `${o.title}请不要输入中文`});
                        return false
                    }
                }
            }
            return true
        } else if (o.type === 'select') {
            if (o.value === '') {
                AlertFunction({title: '警告', msg: `请选择${o.title}`});
                return false;
            }
            return true
        } else {
            return true
        }
    })
}