import React, {Component} from 'react';
import {Header, Balance,Cs} from "../common";
import {Req} from "../../lib";
import AlertFunction from "../../lib/AlertFunction";
import Input from './input';
import Option from './option';
import Verify from "./verify";
import {getSearch} from "../../lib/tool";
import wechat_pay from '../../images/wechat_pay.png';
import {LinkTo} from "../../lib/native";
import {Link} from 'react-router-dom';

var QRCode = require('qrcode.react');
export default class App extends Component {
    _type = null;

    constructor(props) {
        super(props);
        this.state={
            note:'',
            payOrderId:'',
            qrURL:null,
            active: '',
            money: '',
            moneyAry: ['100', '300','500', '1000', '2000', '3000', '5000', '10000', '20000', '30000','40000', '50000'],
        };
        this.config = {
            title: '银联扫码',
            bank: false,
            url: '/api/pay/rechargeXXPay.htm',
            des: [
                '◆仅支持以下相关银行App充值：',
                '银联云闪付、工行、农行、建行、招行、中信、兴业、浦发、中国银行等手机银行；',
                '◆二维码仅可使用一次，再次充值需要重新获取二维码，重复扫码付款造成的资金损失，由客户自行承担！'
            ]
        };

        if (getSearch() && getSearch().type) {
            this._type = getSearch().type;
        }
    }

    renderFirst=()=>{
        return (
            <div className={'rechargeStyle'}>
                <Header title={this.config.title} {...this.props}/>
                <div className={'main'}>
                    <Balance/>
                    <div className={'rechargeBody'}>
                        <div className={'choose'}>
                            <div className={'chooseStyle'}>
                                <div>选择金额</div>
                            </div>
                            <div className={'chooseMoney'}>
                                {
                                    this.state.moneyAry.map((item, key) => {
                                        return (
                                            <div className={this.state.active === key ? 'active' : ''}
                                                 onClick={() => this.switchMoney(key, item)}>{item}</div>
                                        )
                                    })
                                }
                            </div>
                        </div>

                        <div className={'next'} onClick={() => this.next()}>下一步</div>
                    </div>
                    <div className={'remark'}>
                        <h4>温馨提示</h4>
                        {
                            this.config.des.map((e, key) => {
                                return <p>{e}</p>
                            })
                        }
                        <p>◆操作过程中如遇任何疑问，请<Link to={'/cs'}> 联系客服</Link>。</p>
                    </div>
                </div>
            </div>
        )
    }

    renderQRCode=()=>{
        return (
            <div className={'rechargeStyle'}>
                <Header title={this.config.title} {...this.props}/>
                <div className={'main'}>

                    <div className={"rewarm"}>
                        警告：二维码仅可使用一次，再次充值需重新获取二维码，重复扫码付款造成的资金损失，由客户自行承担！
                    </div>

                    <div className={"qrCodeContainer"}>
                        {this.renderQRImage()}
                    </div>

                    <div className="wechat_pay_title">
                        截图保存到相册 用银行APP或云闪付扫一扫
                    </div>

                    <div className={"flashPayRow"} style={{margin:"30px"}}>
                        {/*<label></label>*/}
                        <input type="tel" className="input" placeholder={"付款后输入银行卡后4位"} maxLength={4}
                               onChange={(e) => {  this.setState({note: e.target.value}) }}/>
                        <div className={"sButton"} onClick={() => this.submit()}><span>提交</span></div>
                    </div>
                    <div className={'wechat_pay_info'}>
                        <div className={"title"}>充值步骤</div>
                        <div className={"step"}>第一步</div>
                        <div className={"stepInfo"}>截图保存到手机相册</div>
                        <div className={"step"}>第二步</div>
                        <div className={"stepInfo"}>打开手机银行或银联云闪付app并选择相册中的二维码扫一扫进行付款</div>
                        <div className={"step"}>第三步</div>
                        <div className={"stepInfo"}>支付成功后，请输入本次付款的银行卡卡号后四位，并点击提交，即可完成支付。</div>
                    </div>

                </div>
            </div>
        )
    }

    renderQRImage(){
        if (this.state.qrURL!=null) {
            console.log(this.state.qrURL);
            return(
                <QRCode value={this.state.qrURL} size={200} renderAs="svg" level={"L"}/>
            );
        }
        return(<div></div>);
    }


    render() {
        return this.renderFirst();
       /* if(this.state.qrURL!=null){
            return this.renderQRCode();
        }else {
            return this.renderFirst();
        }*/
    }



    next() {
        if (this.state.money.length === 0) return AlertFunction({title: '警告', msg: '请填写入金金额!'});
        if (this.state.money < 100 || this.state.money > 50000) return AlertFunction({
            title: '警告',
            msg: '单笔最低100元，最高50000元！'
        });
        Req({
            url: '/api/pay/rechargeXXPay.htm',
            type: 'POST',
            data: {
                type: this._type,
                money: this.state.money,
                channel: 'ALIPAY_WAP',
            },
            animate: true
        }).then((data) => {
            if(data.html){
                document.write(data.html)
            }else if(data.redirectURL){
                /*if(data.redirectURL!=null){
                    this.setState({
                        qrURL:data.redirectURL,
                        payOrderId:data.payOrderId
                    });
                }else {
                    AlertFunction({title: '错误 ~', msg: '网络异常'})
                }*/
                if(window.isSuperman){
                    LinkTo(data.redirectURL)
                }else{
                    window.location.href = data.redirectURL
                }
            }

        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
        })
    }


    submit() {
        if (this.state.money.length === 0) return AlertFunction({title: '警告', msg: '请填写入金金额!'});
        if (this.state.money < 300 || this.state.money > 50000) return AlertFunction({
            title: '警告',
            msg: '单笔最低300元，最高50000元！'
        });
        Req({
            url: '/api/pay/xxpay/note.htm',
            type: 'POST',
            data: {
                payOrderId: this.state.payOrderId,
                note: this.state.note,
                type:this._type
            },
            animate: true
        }).then((data) => {
            if(data.success==true){
                AlertFunction({
                    title: '支付成功', msg: '支付成功，点击返回', confirm: () => {
                        window.location.href = "/mine"
                    }
                });
            }else {
                AlertFunction({title: '错误 ~', msg: '网络异常'})
            }
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
        })
    }

    switchMoney(index,money){
        this.setState({
            active:index,
            money:money
        });
    }
}