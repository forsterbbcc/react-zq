import React, {Component} from 'react';
import {Header, Balance,Cs} from "../common";
import {Req} from "../../lib";
import AlertFunction from "../../lib/AlertFunction";
import Input from './input';
import Option from './option';
import Verify from "./verify";
import {getSearch} from "../../lib/tool";
import {LinkTo} from "../../lib/native";
import {Link} from 'react-router-dom';
export default class App extends Component {
   //5万的支付宝通道
    constructor(props) {
        super(props);
        this.config = {
            title: '支付宝支付',
            bank: false,
            url: '/api/pay/rechargeXXPay.htm',
            param: {
                /**
                 * type 数据源类型 input-输入  select-选择  fixed-定值
                 * placeholder 占位符
                 * style input-only 输入框的类型 number tel text password
                 * length input-only 输入的长度限制  仅 text password tel生效
                 * float input-only 限制输入的内容是否包含小数点
                 * nonzero input-only 限制输入的内容最后一位是否不为0
                 * min input-only number-tel only 限制输入的数字的最小值
                 * max input-only number-tel only 限制输入的数字的最大值
                 * chn 中文限制 true-只能中文  false-非中文
                 */
                money: {
                    type: 'input',
                    title: '充值金额',
                    placeholder: '单笔最低100元，最高50000元',
                    value: '',
                    style: 'number',
                    float: false,
                    min: 100,
                    max: 50000,
                    length: 5
                },
                type: {
                    type: 'fixed',
                    value: getSearch().type
                },
                channel: {
                    type: 'fixed',
                    value: getSearch().channel
                }
            },
            des: [
                '◆申请金额与转账金额必须一致，否则无法到账。',
                '◆第三方收款账户不定期更新，充值前需获取最新收款信息。'
            ]
        };
    }

    render() {
        return (
            <div className={'rechargeStyle'}>
                <Header title={this.config.title} {...this.props}/>
                <div className={'main'}>
                    <Balance/>
                    <div className={'rechargeBody'}>
                        <ul className={'panel'}>
                            {
                                Object.entries(this.config.param).map(([key, o]) => {
                                    if (o.type === 'input') {
                                        return <Input {...o} callback={(e) => this.config.param[key].value = e}/>
                                    } else if (o.type === 'select') {
                                        return <Option {...o} callback={(e) => this.config.param[key].value = e}/>
                                    } else {
                                        return ''
                                    }
                                })
                            }
                        </ul>
                        <div className={'next'} onClick={() => this.submit()}>下一步</div>
                    </div>
                    <div className={'remark'}>
                        <h4>温馨提示</h4>
                        {
                            this.config.des.map((e, key) => {
                                return <p>{e}</p>
                            })
                        }
                        <p>◆操作过程中如遇任何疑问，请<Link to={'/cs'}> 联系客服</Link>。</p>
                    </div>
                </div>
            </div>
        )
    }

    submit() {
        if (Verify(this.config.param)) {
            let o = {};
            for(let [key,{value}] of Object.entries(this.config.param)){
                if(key === 'money'){
                    o[key] = Number(value);
                }else{
                    if(value === 'origin') value = window.location.origin;
                    o[key] = value
                }
            }
            Req({
                url: this.config.url,
                type: 'POST',
                data: o,
                animate: true
            }).then((data) => {
                if(data.html){
                    document.write(data.html)
                }else if(data.redirectURL){
                    if(window.isSuperman){
                        LinkTo(data.redirectURL)
                    }else{
                        window.location.href = data.redirectURL
                    }
                }
            }).catch((err) => {
                AlertFunction({title: '错误', msg: err.resultMsg || err.errorMsg || '未知错误'})
            })
        }
    }
}