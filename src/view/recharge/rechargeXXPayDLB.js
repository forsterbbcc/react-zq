import React, {Component} from 'react';
import {Header, Balance,Cs} from "../common";
import {Req} from "../../lib";
import AlertFunction from "../../lib/AlertFunction";
import Input from './input';
import Option from './option';
import Verify from "./verify";
import {getSearch} from "../../lib/tool";
import wechat_pay from '../../images/wechat_pay.png';
import {LinkTo} from "../../lib/native";
import {Link} from 'react-router-dom';

var QRCode = require('qrcode.react');
export default class App extends Component {

    constructor(props) {
        super(props);
        this.state={
            qrURL:null,
        };
        this.config = {
            title: '综合扫码支付',
            bank: false,
            url: '/api/pay/rechargeXXPay.htm',
            param: {
                /**
                 * type 数据源类型 input-输入  select-选择  fixed-定值
                 * placeholder 占位符
                 * style input-only 输入框的类型 number tel text password
                 * length input-only 输入的长度限制  仅 text password tel生效
                 * float input-only 限制输入的内容是否包含小数点
                 * nonzero input-only 限制输入的内容最后一位是否不为0
                 * min input-only number-tel only 限制输入的数字的最小值
                 * max input-only number-tel only 限制输入的数字的最大值
                 * chn 中文限制 true-只能中文  false-非中文
                 */
                money: {
                    type: 'input',
                    title: '充值金额',
                    placeholder: '单笔最低100元，最高5000元',
                    value: '',
                    style: 'number',
                    float: false,
                    min: 100,
                    max: 5000,
                    length: 5
                },
                type: {
                    type: 'fixed',
                    value: getSearch().type
                },
                channel: {
                    type: 'fixed',
                    value: getSearch().channel
                }
            },
            des: [
                '◆二维码仅可使用一次，每次充值需重新申请获取二维码，重复扫码付款造成的资金损失将自行承担。',
            ]
        };
    }

    renderFirst=()=>{
        return (
            <div className={'rechargeStyle'}>
                <Header title={this.config.title} {...this.props}/>
                <div className={'main'}>
                    <Balance/>
                    <div className={'rechargeBody'}>
                        <ul className={'panel'}>
                            {
                                Object.entries(this.config.param).map(([key, o]) => {
                                    if (o.type === 'input') {
                                        return <Input {...o} callback={(e) => this.config.param[key].value = e}/>
                                    } else if (o.type === 'select') {
                                        return <Option {...o} callback={(e) => this.config.param[key].value = e}/>
                                    } else {
                                        return ''
                                    }
                                })
                            }
                        </ul>
                        <div className={'next'} onClick={() => this.submit()}>下一步</div>
                    </div>
                    <div className={'remark'}>
                        <h4>温馨提示</h4>
                        {
                            this.config.des.map((e, key) => {
                                return <p>{e}</p>
                            })
                        }
                        <p>◆操作过程中如遇任何疑问，请<Link to={'/cs'}> 联系客服</Link>。</p>
                    </div>
                </div>
            </div>
        )
    }

    renderQRCode=()=>{
        return (
            <div className={'rechargeStyle'}>
                <Header title={this.config.title} {...this.props}/>
                <div className={'main'}>
                    <div className={"qrCodeContainer"}>
                        {/*<img src={wechat_pay} className="wechat_pay"/>*/}
                        <span className="rewarm">警告：二维码仅可使用一次，每次充值需重新申请获取二维码，重复扫码付款造成的资金损失将自行承担！</span>
                        {this.renderQRImage()}
                    </div>

                    <div className="wechat_pay_title">
                        支持【支付宝/微信/京东钱包】扫码支付
                    </div>

                    {/*<a href={this.state.qrURL}>
                        <div className="wechat_button">
                            点击打开APP支付
                        </div>
                    </a>*/}
                    <div className={'wechat_pay_info'}>
                        <div className={"title"}>充值提示</div>
                        <div className={"step"}>第一步</div>
                        <div className={"stepInfo"}>截图保存到手机相册</div>
                        <div className={"step"}>第二步</div>
                        <div className={"stepInfo"}>打开微信，支付宝,京东钱包的扫一扫<br/>选择相册中的二维码进行付款</div>
                        <div className={"step"}>第三步</div>
                        <div className={"stepInfo"}>支付结果请返回当前页面查看结果</div>
                    </div>

                </div>
            </div>
        )
    }

    renderQRImage(){
        if (this.state.qrURL!=null) {
            console.log(this.state.qrURL);
            return(
                <QRCode value={this.state.qrURL} size={200} renderAs="svg" level={"L"}/>
            );
        }
        return(<div></div>);
    }


    render() {
        return this.renderFirst();
       /* if(this.state.qrURL!=null){
            return this.renderQRCode();
        }else {
            return this.renderFirst();
        }*/
    }



    submit() {
        if (Verify(this.config.param)) {
            let o = {};
            for(let [key,{value}] of Object.entries(this.config.param)){
                if(key === 'money'){
                    o[key] = Number(value);
                }else{
                    if(value === 'origin') value = window.location.origin;
                    o[key] = value
                }
            }
            Req({
                url: this.config.url,
                type: 'POST',
                data: o,
                animate: true
            }).then((data) => {
                if(data.html){
                    document.write(data.html)
                }else if(data.redirectURL){
                   /* if(data.redirectURL!=null){
                        this.setState({
                            qrURL:data.redirectURL
                        });
                    }else {
                        AlertFunction({title: '错误 ~', msg: '网络异常'})
                    }*/
                    if(window.isSuperman){
                        LinkTo(data.redirectURL)
                    }else{
                        window.location.href = data.redirectURL
                    }
                }
            }).catch((err) => {
                AlertFunction({title: '错误', msg: err.resultMsg || err.errorMsg || '未知错误'})
            })
        }
    }
}