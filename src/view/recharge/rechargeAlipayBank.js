import React, {Component} from 'react';
import {Header, SelectBank, Balance, Cs} from "../common";
import {Link} from 'react-router-dom';
import CopyToClipboard from 'react-copy-to-clipboard';
import {Req, Schedule} from "../../lib";
import AlertFunction from "../../lib/AlertFunction";
import {bufferCallback, getSearch} from "../../lib/tool";
import {Cache} from "../../module";
import {LinkTo} from "../../lib/native";


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            select: 0,
            money: '',
            note: '',
            identity: Cache.initial ? Cache.idNumberValid : false,
            next: 0,
            successful: false,
            transBank: '',
            transBankBranch: '',
            transName: '',
            transNumber: '',
            transMoney: '',
            remark: ''
        };
    }

    render() {
        return (
            <div className={'rechargeStyle'}>
                <Header title={'支付宝转银行卡'} {...this.props}/>
                {
                    this.state.next === 0 ? (
                        <div className={'main'}>
                            <Balance/>
                            <div className={'rechargeBody'}>
                                <p>充值金额和支付宝转账金额必须一致,否则充值无法到账</p>
                                <ul className={'panel'}>
                                    <li className={'row'}>
                                        <label>支付宝姓名</label>
                                        <input type="text" className={'input'} placeholder={'请输入支付宝姓名'}
                                               value={this.state.note} maxLength={7}
                                               onChange={(e) => this.setState({note: e.target.value})}
                                               disabled={this.state.identity}/>
                                    </li>
                                    <li className={'row'}>
                                        <label>充值金额</label>
                                        <input type="number" className={'input'} placeholder={'请输入充值金额'}
                                               value={this.state.money} maxLength={7}
                                               onChange={(e) => this.setState({money: e.target.value})}/>
                                    </li>
                                </ul>
                                <div className={'next'} onClick={() => this.submit()}>下一步</div>
                            </div>
                            <div className={'remark'}>
                                <h4>温馨提示：</h4>
                                <p>* 单笔充值100-50000元</p>
                                <p>* 充值时间24小时,每日总额无上限</p>
                                <p>* 如需帮助,请<Link to={'/cs'}> 联系客服</Link></p>
                            </div>
                        </div>
                    ) : (
                        <div className={'transferCardInfo'}>
                            <div className={'title'}>
                                <p>请您通过支付宝向以下账户转账</p>
                                <span>若支付宝姓名错误，将无法自动到账</span>
                            </div>
                            <ul className={'panel'}>
                                <li className={'row'}>
                                    <label>收款银行</label>
                                    <div>{this.state.transBank}</div>
                                </li>
                                <li className={'row'}>
                                    <label>支行名称</label>
                                    <div>{this.state.transBankBranch}</div>
                                    <CopyToClipboard text={this.state.transBankBranch} onCopy={() => this.onCopy()}>
                                        <div className={'copy'}>复制</div>
                                    </CopyToClipboard>
                                </li>
                                <li className={'row'}>
                                    <label>收款姓名</label>
                                    <div>{this.state.transName}</div>
                                    <CopyToClipboard text={this.state.transName} onCopy={() => this.onCopy()}>
                                        <div className={'copy'}>复制</div>
                                    </CopyToClipboard>
                                </li>
                                <li className={'row'}>
                                    <label>收款账号</label>
                                    <div>{this.state.transNumber}</div>
                                    <CopyToClipboard text={this.state.transNumber} onCopy={() => this.onCopy()}>
                                        <div className={'copy'}>复制</div>
                                    </CopyToClipboard>
                                </li>
                                <li className={'row'}>
                                    <label>充值金额</label>
                                    <div>{this.state.transMoney}</div>
                                </li>
                                <li className={'row'}>
                                    <label>附 言</label>
                                    <div>{this.state.remark}</div>
                                    <CopyToClipboard text={this.state.remark} onCopy={() => this.onCopy()}>
                                        <div className={'copy'}>复制</div>
                                    </CopyToClipboard>
                                    <i>(重要必填项)</i>
                                </li>
                            </ul>
                            <div className={'remark'}>
                                <h4>温馨提示</h4>
                                <p>* 请确认收款银行，账号再进行充值</p>
                                <p>* 附言区分大小写，请复制填写在 <i>转账备注，留言或用途里</i></p>
                                <p>* <span>0手续费</span>，若附言填写正确，付款成功后自动到账</p>
                                <p>* 如需帮助，请<Link to={'/cs'}>联系客服</Link></p>
                            </div>
                        </div>
                    )
                }
                {/*复制成功*/}
                {
                    this.state.successful ? (
                        <div className={'copySuccess'}>
                            复制成功
                        </div>
                    ) : (null)
                }
            </div>
        )
    }

    componentDidMount() {
        bufferCallback(Cache.initial, 'getUserInfo', () => {
            if (Cache.idNumberValid) {
                this.setState({identity: Cache.idNumberValid, note: Cache.realName})
            }
        }, this);
    }

    //todo 提交
    submit() {
        if (this.state.note === '') return AlertFunction({title: '警告', msg: '请天蝎支付宝姓名!'});
        if (this.state.money.length === 0) return AlertFunction({title: '警告', msg: '请填写充值金额!'});
        if (this.state.money < 100 || this.state.money > 50000) return AlertFunction({
            title: '警告',
            msg: '单笔最低100元，最高50000元！'
        });
        const search = getSearch();
        Req({
            url: '/api/pay/rechargeDPBank.htm',
            type: 'POST',
            data: {
                cardNumber: 0,
                payType: search.payType || 'dp',
                bank: '支付宝',
                money: Number(this.state.money),
                note: this.state.note
            },
            animate: true
        }).then((data) => {
            if (data.redirectURL) {
                if (window.isNative) {
                    LinkTo(data.redirectURL)
                } else {
                    window.location.href = data.redirectURL
                }
            } else {
                this.setState({
                    transBank: data.dp.bank,
                    transBankBranch: data.dp.bank_address,
                    transName: data.dp.cardUsername,
                    transNumber: data.dp.cardNumber,
                    transMoney: data.dp.amount,
                    remark: data.dp.note,
                    next: 1
                })
            }
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
        })
    }

    //todo 复制
    onCopy() {
        this.setState({successful: true});

        setTimeout(() => {
            this.setState({successful: false})
        }, 1500)
    }

}