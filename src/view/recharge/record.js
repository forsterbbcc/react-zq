import React, {Component} from 'react';
import {Header} from "../common";
import {Req} from "../../lib";
import {formatDate} from "../../lib/tool";

import empty from '../../images/empty.png'


function statusText(status) {
    switch (status) {
        case 0:
            return '未处理';
        case 1:
            return '处理成功';
        case 2:
            return '处理失败';
        case 3:
            return '已取消';
        case 4:
            return '处理中';
        case 5:
            return '汇款中';
    }
}

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        }
    }

    render() {
        return (
            <div className={'record'}>
                <Header title={'充值记录'} {...this.props}/>

                {
                    this.state.list.length !== 0 ? (
                        <ul>
                            {
                                this.state.list.map((item) => {
                                    return (
                                        <li>
                                            <div className={'left'}>
                                                <p>{item.explain}</p>
                                                <span>{statusText(item.status)}</span>
                                            </div>
                                            <div className={'right'}>
                                                <p>+{item.money.toFixed(2)}</p>
                                                <span>{formatDate('y-m-d h:i:s', {date: item.disposeTime.time})}</span>
                                            </div>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    ) : (
                        <img src={empty} alt=""/>
                    )
                }
            </div>
        )
    }

    componentDidMount() {
        Req({
            url: '/api/pay/rechargeHistory.htm',
            type: 'GET',
            animate:true
        }).then((data) => {
            this.setState({
                list: data.inouts,
            })
        }).catch((err) => {

        })
    }
}