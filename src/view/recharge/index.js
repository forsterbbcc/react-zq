import React, {Component} from 'react';
import {Header, Cs} from "../common";
import {Link} from 'react-router-dom';
import ReactSvg from 'react-svg';
import {Req} from "../../lib";
import {getPlatform} from "../../lib/tool";


//todo 图片
import bank from '../../images/svg/bank.svg'
import cancel from '../../images/svg/cancel.svg'
import AlertFunction from "../../lib/AlertFunction";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            payList: [],
            first: '',
            realName: '',
            darkType:false
        }
    }

    componentWillMount(){
        this.setState({
            darkType:JSON.parse(localStorage.getItem('bgc'))
        })
    }

    componentWillMount(){
        this.setState({
            darkType:JSON.parse(localStorage.getItem('bgc'))
        })
    }

    render() {
        return (
            <div className={`recharge ${this.state.darkType?'dark':''}`}>
                <Header title={'充值'} right={'充值记录'} goTo={'/records'} {...this.props}/>

                {
                    this.state.first === 1 ? (
                        <div className={'mainBg'}>
                            <div className={'mainBox'}>
                                <h4>首次充值认证</h4>
                                <div className="back"
                                     onClick={() => {
                                         window.history.back()
                                     }}>
                                    <ReactSvg
                                        wrapperClassName={'cancel'}
                                        path={cancel}
                                    />
                                </div>
                                <div className={'mainRealName'}>
                                    <span>真实姓名</span>
                                    <input type="text"
                                           placeholder={'请输入您的真实姓名'}
                                           onChange={(e) => {
                                               this.setState({
                                                   realName: e.target.value
                                               })
                                           }}
                                    />
                                </div>
                                <p>备注:完成充值实名认证，即可开启充值服务，姓名需要与手机号码注册人一致，如验证三次错误，此手机号码将无法在平台继续使用。</p>
                                <div className={'submit'} onClick={() => {
                                    this.valid()
                                }}>提交验证
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className={'main'}>
                            <p className={'chooice'}>选择支付方式</p>
                            {
                                this.state.payList.map((item) => {
                                    return (
                                        <Link to={item.url}>
                                            <div className={'cardLeft'}>
                                                <div className={'circle'}>
                                                    <ReactSvg className={'card'}
                                                              wrapperClassName={'outCard'}
                                                              path={bank}
                                                    />
                                                </div>
                                            </div>
                                            <div className={'cardRight'}>
                                                <h4>{item.name}</h4>
                                                <p>{item.description}</p>
                                            </div>
                                        </Link>
                                    )
                                })
                            }
                            <p>如遇问题,请 <Link to={'/cs'}> 联系客服</Link></p>
                        </div>
                    )
                }
            </div>
        )
    }

    componentDidMount() {
        this.getPayList()
    }

    getPayList() {
        Req({
            url: '/api/pay/recharge.htm',
            type: 'POST',
            data: {
                action: 'getPayList',
                switchType: 1,
                platform: getPlatform()
            },
            animate: true
        }).then((data) => {
            this.setState({
                payList: data.payList,
                first: data.payFirst
            });
        })
    }

    //todo 验证真实姓名
    valid() {
        if (this.state.realName.length === 0) return AlertFunction({title: '提示', msg: '请输入您的真实姓名'});

        Req({
            url: '/api/pay/recharge.htm',
            type: 'POST',
            data: {
                action: 'checkName',
                name: this.state.realName,
            },
            animate: true
        }).then((data) => {
            AlertFunction({
                title: '提示', msg: data.errorMsg, confirm: () => {
                    this.setState({
                        first: data.payFirst
                    });
                }
            })
        }).catch((err) => {
            AlertFunction({title: '提示', msg: err.errorMsg})
        })
    }
}