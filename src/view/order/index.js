import React, {Component} from 'react';
import {Req, Schedule} from "../../lib";
import {Cache, Contracts} from "../../module";
import List from './list'
import {formatDate, getCloseTime, getIdentity, getPlatform} from "../../lib/tool";
import AlertFunction from "../../lib/AlertFunction";
import {Svg} from "../common";
import close from '../../images/svg/wrong.svg';

export default class App extends Component {
    constructor(props) {
        super(props);
        this._init = false;
        this.state = {
            name: '',
            code: '',
            hasMoneyType: 0,
            moneyType: 0,
            stopLoss: [0],
            stopProfit: [0],
            stopLossIndex: 0,
            volume: [0],
            volumeIndex: 0,
            chargeUnit: 0,
            close: '',
            balance: this.props.location.state.simulate ? Cache.gameBalance : Cache.realBalance,
            price: 0.00,
            tend:''
        };

        let c = Contracts.getContract(this);
        if (!!c) {
            this.state.hasMoneyType = Contracts.total[c].moneyType;
            this.state.code = c;
            this.state.name = Contracts.total[c].name;
            this._init = true;
            if (!!Cache.tradeList) {
                this.updateSchemeInfo(true);
            }
        }
    }

    render() {
        return (
            <div className={`orderPage ${this.props.show ? 'come' : ''}`}>
                {/*<Header title={'买入委托'} back={() => this.props.close()}/>*/}
                <div className={'top'} onClick={() => this.props.close()}/>
                <div className={'body'}>
                    <div className={'orderBalance'}>
                        <div className={'balanceBox'}>
                            <span className={'sub-title'}>{this.props.location.state.simulate? '模拟余额:':'实盘余额'}</span> {this.state.balance}&nbsp;<span className={'sub-title'}>元</span>
                        </div>
                        <div className={'closed'} onClick={() => this.props.close()}><Svg path={close}/></div>
                    </div>
                    {
                        this.state.hasMoneyType && !this.props.location.state.simulate? (
                        <div className={'grid row-controller'}>
                            <div className={'left'}>
                                交易模式
                            </div>
                            <div className={'btn-group'}>
                                <div className={this.state.moneyType === 0 ? 'active' : ''}
                                     onClick={() => this.setState({moneyType: 0})}>
                                    元模式
                                </div>
                                <div className={this.state.moneyType === 1 ? 'active' : ''}
                                     onClick={() => this.setState({moneyType: 1})}>
                                    角模式
                                </div>
                            </div>
                        </div>
                    ) : ''
                    }

                    <div className={'grid row-column'}>
                        <div className={'sub-title2'}>
                            交易数量
                        </div>
                        <List className={'btn-list pad-left'} list={this.state.volume} select={this.state.volumeIndex}
                              unit={'手'} pick={(key) => this.setState({volumeIndex: key})}/>
                    </div>
                    <div className={'grid row-column'}>
                        <div className={'sub-title2'}>
                            触发止损
                        </div>
                        <List className={'btn-list pad-left'} list={this.state.stopLoss}
                              plus={this.state.volume[this.state.volumeIndex].mul(this.state.moneyType === 0 ? 1 : 0.1)}
                              select={this.state.stopLossIndex}
                              unit={'元'} pick={(key) => this.setState({stopLossIndex: key})}/>
                    </div>
                    <div className={'grid row-des pad-right'}>
                        <div className={'left'}>
                            触发止盈
                        </div>
                        <div className={'btn raise'}>
                            {this.state.stopProfit[this.state.stopLossIndex].mul(this.state.volume[this.state.volumeIndex]).mul(this.state.moneyType === 0 ? 1 : 0.1)}元
                        </div>
                    </div>
                    <div className={'grid row-des pad-right'}>
                        <div className={'left'}>
                           交易综合费
                        </div>
                        <div className={'btn fall zero'}>
                            {this.state.chargeUnit.mul(this.state.volume[this.state.volumeIndex]).mul(this.state.moneyType === 0 ? 1 : 0.1)}元
                        </div>
                    </div>
                    <div className={'grid row-des'}>
                        <div className={'contract'}>
                            最新买入价 <span className={this.state.tend}>{this.state.price}</span>
                        </div>
                        <div className={'des'}>
                            持仓至{this.state.close}自动平仓
                        </div>
                    </div>
                    <div className={this.props.location.state.isBuy ?'submit raiseBg':'submit fallBg'} onClick={this.submit.bind(this)}>
                        确定{this.props.location.state.isBuy ? '买涨' : '买跌'}
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        if (this._init) {
            if (this.state.stopLoss.length === 1 && !!Cache.tradeList) {
                this.updateSchemeInfo();
            }
            // Quotes.start('quoteUpdate', this.props.location.state.contract);
        } else {
            let c = Contracts.getContract(this);
            if (!!c) {
                // Quotes.start('quoteUpdate', c);
                this.setState({
                    hasMoneyType: Contracts.total[c].moneyType,
                    code: c,
                    name: Contracts.total[c].name
                });
            } else {
                Schedule.addEventListener('contractsInitial', this.updateContracts, this);
            }
        }

        if (this.state.stopLoss.length === 1 && !!Cache.tradeList) {
            this.updateSchemeInfo();
        } else {
            Schedule.addEventListener('getSchemeInfo', this.updateSchemeInfo, this);
        }
        Schedule.addEventListener('getUserInfo', this.updateUserInfo, this);
        Schedule.addEventListener('quoteUpdate', this.priceUpdate, this);
    }

    componentWillUnmount() {
        // Quotes.end();
        Schedule.removeEventListeners(this);
    }

    updateContracts() {
        let c = Contracts.getContract(this);
        if (!!c) {
            this.setState({
                hasMoneyType: Contracts.total[c].moneyType,
                code: c,
                name: Contracts.total[c].name
            });
            // Quotes.start('quoteUpdate', c);
        }
    }

    updateSchemeInfo(init, code) {
        // return console.log(scheme,scheme,1111)
        if (!Cache.isLogin())
            return;
        let scheme = Cache.tradeList[code || this.props.location.state.contract];
        if (init) {
            this.state.volume = scheme.volumeList;
            this.state.stopLoss = scheme.stopLossList;
            this.state.stopProfit = scheme.stopProfitList;
            this.state.chargeUnit = scheme.chargeUnit;
            this.state.close = formatDate('h:i:s', {date: getCloseTime(scheme.closeTime)});
        } else {
            this.setState({
                volume: scheme.volumeList,
                stopLoss: scheme.stopLossList,
                stopProfit: scheme.stopProfitList,
                chargeUnit: scheme.chargeUnit,
                close: formatDate('h:i:s', {date: getCloseTime(scheme.closeTime)})
            });
            if (code){
                this.setState({
                    hasMoneyType: Contracts.total[code].moneyType,
                    code: code,
                    name: Contracts.total[code].name,
                    moneyType:0
                })
            }
        }
    }

    updateUserInfo() {
        this.setState({
            balance: this.props.location.state.simulate ? Cache.gameBalance : Cache.realBalance
        })
    }

    priceUpdate(data) {
        const prev = data.settle_price_yes || data.close;
        let rate = data.price.sub(prev);
        let tend = rate > 0 ?'raise':'fall';
        this.setState({
            tend:tend
        });
        const {priceDigit} = Contracts.total[data.code];
        if (this.props.location.state.isBuy) {
            this.setState({price: data.wt_sell_price.toFixed(priceDigit)});
        } else {
            this.setState({price: data.wt_buy_price.toFixed(priceDigit)});
        }
    }


    async submit() {
        try {
            const o = this.props.location.state.code;
            const obj = Contracts.total[o];
            const scheme = Cache.tradeList[o];
            const handle = this.state.volume[this.state.volumeIndex];
            const result = await Req({
                url: '/api/trade/open.htm',
                type: 'POST',
                data: {
                    identity: getIdentity(16),
                    tradeType: this.props.location.state.simulate ? 2 : 1,//模拟交易2 实盘交易1
                    source: '下单',  // 买入来源（下单、反向、快捷)
                    commodity: obj.code,
                    contract: o,
                    isBuy: this.props.location.state.isBuy,
                    price: 0,
                    stopProfit: this.state.stopProfit[this.state.stopLossIndex].mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    stopLoss: this.state.stopLoss[this.state.stopLossIndex].mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    serviceCharge: scheme.chargeUnit.mul(handle).mul(this.state.moneyType === 0 ? 1 : 0.1),
                    eagleDeduction: 0,
                    volume: handle,
                    moneyType: this.state.moneyType,
                    platform: getPlatform()
                },
                animate: true
            });
            AlertFunction({title: '提示', msg: result.errorMsg});
            Cache.getUserInfo();
            this.props.close();
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg});
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location.state.contract !== nextProps.location.state.contract) {
            this.updateSchemeInfo(false, nextProps.location.state.contract);
        }

        if (this.props.show !== nextProps.show) {
            if (nextProps.show) {
                this.setState({
                    volumeIndex: 0,
                    stopLossIndex: 0
                })
            }
        }
    }
}