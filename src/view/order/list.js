import React, {Component} from 'react';

export default class App extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <ul className={this.props.className}>
                {this.props.list.map((i,key)=><li className={this.props.select===key?'active':''} onClick={()=>this.props.pick(key)}>{this.props.plus?i.mul(this.props.plus):i}{this.props.unit}</li>)}
            </ul>
        )
    }
}