import React, {Component} from 'react'
import Header from '../../view/common/header'
import {Req} from './../../lib/index'
import {formatDate} from './../../lib/tool'

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            money: 0,
            assetMoney: 0,
            type: 0,
            date: null,
            desc: '-',
            explain: '-'
        }
    }

    componentDidMount() {
        let id = window.location.href.split('/')[4];
        this.update(id);
    }

    async update(id) {
        try {
            let result = await Req({
                url: '/api/mine/fundDetail.htm',
                data: {
                    id: id
                },
                animate: true
            });
            this.setState({
                money: result.fundDetail.money,
                assetMoney: result.fundDetail.assetMoney,
                type: result.fundDetail.type,
                date: result.fundDetail.time.time,
                desc: result.fundDetail.detail,
                explain: result.fundDetail.explain
            });
        } catch (e) {
            console.log(2);
        }
    }

    render() {

        let date = new Date(this.state.date);
        let dateString = formatDate('y-m-d h:i:s', {date});
        let amountClassName = this.state.type === 200 ? 'fall' : 'raise';
        return (
            <div className='moneyListDetail'>
                <Header title={'资金明细'} {...this.props}/>
                <div className='moneyListDetail-content'>
                    <div>
                        <div>{this.state.explain}</div>
                        <div className={amountClassName}>{(this.state.type === 200 ? '-' : '+') + this.state.money.toFixed(2)}</div>
                    </div>
                    <div>
                        <div>账户余额</div>
                        <div>{this.state.assetMoney.toFixed(2)}</div>
                    </div>
                    <div>
                        <div>创建时间</div>
                        <div>{dateString}</div>
                    </div>
                    <div>{'详情：' + this.state.desc}</div>
                </div>
            </div>
        );
    }
}