import React ,{Component} from 'react'
import Svg from 'react-svg'
import {Link} from 'react-router-dom'
import {formatDate} from './../../lib/tool'
import {Req} from './../../lib/index'
import ReactPullLoad, {STATS} from 'react-pullload'

import empty from '../../images/empty.png';
import rightArrow from './../../images/svg/right-arrow.svg'

export default class App extends Component{

    // type 200=withdraw
    // type 100=topup

    mount = true
    constructor(props){
        super(props);

        this.state = {
            items:null,
            action: STATS.init,
            hasMore:true
        }
    }

    componentDidMount(){
        this.update(true)
    }

    componentWillUnmount(){
        this.mount = false
    }

    async update(animate){
        try {
            let result = await Req({
                url:'/api/mine/funds.htm',
                data:{
                    action:'more',
                    type:1
                },
                animate:animate
            });
            if (this.mount === false) {
                return
            }

            if (animate){
                this.setState({
                    items:result.data,
                });
            } else{
                this.setState({
                    items:result.data,
                    action: STATS.refreshed,
                    hasMore:true
                });
            }

        }catch (e) {
            console.log(e);
        }
    }

    async loadMore(beginId){
        try {
            let result = await Req({
                url:'/api/mine/funds.htm',
                data:{
                    action:'more',
                    type:1,
                    beginId:beginId
                }
            });
            if (this.mount === false) {
                return
            }
            if (result.data.length === 0) {
                this.setState({
                    hasMore:false,
                    action: STATS.reset
                });
            } else{
                this.setState({
                    items:this.state.items.concat(result.data),
                    action: STATS.reset
                });
            }

        }catch (e) {
            console.log(e);
        }
    }

    handleAction = (action) => {
        console.info(action, this.state.action, action === this.state.action);
        //new action must do not equel to old action
        if (action === this.state.action) {
            return false
        }

        if (action === STATS.refreshing) {
            this.handRefreshing();
        } else if (action === STATS.loading) {
            this.handLoadMore();
        } else {
            //DO NOT modify below code
            if (this.mount === false) {
                return
            }
            this.setState({action: action})
        }
    }

    handRefreshing = () => {
        if (STATS.refreshing === this.state.action) {
            return false
        }

        this.update(false)
        if (this.mount === false) {
            return
        }
        this.setState({action: STATS.refreshing})
    }

    handLoadMore = () => {
        if (STATS.loading === this.state.action) {
            return false
        }
        //无更多内容则不执行后面逻辑
        if (!this.state.hasMore) {
            return;
        }

        if(this.state.items){
            let lastElement = this.state.items[this.state.items.length-1];
            let beginId = lastElement.id;

            this.loadMore(beginId);
            if (this.mount === false) {
                return
            }
            this.setState({action: STATS.loading})
        }
    }

    renderItem(){
        let content = this.state.items.map((item,i)=>{

            let amount = item.money.toFixed(2);
            amount = item.type === 200 ? '-'+amount : '+'+amount;

            let amountClassName = item.type === 200 ? 'accountRemove' : 'accountAdd';

            let date = new Date(item.time);
            let dateString = formatDate('y-m-d h:i:s',{date})

            return(
                <li key={i}>
                    <Link to={'/moneyListDetail/'+item.id}>
                        <div>
                            <div>{item.explain}</div>
                            <div>{dateString}</div>
                        </div>
                        <div>
                            <div className={amountClassName}>{amount}</div>
                            <Svg path={rightArrow}/>
                        </div>
                    </Link>

                </li>
            );
        });
        return content;
    }

    render(){
        return(
            <div className={'pullBody'}>
                <ReactPullLoad
                    downEnough={50}
                    action={this.state.action}
                    handleAction={this.handleAction}
                    hasMore={this.state.hasMore}
                    style={{
                        backgroundColor: "#fff"
                    }}
                    distanceBottom={1000}>
                    {this.state.items === null?'':(
                        this.state.items.length === 0?(
                            <div className={'imgWrap'}>
                                <img src={empty} alt="" className={'placeholder'}/>
                            </div>
                        ):(
                            <ul className='moneyList-all'>
                                {this.renderItem()}
                            </ul>
                        )
                    )}
                </ReactPullLoad>
            </div>
        );
    }
}