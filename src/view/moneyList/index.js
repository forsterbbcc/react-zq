import React,{Component} from 'react'
import Header from './../common/header'
import RouteWithSubRoutes from '../../routes/routeWithSubRoutes'
import {NavLink} from 'react-router-dom'
import Balance from './../common/balance'
export default class App extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const a = this.props.routes;
        return(
            <div className='moneyList'>
                <Header title={'资金明细'}{...this.props} back={'/mine'}/>
                <div className={'main'}>
                    <Balance/>
                    <ul className='category'>
                        <NavLink to={'/list/all'} activeClassName={'selected'}>全部</NavLink>
                        <NavLink to={'/list/transaction'} activeClassName={'selected'}>充值提款</NavLink>
                        <NavLink to={'/list/transactionDetail'} activeClassName={'selected'}>交易明细</NavLink>
                        <NavLink to={'/list/promotionCommission'} activeClassName={'selected'}>推广佣金</NavLink>
                    </ul>
                    {a.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
                </div>
            </div>
        );
    }
}