import React, {Component} from 'react';
import {Header} from "../common";
import ReactSvg from 'react-svg';
import {Cache} from "../../module";
import {Schedule} from "../../lib";
import {Req} from "../../lib";
import {Link} from 'react-router-dom'

//todo 引入图片
import close from '../../images/svg/wrong.svg';
import AlertFunction from "../../lib/AlertFunction";
import warn from '../../images/warn.png';


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            withdrawPass: '',
            password: '',
            newWithdrawPass: '',
            comWithdrawPass: ''
        };
    }

    render() {
        return (
            <div className={'changePassword'}>
                <Header title={'提款密码'} {...this.props}/>
                <div className={'main'}>
                    <div className={'warn'}>
                        <img src={warn} alt=""/>
                        <p>初次使用登录密码设置</p>
                    </div>
                    <ul>
                        <li>
                            <span>{this.state.withdrawPass.length !== 0 ? '旧提款密码' : '登录密码'}</span>
                            <input maxLength={16}
                                type="password" placeholder={this.state.withdrawPass.length !== 0 ? '请输入旧提款密码' : '请输入登录密码'} value={this.state.password}
                                   onChange={(e)=>this.setState({password:e.target.value.replace(/[^\w]/g,'')})}/>
                            {
                                this.state.withdrawPass.length !== 0 && !this.state.password?
                                    (<Link to={'/recoverWithdrawPassword'} className={'forgotPassword'}>忘记密码?</Link>):
                                    (null)
                            }

                            {
                                this.state.password.length !== 0 ? (
                                    <div className={'closeBox'} onClick={() => this.setState({password: ''})}>
                                        <ReactSvg path={close} className={'close'}/>
                                    </div>
                                ) : (null)
                            }
                        </li>
                        <li>
                            <span>提款密码</span>
                            <input maxLength={16}
                                type="password" placeholder={'请输入提款密码'} value={this.state.newWithdrawPass}
                                   onChange={(e)=>this.setState({newWithdrawPass:e.target.value.replace(/[^\w]/g,'')})}/>
                            {
                                this.state.newWithdrawPass.length !== 0 ? (
                                    <div className={'closeBox'} onClick={() => this.setState({newWithdrawPass: ''})}>
                                        <ReactSvg path={close} className={'close'}/>
                                    </div>
                                ) : (null)
                            }
                        </li>
                        <li>
                            <span>确认密码</span>
                            <input maxLength={16}
                                type="password" placeholder={'请再次输入新密码'} value={this.state.comWithdrawPass}
                                   onChange={(e)=>this.setState({comWithdrawPass:e.target.value.replace(/[^\w]/g,'')})}/>
                            {
                                this.state.comWithdrawPass.length !== 0 ? (
                                    <div className={'closeBox'} onClick={() => this.setState({comWithdrawPass: ''})}>
                                        <ReactSvg path={close} className={'close'}/>
                                    </div>
                                ) : (null)
                            }
                        </li>
                    </ul>
                    <div className={'buttonBox'}>
                        <p className={'tips'}>6-16位数字、字母组合（特殊字符除外）</p>
                        <div className={'submit'} onClick={() => this.submit()}>确认</div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback();
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this);
        }
        Schedule.addEventListener('getUserInfo', this.getInfoCallback, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this)
    }

    loginCallback() {
        this.setState({
            isLogin: Cache.isLogin()
        });
        this.getInfoCallback();
    }

    getInfoCallback() {
        this.setState({
            withdrawPass: Cache.withdrawPass
        })
    }

    //todo 提交
    submit() {
        if(this.state.password.length !== 0 || this.state.newWithdrawPass.length !== 0 ||this.state.comWithdrawPass.length !== 0){
            Req({
                url: '/api/mine/atmPasswd.htm',
                type: 'POST',
                data: {
                    password: this.state.password,
                    withdrawPw: this.state.newWithdrawPass,
                    withdrawPwCfm: this.state.comWithdrawPass
                }
            }).then((data) => {
                AlertFunction({title:'提示',msg:data.errorMsg, confirm:()=>{window.history.back()}});
                Cache.getUserInfo()
            }).catch((err)=>{
                AlertFunction({title:'错误',msg:err.errorMsg})
            })
        }else{
            AlertFunction({title:'警告',msg:'提交信息不能为空！'})
        }
    }
}