import React, {Component} from 'react';
import {Header} from "../common";
import {Cache} from "../../module";
import {Schedule} from "../../lib";
import {mobileMask} from "../../lib/tool";
import {Link} from 'react-router-dom';
import {Req} from "../../lib";
import AlertFunction from '../../lib/AlertFunction'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            phone: '',
            code: '',
            showImg: false,
            imageAddress: '',
            imageCode: '',
            step: 1,
            newPhone: '',
            count: '获取验证码',
            countNumber: 60,
        };
    }

    render() {
        return (
            <div className={'changePhoneNumber'}>
                <Header title={'修改手机号码'} {...this.props}/>
                <div className={'main'}>
                    <ul>
                        <li>
                            <span>手机号码</span>
                            {
                                this.state.step === 1 ? (
                                    <input type="tel" value={mobileMask(this.state.phone)} disabled={true}
                                           maxLength={11}/>
                                ) : (
                                    <input type="tel" placeholder={'请输入新的手机号码'} value={this.state.newPhone}
                                           onChange={(e) => this.setState({newPhone: e.target.value})} maxLength={11}/>
                                )
                            }
                        </li>
                        <li>
                            <span>验证码</span>
                            <div>
                                <input type="tel" placeholder={'请输入短信验证码'} value={this.state.code}
                                       onChange={(e) => this.setState({code: e.target.value})} maxLength={4}/>

                                <div className={'send'} onClick={() => this.show()}>
                                    {this.state.count}
                                </div>
                            </div>
                        </li>
                    </ul>
                    {
                        this.state.showImg ? (
                            <div className={'cover'}>
                                <div className={'codeBox'}>
                                    <p>获取验证码前,请您先填写下面数字</p>
                                    <div className={'code'}>
                                        <img src={this.state.imageAddress} alt=""/>
                                        <input type="text" placeholder={'请填写图片验证码'} value={this.state.imageCode} maxLength={4}
                                               onChange={(e) => this.setState({imageCode: e.target.value.replace(/[^\d]/g,'')})}/>
                                    </div>
                                    <div className={'codeButton'}>
                                        <div onClick={() => this.setState({showImg: false, imageCode: ''})}>取消</div>
                                        <div className={'sure'} onClick={() => this.confirmCode()}>确定</div>
                                    </div>
                                </div>
                            </div>
                        ) : (null)
                    }
                    {
                        this.state.step === 1 ? (
                            <div className={'buttonBox'}>
                                <div className={'submit'} onClick={() => this.nextStep()}>下一步</div>
                            </div>
                        ) : (
                            <div className={'buttonBox'}>
                                <div className={'submit'} onClick={() => this.submit()}>绑定</div>
                            </div>
                        )
                    }

                    <p>如遇问题，请<Link to={'/cs'}>联系客服</Link></p>
                </div>
            </div>
        )
    }

    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback()
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this)
        }
        Schedule.addEventListener('getUserInfo', this.getInfoCallback, this)
    }

    loginCallback() {
        this.setState({
            isLogin: Cache.isLogin()
        });
        this.getInfoCallback()
    }

    getInfoCallback() {
        this.setState({phone: Cache.phone})
    }

    //todo 下一步
    async nextStep() {
        //todo 如果验证码为空时 不能发送数据
        if (this.state.code.length !== 0) {
            try {
                let result = await Req({
                    url: "/api/mine/mobile.htm",
                    type: 'POST',
                    data: {
                        action: 'verify',
                        type: 1,
                        verifyCode: this.state.code
                    },
                    animate:true
                });
                if (result.code === 200) {
                    this.setState({step: 2, code: '', imageCode: '', countNumber: 60, count: '获取验证码'});
                    clearInterval(this.time)
                }

            } catch (err) {
                AlertFunction({title: '错误', msg: err.errorMsg})
            }
        } else {
            AlertFunction({title: '警告', msg: '请填写验证码！'})
        }
    }

    //todo 绑定新手机
    submit() {
        //手机号为空时,不能发送数据
        if(!!this.state.newPhone){
            //todo 如果验证码为空时 不能发送数据
            if (this.state.code.length !== 0) {
                Req({
                    url: "/api/mine/mobile.htm",
                    type: 'POST',
                    data: {
                        action: 'verify',
                        type: 2,
                        verifyCode: this.state.code
                    },
                    animate: true
                }).then((date) => {
                    AlertFunction({title: '提示', msg: date.errorMsg, confirm: () => window.history.back()})
                    Cache.getUserInfo()
                }).catch((err) => {
                    AlertFunction({title: '错误', msg: err.errorMsg})
                })
            } else {
                AlertFunction({title: '警告', msg: '请填写验证码！'})
            }
        }else{
            AlertFunction({title: '警告', msg: '请填新的手机号码！'})
        }
    }

    //todo 显示图片验证码
    show() {
        // if (!/^1[203456789]\d{9}$/.test(this.state.newPhone)) return AlertFunction({title: '警告', msg: '请输入正确的手机号'});
        // if (!this.state.newPhone) return AlertFunction({title: '提示', msg: '请先输入您的手机号!'});
        if (this.state.countNumber !== 0 && this.state.count !== '获取验证码') {
            return AlertFunction({title: '警告', msg: '验证码已经发送，请稍后再试！'})
        }

        this.setState({
            showImg: true,
            imageAddress: `/api/vf/verifyCode.jpg?_=${new Date()}`,
            imageCode:''
        });

    }

    //todo 确定图片验证码
    async confirmCode() {
        if (this.state.imageCode.length !== 4) {
            AlertFunction({title: '警告', msg: '请输入正确的验证码', confirm: () => this.show()})
        } else {
            let action = this.state.step === 1 ? 'sendVerify' : 'sendVerifyNew';
            let phoneNumber = this.state.step === 1 ? this.state.phone : this.state.newPhone;
            try {
                let result = await Req({
                    url: '/api/mine/mobile.htm',
                    type: 'POST',
                    data: {
                        action: action,
                        mobile: phoneNumber,
                        imageCode: this.state.imageCode
                    },
                    animate: true
                });
                if (result.code === 200) {
                    AlertFunction({
                        title: '提示',
                        msg: result.errorMsg,
                        confirm: () => this.setState({showImg: false})
                    });
                    this.countDown()
                }
            } catch (err) {
                if(err.errorMsg.match('手机')){
                    AlertFunction({title: '错误', msg: err.errorMsg,
                        confirm: () => {
                        this.show();
                        this.setState({
                            showImg:false,
                            newPhone:''
                        })
                    }})
                }else{
                    AlertFunction({title: '错误', msg: err.errorMsg, confirm: () => this.show()})
                }
            }
        }
    }

    //todo 倒计时
    countDown() {
        if (this.state.countNumber <= 0) {
            this.setState({
                count: '获取验证码',
                countNumber: 60
            })
        } else {
            this.time = setTimeout(() => {
                this.setState({
                    countNumber: this.state.countNumber - 1,
                    count: `${this.state.countNumber}秒后重发`,
                });
                this.countDown()
            }, 1000)

        }
    }
}