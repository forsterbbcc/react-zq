import React, {Component} from 'react';
import {Alert, Footer, Svg} from "../common";
import {Link} from 'react-router-dom'
import {Cache} from "../../module";
import {Schedule} from "../../lib";
import {nameMark, mobileMask, idMark} from "../../lib/tool";

import user from '../../images/user.png'
import gift from '../../images/gift.png'
import phone from '../../images/svg/phone.svg'
import logout from '../../images/svg/logout.svg'
import realName from '../../images/svg/realName.svg'
import bankCard from '../../images/svg/bankCard.svg'
import withdraw from '../../images/svg/withDraw.svg'
import share from '../../images/svg/share.svg'
import agent from '../../images/svg/agent.svg'
import myAgent from '../../images/svg/myAgent.svg'
import logout0 from '../../images/svg/logout0.svg'
import position from '../../images/svg/positionSearch.svg'
import list from '../../images/svg/list.svg'
import rightArrow from '../../images/rightArrow.png'
import AlertFunction from "../../lib/AlertFunction";
export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            name: '',
            bankCardCount: '',
            aLiPay: '',
            phone: '',
            withdrawPass: '',
            userLevel:Cache.userLevel,
            userName:'',
            userId:Cache.userId,
            realBalance:'',
        };
    }

    render() {
        return (
            <div className={'mineBox'}>
                <div className={'main'}>
                    <div className={'title'}>
                        <div>
                            <img src={user} alt=""/>
                            <div>{this.state.userName}</div>
                        </div>
                        <div>
                            <div onClick={()=>{
                                this.logout()
                            }}>
                                {/*<span>登出</span>&nbsp;*/}
                                <Svg path={logout0} className={'logout0'}/>
                            </div>
                        </div>
                    </div>
                    <div className={'balanceBox'}>
                        <div className={'left'}>
                            <div>账户余额(元)</div>
                            <b>{this.state.realBalance}</b>
                        </div>
                        <div className={'right'}>
                            <Link to={'/recharge'} className={'bg'}>
                                <div>充值</div>
                            </Link>
                            <Link to={'/withdraw/card'}>
                                <div>提现</div>
                            </Link>
                        </div>
                    </div>
                    <div className={'list'}>
                        <Link to={'/realName'}>
                            <div className={'listLeft'}>
                                <Svg path={realName} className={'orange'}/>
                                <span>实名认证</span>
                            </div>
                            <div className={'listRight'}>
                                {
                                    !!this.state.name && this.state.name.length !== 0 ?(
                                        <span>{nameMark(this.state.name)}</span>
                                    ):(<span>未认证</span>)
                                }
                                <img src={rightArrow} className={'arrow'}/>
                            </div>
                        </Link>
                        <Link to={'/bankCardList'}>
                            <div className={'listLeft'}>
                                <Svg path={bankCard} className={'blue'}/>
                                <span>提款银行卡</span>
                            </div>
                            <div className={'listRight'}>
                                <span>{this.state.bankCardCount}张</span>
                                <img src={rightArrow} className={'arrow'}/>
                            </div>
                        </Link>
                        <Link to={'/changePhoneNumber'}>
                            <div className={'listLeft'}>
                                <Svg path={phone}  className={'green'}/>
                                <span>手机绑定</span>
                            </div>
                            <div className={'listRight'}>
                                <span>{this.state.phone.length !== 0 ? mobileMask(this.state.phone) : '未绑定'}</span>
                                <img src={rightArrow} className={'arrow'}/>
                            </div>
                        </Link>
                        <Link to={'/changePassword'}>
                            <div className={'listLeft'}>
                                <Svg path={logout}  className={'pink'}/>
                                <span>登录密码</span>
                            </div>
                            <div className={'listRight'}>
                                <span>修改</span>
                                <img src={rightArrow} className={'arrow'}/>
                            </div>
                        </Link>
                        <Link to={'/changeWithdrawPassword'}>
                            <div className={'listLeft'}>
                                <Svg path={withdraw}  className={'brown'}/>
                                <span>提款密码</span>
                            </div>
                            <div className={'listRight'}>
                                <span>{this.state.withdrawPass.length !== 0 ? '已设置' : '未设置'}</span>
                                <img src={rightArrow} className={'arrow'}/>
                            </div>
                        </Link>
                    </div>
                    <div className={'list'}>
                        <Link to={{pathname:'/position/live',state:{simulate:false}}}>
                            <div className={'listLeft'}>
                                <Svg path={position} className={'orange'}/>
                                <span>我的持仓</span>
                            </div>
                            <div className={'listRight'}>
                                <img src={rightArrow} className={'arrow'}/>
                            </div>
                        </Link>
                        <Link to={'/list/all'}>
                            <div className={'listLeft'}>
                                <Svg path={list} className={'blue'}/>
                                <span>资金明细</span>
                            </div>
                            <div className={'listRight'}>
                                <img src={rightArrow} className={'arrow'}/>
                            </div>
                        </Link>
                        <Link to={'/recommendFriend'}>
                            <div className={'listLeft'}>
                                <Svg path={share} className={'green'}/>
                                <span>推荐好友</span>
                            </div>
                            <div className={'listRight'}>
                                <img src={rightArrow} className={'arrow'}/>
                            </div>
                        </Link>
                        {/*<Link to={'/promotion'}>*/}
                            {/*<div className={'listLeft'}>*/}
                                {/*<Svg path={myAgent} className={'pink'}/>*/}
                                {/*<span>我的经纪人</span>*/}
                            {/*</div>*/}
                            {/*<div className={'listRight'}>*/}
                                {/*<img src={rightArrow} className={'arrow'}/>*/}
                            {/*</div>*/}
                        {/*</Link>*/}
                        {/*<Link to={'/myUser'}>*/}
                            {/*<div className={'listLeft'}>*/}
                                {/*<Svg path={agent} className={'brown'}/>*/}
                                {/*<span>我的用户</span>*/}
                            {/*</div>*/}
                            {/*<div className={'listRight'}>*/}
                                {/*<img src={rightArrow} className={'arrow'}/>*/}
                            {/*</div>*/}
                        {/*</Link>*/}
                    </div>
                </div>
                <Footer/>
            </div>
        )
    }

    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback();
        }else{
            Schedule.addEventListener('cacheInitial', this.loginCallback, this);
        }
        Schedule.addEventListener('getUserInfo', this.getInfoCallback, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this)
    }

    //todo 退出
    logout(){
        AlertFunction({title:'警告',msg:'是否退出账号？',hasCancel:true, confirm:()=>Cache.logout()})
    }

    loginCallback() {
        this.setState({
            isLogin: Cache.isLogin()
        });
        this.getInfoCallback();
    }

    getInfoCallback() {
        this.setState({
            name: Cache.realName,
            bankCardCount: Cache.bankCardCount,
            phone: Cache.phone,
            withdrawPass: Cache.withdrawPass,
            userLevel : Cache.userLevel,
            userName:Cache.username,
            userId:Cache.idNumber,
            realBalance:Cache.realBalance
        })
    }
}