import React, {Component} from 'react';
import {Header, SelectBank} from "../common";
import {Cache} from "../../module";
import {Schedule} from "../../lib";
import {nameMark} from "../../lib/tool";
import {provinceData, cityeData} from "../../lib/address";
import {Req} from "../../lib";
import {Svg} from "../common";

import back from '../../images/svg/back.svg'
import arrow from '../../images/svg/right-arrow.svg'
import warn from '../../images/warn.png'
import AlertFunction from "../../lib/AlertFunction";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            realName: Cache.realName,
            bankType: '请选择银行',
            province: '请选择开户省份',
            city: '请选择开户城市',
            select: 0,  //todo 0 未选择  1 选择银行  2 选择省份  3 选择城市
            provinceList: [],
            cityList: [],
            subbranch: '',
            cardNumber: '',
            cfmCardNumber: ''
        };

        this.selectBank = this.selectBank.bind(this)
    }

    render() {
        // console.log(this.props.location.bankList);
        return (
            <div className={'addBank'}>
                <Header title={'添加银行卡'} {...this.props}/>
                <div className={'main'}>
                    <div className={'warn'}>
                        <img src={warn} alt=""/>
                        <p>为确保资金安全，只能添加"{nameMark(this.state.realName)}"的银行卡</p>
                    </div>
                    <ul>
                        <li>
                            <span>开户姓名</span>
                            <div>{nameMark(this.state.realName)}</div>
                        </li>
                        <li>
                            <span>开户银行</span>
                            <div onClick={() => this.setState({select: 1})}>{this.state.bankType}</div>
                        </li>
                        <li>
                            <span>开户省份</span>
                            <div onClick={() => {
                                this.setState({
                                    select: 2,
                                    provinceList: provinceData(),
                                    city:'请选择开户城市'
                                })
                            }}>{this.state.province}</div>
                        </li>
                        <li>
                            <span>开户城市</span>
                            <div onClick={() => {
                                if (this.state.province === '请选择开户省份') return AlertFunction({
                                    title: '警告',
                                    msg: '请先选择省份'
                                });
                                this.setState({select: 3, cityList: cityeData(this.state.province)})
                            }}>{this.state.city}</div>
                        </li>
                        <li>
                            <span>开户支行</span>
                            <input type="text" placeholder={'请输入开户支行'} value={this.state.subbranch}
                                   onChange={(e) => this.setState({subbranch: e.target.value})}/>
                        </li>
                        <li>
                            <span>银行卡号</span>
                            <input type="tel" placeholder={'请输入银行卡号'} value={this.state.cardNumber}
                                   onChange={(e) => this.setState({cardNumber: e.target.value.replace(/\D/g, '').replace(/....(?!$)/g, '$& ')})}
                                   minLength={16} maxLength={23}/>
                        </li>
                        <li>
                            <span>确认卡号</span>
                            <input type="tel" placeholder={'请在此确认银行卡号'} value={this.state.cfmCardNumber}
                                   onChange={(e) => this.setState({cfmCardNumber: e.target.value.replace(/\D/g, '').replace(/....(?!$)/g, '$& ')})}
                                   minLength={16} maxLength={23}/>
                        </li>
                    </ul>
                    <div className={'submit'} onClick={() => this.submit()}>确定</div>
                </div>
                {
                    this.state.select === 1 ? (
                        <SelectBank selectBank={this.selectBank}/>
                    ) : (null)
                }
                {
                    this.state.select === 2 ? (
                        <div className={'select'}>
                            <div className={'selectHeader'}>
                                <div className={'backTag'} onClick={() => this.setState({select: 0})}>
                                    <Svg path={back} className={'arrow'}/>
                                </div>
                                <div className={'title'}>请选择省份</div>
                            </div>
                            <div className={'selectMain'}>
                                <ul>
                                    {
                                        this.state.provinceList.map((item) => {
                                            return (
                                                <li onClick={() => this.setState({
                                                    select: 0,
                                                    province: item.value
                                                })}>{item.value}<Svg path={arrow} className={'arrow'}/></li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    ) : (null)
                }
                {
                    this.state.select === 3 ? (
                        <div className={'select'}>
                            <div className={'selectHeader'}>
                                <div className={'backTag'} onClick={() => this.setState({select: 0})}>
                                    <Svg path={back} className={'arrow'}/>
                                </div>
                                <div className={'title'}>请选择城市</div>
                            </div>
                            <div className={'selectMain'}>
                                <ul>
                                    {
                                        this.state.cityList.map((item) => {
                                            return (
                                                <li onClick={() => this.setState({
                                                    select: 0,
                                                    city: item.value
                                                })}>{item.value}<Svg path={arrow} className={'arrow'}/></li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    ) : (null)
                }
            </div>
        )
    }

    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback()
        } else {
            Schedule.addEventListener('cacheInitial', this.loginCallback, this)
        }

        if (Cache.realName.length === 0) {
            AlertFunction({
                title: '提示', msg: '您当前还未实名认证，为保障您的账户安全，请先实名认证', hasCancel: true, cancel: () => {
                    window.history.back()
                }, confirm: () => {
                    this.props.history.push('/realName')
                }
            })
        }
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this)
    }

    loginCallback() {
        this.setState({isLogin: Cache.isLogin(), realName: Cache.realName})
    }

    //todo 选择银行
    selectBank(bank, type) {
        bank === '' ? this.setState({bankType: '请选择银行',select:0}) : this.setState({bankType: bank, select: type})
    }

    //todo 添加银行卡
    submit() {
        if (this.state.bankType.length === 0 || this.state.bankType === '请选择银行') return AlertFunction({
            title: '警告',
            msg: '请选择开户银行'
        });
        if (this.state.province === '请选择开户省份') return AlertFunction({title: '警告', msg: '请选择开户省份'});
        if (this.state.city === '请选择开户城市') return AlertFunction({title: '警告', msg: '请选择开户城市'});
        if (this.state.cardNumber.length === 0 || this.state.cardNumber.length < 16) return AlertFunction({
            title: '警告',
            msg: '银行卡格式不正确'
        });
        if (this.state.cfmCardNumber.length === 0 || this.state.cfmCardNumber.length < 16) return AlertFunction({
            title: '警告',
            msg: '银行卡格式不正确'
        });
        if (!/^[\u4e00-\u9fa5]{0,}$/g.test(this.state.subbranch) || this.state.subbranch.length === 0) return AlertFunction({
            title: '警告',
            msg: '开户支行格式错误，请重新输入'
        });
        // if(!!this.props.location.bankList){
        //     const result = this.props.location.bankList.filter((item,index)=>{
        //         return item.cardNumber === this.state.cardNumber
        //     });
        //     if(!!result){
        //         AlertFunction({
        //             title: '警告',
        //             msg: '该卡号已被绑定,请绑定其他卡号!'
        //         });
        //     }
        // }

        Req({
            url: '/api/mine/bankCardAdd.htm',
            type: 'POST',
            data: {
                action: 'add',
                bank: this.state.bankType,
                province: this.state.province,
                city: this.state.city,
                subbranch: this.state.subbranch,
                cardNumber: this.state.cardNumber.replace(/\s+/g, ""),
                cardNumberCfm: this.state.cfmCardNumber.replace(/\s+/g, ""),
            },
            animate: true
        }).then((data) => {
            AlertFunction({title: '提示', msg: data.errorMsg, confirm: () => window.history.back()})
            Cache.getUserInfo()
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
        })
    }

}