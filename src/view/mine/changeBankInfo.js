import React, {Component} from 'react';
import {Header} from "../common";
import {nameMark} from "../../lib/tool";
import AlertFunction from "../../lib/AlertFunction";
import {provinceData, cityeData} from "../../lib/address";
import {Svg} from "../common";
import {Cache} from "../../module";
import {idMark} from "../../lib/tool";

//todo 图片的路径
import ICBC from '../../images/bankIcon/icbc.png'
import CMB from '../../images/bankIcon/cmb.png'
import CCB from '../../images/bankIcon/ccb.png'
import ABC from '../../images/bankIcon/abc.png'
import BOC from '../../images/bankIcon/boc.png'
import COMM from '../../images/bankIcon/comm.png'
import CMBC from '../../images/bankIcon/cmbc.png'
import SPDB from '../../images/bankIcon/spdb.png'
import CITIC from '../../images/bankIcon/citic.png'
import GDB from '../../images/bankIcon/gdb.png'
import SZPAB from '../../images/bankIcon/szpab.png'
import CIB from '../../images/bankIcon/cib.png'
import HXB from '../../images/bankIcon/hxb.png'
import CEB from '../../images/bankIcon/ceb.png'
import PSBC from '../../images/bankIcon/psbc.png'
import back from '../../images/svg/back.svg'
import arrow from '../../images/svg/right-arrow.svg'
import {Req} from "../../lib";


function a(name) {
    switch (name) {
        case '工商银行':
            return ICBC;
        case '招商银行':
            return CMB;
        case '建设银行':
            return CCB;
        case '农业银行':
            return ABC;
        case '中国银行':
            return BOC;
        case '交通银行':
            return COMM;
        case '民生银行':
            return CMBC;
        case '浦发银行':
            return SPDB;
        case '中信银行':
            return CITIC;
        case '广发银行':
            return GDB;
        case '平安银行':
            return SZPAB;
        case '兴业银行':
            return CIB;
        case '华夏银行':
            return HXB;
        case '光大银行':
            return CEB;
        case '邮政储蓄':
            return PSBC;
            break;
    }
}

//todo 根据不同银行显示不同颜色
function color(name) {
    switch (name) {
        case '工商银行':
            return 'redCard';
        case '招商银行':
            return 'redCard';
        case '建设银行':
            return 'blueCard';
        case '农业银行':
            return 'greenCard';
        case '中国银行':
            return 'redCard';
        case '交通银行':
            return 'blueCard';
        case '民生银行':
            return 'greenCard';
        case '浦发银行':
            return 'blueCard';
        case '中信银行':
            return 'pinkCard';
        case '广发银行':
            return 'pinkCard';
        case '平安银行':
            return 'pinkCard';
        case '兴业银行':
            return 'blueCard';
        case '华夏银行':
            return 'pinkCard';
        case '光大银行':
            return 'pinkCard';
        case '邮政储蓄':
            return 'greenCard';
            break;
    }
}

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info: this.props.location.state,
            select: 0,
            id: this.props.location.state.id,
            realName: Cache.realName,
            province: this.props.location.state.province,
            city: this.props.location.state.city,
            subbranch: this.props.location.state.subbranch
        };
    }

    render() {
        return (
            <div className={'changeBankInfo'}>
                <Header title={'修改银行卡'} {...this.props}/>
                <div className={'main'}>
                    <div className={`bankBox ${color(this.state.info.bank)}`}>
                        <div className={'bankImgBox'}>
                            <img src={a(this.state.info.bank)} alt=""/>
                        </div>
                        <div className={`bankInfo`}>
                            <div>{this.state.info.bank}</div>
                            <div className={'bankNum'}>{idMark(this.state.info.cardNumber)}</div>
                        </div>
                    </div>
                    <ul>
                        <li>
                            <span>开户姓名</span>
                            <div>{nameMark(this.state.realName)}</div>
                        </li>
                        <li>
                            <span>开户省份</span>
                            <div onClick={() => {
                                this.setState({
                                    select: 2,
                                    provinceList: provinceData(),
                                    city:'请选择开户城市'
                                })
                            }}>{this.state.province}</div>
                        </li>
                        <li>
                            <span>开户城市</span>
                            <div onClick={() => {
                                if (this.state.province === '请选择开户省份') return AlertFunction({
                                    title: '警告',
                                    msg: '请先选择省份'
                                });
                                this.setState({select: 3, cityList: cityeData(this.state.province)})
                            }}>{this.state.city}</div>
                        </li>
                        <li>
                            <span>开户支行</span>
                            <input type="text" placeholder={'请输入开户支行'} value={this.state.subbranch}
                                   onChange={(e) => this.setState({subbranch: e.target.value})}/>
                        </li>
                    </ul>
                    <div className={'submit'} onClick={() => this.submit()}>确定</div>
                </div>
                {
                    this.state.select === 2 ? (
                        <div className={'select'}>
                            <div className={'selectHeader'}>
                                <div className={'backTag'} onClick={() => this.setState({select: 0})}>
                                    <Svg path={back} className={'arrow'}/>
                                </div>
                                <div className={'title'}>请选择省份</div>
                            </div>
                            <div className={'selectMain'}>
                                <ul>
                                    {
                                        this.state.provinceList.map((item) => {
                                            return (
                                                <li onClick={() => this.setState({
                                                    select: 0,
                                                    province: item.value
                                                })}>{item.value} <Svg path={arrow} className={'arrow'}/></li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    ) : (null)
                }
                {
                    this.state.select === 3 ? (
                        <div className={'select'}>
                            <div className={'selectHeader'}>
                                <div className={'backTag'} onClick={() => this.setState({select: 0})}>
                                    <Svg path={back} className={'arrow'}/>
                                </div>
                                <div className={'title'}>请选择城市</div>
                            </div>
                            <div className={'selectMain'}>
                                <ul>
                                    {
                                        this.state.cityList.map((item) => {
                                            return (
                                                <li onClick={() => this.setState({
                                                    select: 0,
                                                    city: item.value
                                                })}>{item.value} <Svg path={arrow} className={'arrow'}
                                                /></li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                    ) : (null)
                }
            </div>
        )
    }

    //todo 修改银行卡
    submit() {
        if(this.state.city==='请选择开户城市') return AlertFunction({title: '警告', msg: '请选择开户城市'});
        if (this.state.subbranch.length === 0) return AlertFunction({title: '警告', msg: '请输入开户支行信息'});
        if (!/^[\u4e00-\u9fa5]{0,}$/g.test(this.state.subbranch)) return AlertFunction({
            title: '警告',
            msg: '开户支行格式错误，请重新输入'
        });
        Req({
            url: '/api/mine/bankCardUpdate.htm',
            type: 'POST',
            data: {
                action: 'update',
                province: this.state.province,
                city: this.state.city,
                subbranch: this.state.subbranch,
                id:this.state.id
            },
            animate:true
        }).then((data) => {
            AlertFunction({title: '提示', msg: data.errorMsg, confirm: () => window.history.back()});
            Cache.getUserInfo()
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
        })
    }

}