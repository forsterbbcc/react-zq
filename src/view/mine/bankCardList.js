import React, {Component} from 'react';
import {Header} from "../common";
import {Req} from "../../lib";
import {Link} from 'react-router-dom';
import {idMark} from "../../lib/tool";
import {Cache} from "../../module";


//todo 图片的路径
import ICBC from '../../images/bankIcon/icbc.png'
import CMB from '../../images/bankIcon/cmb.png'
import CCB from '../../images/bankIcon/ccb.png'
import ABC from '../../images/bankIcon/abc.png'
import BOC from '../../images/bankIcon/boc.png'
import COMM from '../../images/bankIcon/comm.png'
import CMBC from '../../images/bankIcon/cmbc.png'
import SPDB from '../../images/bankIcon/spdb.png'
import CITIC from '../../images/bankIcon/citic.png'
import GDB from '../../images/bankIcon/gdb.png'
import SZPAB from '../../images/bankIcon/szpab.png'
import CIB from '../../images/bankIcon/cib.png'
import HXB from '../../images/bankIcon/hxb.png'
import CEB from '../../images/bankIcon/ceb.png'
import PSBC from '../../images/bankIcon/psbc.png'
import AlertFunction from "../../lib/AlertFunction";

//todo 根据不同银行显示不同图片
function a(name) {
    switch (name) {
        case '工商银行':
            return ICBC;
        case '招商银行':
            return CMB;
        case '建设银行':
            return CCB;
        case '农业银行':
            return ABC;
        case '中国银行':
            return BOC;
        case '交通银行':
            return COMM;
        case '民生银行':
            return CMBC;
        case '浦发银行':
            return SPDB;
        case '中信银行':
            return CITIC;
        case '广发银行':
            return GDB;
        case '平安银行':
            return SZPAB;
        case '兴业银行':
            return CIB;
        case '华夏银行':
            return HXB;
        case '光大银行':
            return CEB;
        case '邮政储蓄':
            return PSBC;
            break;
    }
}

//todo 根据不同银行显示不同颜色
function color(name) {
    switch (name) {
        case '工商银行':
            return 'redCard';
        case '招商银行':
            return 'redCard';
        case '建设银行':
            return 'blueCard';
        case '农业银行':
            return 'greenCard';
        case '中国银行':
            return 'redCard';
        case '交通银行':
            return 'blueCard';
        case '民生银行':
            return 'greenCard';
        case '浦发银行':
            return 'blueCard';
        case '中信银行':
            return 'pinkCard';
        case '广发银行':
            return 'pinkCard';
        case '平安银行':
            return 'pinkCard';
        case '兴业银行':
            return 'blueCard';
        case '华夏银行':
            return 'pinkCard';
        case '光大银行':
            return 'pinkCard';
        case '邮政储蓄':
            return 'greenCard';
    }
}

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bankList: [],
            bankIcon: ''
        }
    }

    render() {
        return (
            <div className={'bankCardList'}>
                <Header title={'银行卡管理'} bank={'添加'} goTo={'/addBankCard'} {...this.props}/>
                <div className={'main'}>
                    {
                        this.state.bankList.map((item) => {
                            return (
                                <div className={`bankBox  ${color(item.bank)}`}>
                                    <div className={'bankImgBox'}>
                                        <img src={a(item.bank)} alt=""/>
                                    </div>
                                    <div className={`bankInfo`}>
                                        <Link to={{
                                            pathname: '/changeBankInfo',
                                            state: item
                                        }}>
                                            <div className={'nameBox'}>
                                                <div>{item.bank}</div>
                                                <div className={'saveCard'}>储蓄卡</div>
                                            </div>
                                            <div className={'bankNum'}>{idMark(item.cardNumber)}</div>
                                        </Link>
                                        <div className={'setting'}>
                                            {
                                                item.defaultCard === 1 ? (<div className={'none'}/>) : (
                                                    <div onClick={() => this.setDefault(item.id)}>默认</div>
                                                )
                                            }
                                            <div onClick={() => this.deleteCard(item.id)}>删除</div>
                                        </div>
                                    </div>
                                    {
                                        item.defaultCard === 1 ? (
                                            <div className={'mark'}>默认</div>
                                        ) : (null)
                                    }
                                </div>
                            )
                        })
                    }
                    <Link to={{
                        pathname:'/addBankCard',
                        bankList:this.state.bankList
                    }} className={'addNewCard'}>
                        <div><div className={'add'}>+</div></div>
                        <p>添加银行卡</p>
                    </Link>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.getAllCard()
    }

    //todo 获取银行卡信息
    async getAllCard() {
        try {
            let result = await Req({
                url: '/api/mine/bankCard.htm',
                type: 'GET'
            });
            if (result && result.code === 200) {
                this.setState({
                    bankList: result.bankCards
                });
            }
        } catch (e) {
        }
    }

    //todo 设置默认
    setDefault(id) {
        try {
            Req({
                url: '/api/mine/bankCardUpdate.htm',
                type: 'POST',
                data: {
                    action: 'setDefault',
                    id: id
                },
                animate:true
            }).then((data) => {
                AlertFunction({title: '提示', msg: data.errorMsg, confirm: () => this.getAllCard()})
            });
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg})
        }
    }

    //todo 删除银行卡
    deleteCard(id) {
        AlertFunction({
            title: '警告',
            msg: '您确定要删除该银行卡？',
            hasCancel: true,
            confirm: () => {
                Req({
                    url: '/api/mine/bankCardUpdate.htm',
                    type: 'POST',
                    data: {
                        action: 'del',
                        id: id
                    },
                    animate:true
                }).then((data) => {
                    AlertFunction({title: '提示', msg: data.errorMsg, confirm: () => this.getAllCard()});
                    Cache.getUserInfo();
                })
            }
        })
    }
}