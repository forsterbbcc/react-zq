import React, {Component} from 'react';
import Header from '../common/header';
import {Link} from 'react-router-dom';
import {Cache} from "../../module";
import {Schedule,Req} from "../../lib";
import {nameMark, idMark} from "../../lib/tool";
import AlertFunction from "../../lib/AlertFunction";

import warn from '../../images/warn.png'
export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: Cache.isLogin(),
            already: false,
            name: '',
            idNumber: ''
        };

        this.changeName = this.changeName.bind(this);
        this.changeNumber = this.changeNumber.bind(this);
        this.valid = this.valid.bind(this);
    }

    render() {
        return (
            <div className={'realName'}>
                <Header title={'实名认证'} back={'/mine'}/>
                <div className={'main'}>
                    <div className={'warn'}>
                        <img src={warn} alt=""/>
                        <p>请填写您的真实信息,通过后将不能修改</p>
                    </div>
                    <ul>
                        <li>
                            <span>真实姓名</span>
                            <input type="text" value={this.state.name} onChange={this.changeName} maxLength={10}
                                   placeholder={'请输入真实姓名'} readOnly={this.state.already}/>
                        </li>
                        <li>
                            <span>身份证号</span>
                            <input type="tel" value={this.state.idNumber} onChange={this.changeNumber}
                                   placeholder={'请输入身份证号'} readOnly={this.state.already} maxLength={18}/>
                        </li>
                    </ul>
                    {
                        this.state.already ? (<div className={'verification already'}>已验证</div>) : (
                            <div className={'verification'} onClick={this.valid}>验证</div>)
                    }
                    <p>温馨提示：如遇问题，请<Link to={'/cs'}>联系客服</Link></p>
                </div>
            </div>
        )
    }

    componentDidMount() {
        if (Cache.initial) {
            this.loginCallback()
        } else {
            Schedule.dispatchEvent('cacheInitial', this.loginCallback, this)
        }
        Schedule.dispatchEvent('getUserInfo', this.getUserCallback, this)
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this)
    }

    loginCallback() {
        this.setState({
            isLogin: Cache.isLogin()
        });
        this.getUserCallback()
    }

    //todo 更新用户数据信息
    getUserCallback() {
        this.setState({
            name: nameMark(Cache.realName),
            idNumber: idMark(Cache.idNumber),
            already: Cache.realName !== ''
        });
    }

    //todo 绑定用户数据
    async valid() {
        try {
            if(this.state.name.length === 0) return AlertFunction({title:'警告',msg:'请输入真实姓名'});
            if(!/^[\u4e00-\u9fa5]{0,}$/.test(this.state.name)) return AlertFunction({title:'警告',msg:'请输入正确的真实姓名'});
            if(this.state.idNumber.length === 0) return AlertFunction({title:'警告',msg:'请输入身份证号'});
            if(this.state.idNumber.length !== 18) return AlertFunction({title:'警告',msg:'请输入正确身份证号'});
            await Req({
                url: '/api/mine/profileAuth.htm',
                type: "POST",
                data: {
                    action:'authIdentity',
                    name: this.state.name,
                    identityNumber: this.state.idNumber
                },
                animate:true
            });
            AlertFunction({title:'提示',msg:'实名认证成功'});
            window.history.back();
            Cache.getUserInfo();
        } catch (e) {
            AlertFunction({title:'错误',msg:e.errorMsg});
        }
    }

    changeName(e) {
        let value = e.target.value.toString();
        this.setState({name: value})
    }

    changeNumber(e) {
        let value = e.target.value;
        this.setState({idNumber: value})
    }

}