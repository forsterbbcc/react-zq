import React, {Component} from 'react';
import {Header} from "../common";
import {Link} from 'react-router-dom';
import {Req} from "../../lib";


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            commRatio:'',
            levName:'',
            userCount:'',
            userConsume:'',
            visitCount:'',
            linkAddress:'',
            successful: false
        }
    }

    render() {
        return (
            <div className={'promotionBox'}>
                <Header title={'我的经纪人'} {...this.props}/>
                <div className={'main'}>
                    <div className={'top'}>
                        <ul>
                            <li>佣金比例</li>
                            <li>{this.state.commRatio}</li>
                            <li className={'lev'}>{this.state.levName}</li>
                        </ul>
                        <ul>
                            <li>注册</li>
                            <li>{this.state.userCount}</li>
                            <li>交易{this.state.userConsume}人</li>
                        </ul>
                    </div>
                    {/*<div className={'middle'}>*/}
                        {/*<div className={'p'}> <ReactSvg path={searchIcon} className={'searchIcon'} wrapperClassName={'out'}/><span>如何赚钱?</span></div>*/}
                        {/*<img src={process} alt="" className={'process'}/>*/}
                        {/*<div className={'link'}>*/}
                            {/*<img src={data}  className={"data"}/>*/}
                            {/*<div className={'url'}>*/}
                                {/*<div className={'url-left'}>*/}
                                    {/*<h4>复制推广链接发送给朋友<span>(以访问{this.state.visitCount}次)</span></h4>*/}
                                    {/*<h5>{this.state.linkAddress}</h5>*/}
                                {/*</div>*/}
                                {/*<div className={'url-right'}>*/}
                                    {/*<CopyToClipboard  text={this.state.linkAddress} onCopy={()=>this.onCopy()}>*/}
                                        {/*<div>复制</div>*/}
                                    {/*</CopyToClipboard>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                    <div className={'foot'}>
                        <table>
                            <tbody>
                            <tr>
                                <th>级别</th>
                                <th>下线交易<br/>用户数量</th>
                                <th>交易提成<br/>佣金比例</th>
                                <th>下线<br/>有效期</th>
                            </tr>
                            <tr>
                                <td>普通经纪</td>
                                <td> >=1人 </td>
                                <td> 5% </td>
                                <td> 6个月 </td>
                            </tr>
                            <tr>
                                <td>铜牌经纪</td>
                                <td rowSpan={4} colSpan={2} style={{verticalAlign:'middle'}}>
                                    高达50%的返佣比例<br/>

                                    请联系经纪客服QQ专线<br/>

                                    800837618
                                </td>
                                <td rowSpan={4} style={{verticalAlign:'middle'}}>
                                    永久
                                </td>
                            </tr>
                            <tr>
                                <td>银牌经纪</td>
                            </tr>
                            <tr>
                                <td>金牌经纪</td>
                            </tr>
                            <tr>
                                <td>钻石经纪</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <Link className={'share'} to={'/recommendFriend'}>我要推广</Link>
                </div>
                {
                    this.state.successful?(
                        <div className={'success'}>
                            <div>复制成功，快去推广吧！</div>
                        </div>
                    ):(null)
                }
            </div>
        )
    }

    componentDidMount(){
        this.getPromotionInfo()
    }

    async getPromotionInfo(){
        try{
            let result = await Req({
                url:'/api/mine/union.htm',
                type:'GET'
            });
            let info = result.union;
            this.setState({
                commRatio:info.commRatio * 100,
                userCount:info.userCount,
                userConsume : info.userConsume,
                visitCount: info.visitCount,
                linkAddress:`${window.location.origin}/?ru=${info.userId}`
            });

            if(this.state.userConsume >= 50) {
                this.setState({levName:'钻石经纪'});
            }else if(this.state.userConsume >= 40) {
                this.setState({levName:'金牌经纪'});
            }else if(this.state.userConsume >= 30 || this.state.userConsume >=10){
                this.setState({levName:'银牌经纪'});
            }else {
                this.setState({levName:'普通经纪'});
            }
        }catch(e){}
    }

    //todo 复制
    // onCopy(){
    //     this.setState({successful:true});
    //
    //     setTimeout(()=>{
    //         this.setState({successful:false})
    //     },1500)
    // }
}