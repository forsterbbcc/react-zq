import React, {Component} from 'react';
import {Header} from "../common";
import {Req} from "../../lib";

import empty from '../../images/empty.png'
import {formatDate} from "../../lib/tool";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: null
        }
        this.getUser()
    }

    render() {
        return (
            <div className={'myUser'}>
                <Header title={'我的用户'} {...this.props}/>
                <div className={'main'}>
                    {
                        this.state.users === null ? '' : (
                            this.state.users.length === 0 ? (
                                <div className={'empty'}>
                                    <img src={empty} alt=""/>
                                    <p>您还没有用户，快去推广吧～</p>
                                </div>
                            ) : (
                                <table>
                                    <tbody>
                                    <tr>
                                        <th>用户</th>
                                        <th>当天交易</th>
                                        <th>历史交易</th>
                                        <th>注册时间</th>
                                    </tr>
                                    {
                                        this.state.users.map((item) => {
                                            return (
                                                <tr>
                                                    <td>{item.username}</td>
                                                    <td>{item.dayTradeVolume}</td>
                                                    <td>{item.tradeVolume}</td>
                                                    <td>{formatDate('y-m-d', {date: item.loginTime.time})}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </table>
                            )
                        )
                    }
                </div>
            </div>
        )
    }


    getUser() {
        Req({
            url: '/api/mine/unionUser.htm',
            type: 'GET',
            animate: true
        }).then((data) => {
            this.setState({
                users: data.users
            });
            console.log(data)
        })
    }
}