import React, {Component} from 'react';
import {Header} from "../common";
import {Cache} from "../../module";

export default class live800 extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const infos = window.encodeURIComponent(
            "userId="+Cache.userId+
            "&name="+ Cache.username+
            "&memo="+Cache.realName
        );
        const csSrc = "https://ytpfx.livechatvalue.com/chat/chatClient/chatbox.jsp?companyID=1006450&configID=59169&jid=8704485092&info=" + infos;
        return (
            <div className={'myUser'}>
                <Header title={'在线客服'} {...this.props}/>
                <div className={'main'}>
                    <iframe id="csIframe" name="csIframe"
                            src={csSrc}
                            width="100%"
                            height={this.getHeight().height}
                            frameBorder="0"
                            allowTransparency="true"
                            scrolling="no"
                            allowFullScreen
                            style={{display: "block",  }}>
                    </iframe>
                </div>
            </div>
        )
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    getHeight = () => {
        if (document.compatMode === "BackCompat") {   //浏览器嗅探，混杂模式
            return {
                width: document.body.clientWidth,
                height: document.body.clientHeight-50,
            }
        } else {
            return {
                width:document.documentElement.clientWidth,
                height:document.documentElement.clientHeight-50,
            }
        }
    }

}