import React, {Component} from 'react';
import {Header} from "../common";
import {Svg} from "../common";
import {Req} from "../../lib";
import AlertFunction from '../../lib/AlertFunction'

//todo 引入图片
import close from '../../images/svg/wrong.svg'
import warn from '../../images/warn.png'

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oldPass: '',
            newPass: '',
            comPass: ''
        };
    }

    render() {
        return (
            <div className={'changePassword'}>
                <Header title={'登录密码'} {...this.props}/>
                <div className={'main'}>
                    <div className={'warn'}>
                        <img src={warn} alt=""/>
                        <p>更改密码完成后，请牢记您的新密码</p>
                    </div>
                    <ul>
                        <li>
                            <span>旧密码</span>
                            <input type="password" placeholder={'请输入旧密码'} value={this.state.oldPass}
                                   onChange={(e) => this.setState({oldPass: e.target.value.replace(/[^\a-\z\A-\Z0-9]/g,'')})} maxLength={16}/>
                            {
                                this.state.oldPass.length !== 0 ? (
                                    <div className={'closeBox'} onClick={() => this.setState({oldPass: ''})}>
                                        <Svg path={close} className={'close'} state={'small'}/>
                                    </div>
                                ) : (null)
                            }
                        </li>
                        <li>
                            <span>新密码</span>
                            <input type="password" placeholder={'请输入新密码'} value={this.state.newPass}
                                   onChange={(e) => this.setState({newPass: e.target.value.replace(/[^\a-\z\A-\Z0-9]/g,'')})} maxLength={16}/>
                            {
                                this.state.newPass.length !== 0 ? (
                                    <div className={'closeBox'} onClick={() => this.setState({newPass: ''})}>
                                        <Svg path={close} className={'close'}/>
                                    </div>
                                ) : (null)
                            }
                        </li>
                        <li>
                            <span>确认密码</span>
                            <input type="password" placeholder={'请再次输入新密码'} value={this.state.comPass}
                                   onChange={(e) => this.setState({comPass: e.target.value.replace(/[^\a-\z\A-\Z0-9]/g,'')})} maxLength={16}/>
                            {
                                this.state.comPass.length !== 0 ? (
                                    <div className={'closeBox'} onClick={() => this.setState({comPass: ''})}>
                                        <Svg path={close} className={'close'}/>
                                    </div>
                                ) : (null)
                            }
                        </li>
                    </ul>
                    <div className={'buttonBox'}>
                        <p className={'tips'}>6-16位数字、字母组合（特殊字符除外）</p>
                        <div className={'submit'} onClick={() => this.submit()}>确认</div>
                    </div>
                </div>
            </div>
        )
    }

    //todo 提交
    async submit() {
        try {
            if(this.state.oldPass === '')return AlertFunction({title:'警告',msg:'请输入旧密码'});
            if(this.state.newPass === '')return AlertFunction({title:'警告',msg:'请输入新密码'});
            if(this.state.comPass === '')return AlertFunction({title:'警告',msg:'请输入确认密码'});
            if(this.state.newPass !== this.state.comPass)return AlertFunction({title:'警告',msg:'两次输入密码不相同'});

            let result = await Req({
                url: '/api/mine/loginPasswd.htm',
                type: 'POST',
                data: {
                    oldPass: this.state.oldPass,
                    newPass: this.state.newPass,
                    newPassCfm: this.state.comPass
                },
                animate:true
            });
            AlertFunction({
                title: '提示', msg: result.errorMsg, button: 1, confirm: () => {
                    window.history.back()
                }
            });
        } catch (err) {
            AlertFunction({
                title: '错误', msg: err.errorMsg, button: 1, confirm: () => {
                    this.setState({oldPass: '', newPass: '', comPass: ''})
                }
            })
        }
    }

}