import React, {Component} from 'react';
import Header from '../common/header';
import {Link} from 'react-router-dom';
import {Req} from "../../lib";
import {Contracts,Cache} from "../../module";
import AlertFunction from "../../lib/AlertFunction";
import {Schedule} from "../../lib";

//todo 图片banner


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            code: '',
            showImg: false,
            count: '获取验证码',
            countNumber: 60,
            imageAddress: '',
            imageCode: '',
            next: false,
            nikeName: '',
            password: '',
            goodcode: null,
            name: null,
            id: null,
        }
        if (Contracts.initial) {
            let [o] = Contracts.foreignArray;
            this.state.goodcode = o.contract;
            this.state.name = o.name;
            this.state.id = o.code
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }
    updateContracts() {
        let [o] = Contracts.foreignArray;
        this.setState({
            goodcode: o.contract,
            name: o.name,
            id:o.code
        })
    }
    render() {
        let [o] = Contracts.foreignArray;
        console.log(Contracts.foreignArray,[o],this.state.goodcode)
        return (
            <div className={'register'}>
                <Header {...this.props}/>
                <div className={'main'}>
                    {
                        this.state.next ? (
                            <div className={'infoBox'}>
                                <div>
                                    <h4></h4>
                                    <div className={'inputBox'}>
                                        <input type="text" placeholder={'取个吉利的名字吧'}
                                               value={this.state.nikeName}
                                               onChange={(e) => this.setState({nikeName: e.target.value})}
                                               maxLength={8}/>
                                    </div>
                                </div>
                                <div>
                                    <div className={'inputBox'}>
                                        <input
                                                type="password" placeholder={'请输入登录密码'}
                                               value={this.state.password}
                                               onChange={(e) => this.setState({password: e.target.value})}
                                               maxLength={16}/>
                                    </div>
                                </div>
                                <div className={'buttonBox'}>
                                    <div className={'submit'} onClick={() => this.submit()}>注册</div>
                                </div>
                            </div>
                        ) : (
                            <div className={'infoBox'}>
                                <h4></h4>
                                <div>
                                    <div className={'inputBox'}>
                                        <input type="tel" placeholder={'请输入您的手机号码'}
                                               value={this.state.username}
                                               onChange={(e) => this.setState({username: e.target.value})}
                                               maxLength={11}/>
                                    </div>
                                </div>
                                <div>
                                    <div className={'inputBox'}>
                                        <input type="number" placeholder={'请输入验证码'} maxLength={4}
                                               value={this.state.code}
                                               onChange={(e) => this.setState({code: e.target.value})}/>
                                        <div className={'getCode'} onClick={() => this.show()}>{this.state.count}</div>
                                    </div>
                                    <div className={'buttonBox'}>
                                        <div className={'submit'} onClick={() => this.send()}>注册</div>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    {/*<div className={'none'}>*/}
                        {/*/!*<Link to={{pathname: '/login'}} className={'registerButton button'}>用户登陆</Link>*!/*/}
                        {/*/!*<Link to={{pathname: '/customerService'}} className={'registerButton button'}>在线客服</Link>*!/*/}
                    {/*</div>*/}
                </div>
                {
                    this.state.showImg ? (
                        <div className={'cover'}>
                            <div className={'codeBox'}>
                                <p>获取验证码前,请您先填写下面数字</p>
                                <div className={'code'}>
                                    <input type="text" placeholder={'请填写图片验证码'} maxLength={4}
                                           value={this.state.imageCode}
                                           onChange={(e) => this.setState({imageCode: e.target.value.replace(/[^\d]/g,'')})}/>
                                    <img src={this.state.imageAddress} alt=""/>
                                </div>
                                <div className={'codeButton'}>
                                    <div onClick={() => {
                                        this.setState({showImg: false})
                                    }}>取消
                                    </div>
                                    <div className={'sure'} onClick={() => this.confirmCode()}>确定</div>
                                </div>
                            </div>
                        </div>
                    ) : (null)
                }
            </div>
        )
    }

    //todo 显示图片验证码
    show() {
        if (this.state.username.length === 0) return AlertFunction({title: '警告', msg: '请先输入手机号码！'})
        if (this.state.countNumber !== 0 && this.state.count !== '获取验证码') {
            return AlertFunction({title: '警告', msg: '验证码已经发送，请稍后再试！'})
        }

        this.setState({
            showImg: true,
            imageAddress: `/api/vf/verifyCode.jpg?_=${new Date()}`,
            imageCode:''
        });

    }

    //todo 确定图片验证码
    async confirmCode() {
        if (this.state.imageCode.length !== 4) {
            AlertFunction({title: '警告', msg: '请输入正确的验证码', button: 1,
                confirm: () => {
                this.show()
            }
            })
        } else {
            try {
                let result = await Req({
                    url: '/api/sso/register.htm',
                    type: 'POST',
                    data: {
                        action: 'sendCode',
                        mobile: this.state.username,
                        imageCode: this.state.imageCode
                    }
                });

                AlertFunction({
                    title: '提示',
                    msg: result.errorMsg,
                    confirm: () => this.setState({showImg: false})
                });
                this.countDown()

            } catch (err) {
                if(err.errorMsg.match('手机')){
                    AlertFunction({title: '错误', msg: err.errorMsg, button: 1,
                        confirm: () => {
                            this.show();
                            this.setState({
                                showImg:false,
                                username:''
                            })
                        }})
                }else{
                    AlertFunction({title: '错误', msg: err.errorMsg, confirm: () => {
                            this.show()
                    }})
                }
            }
        }
    }

    //todo 倒计时
    countDown() {
        if (this.state.countNumber === 0) {
            this.setState({
                count: '获取验证码',
                countNumber: 60
            })
        } else {
            setTimeout(() => {
                this.setState({
                    countNumber: this.state.countNumber - 1,
                    count: `${this.state.countNumber}秒后重发`,
                });
                this.countDown()
            }, 1000)

        }
    }

    //todo 下一步
    next() {
        if (this.state.username.length === 0 || this.state.username.length !== 11) {
            return AlertFunction({title: '警告', msg: '手机号码有误，请重新输入！'})
        } else {
            this.setState({phoneCode: false})
        }
    }

    //todo 提交验证码
    send() {
        Req({
            url: '/api/sso/register.htm',
            type: 'POST',
            data: {
                action: 'verifyCode',
                verifyCode: this.state.code
            }
        }).then((data) => {
            this.setState({next: true})
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg})
            })
    }

    //todo 注册
    submit() {
        Req({
            url: '/api/sso/register.htm',
            type: 'POST',
            data: {
                action: 'register',
                username: this.state.nikeName,
                password: this.state.password
            }
        }).then((data) => {
            AlertFunction({title: '提示',yes: '前往模拟交易', msg: '恭喜您,注册成功!', confirm: () => {
                this.props.history.push({ pathname: "/trade", 
                state: {simulate: true,
                    id: this.state.id,
                    name:this.state.name,
                    contract: this.state.goodcode,
                    code:this.state.goodcode} })
                }
            })
            Cache.status = true;
            localStorage.setItem('isLogin', true);
            localStorage.setItem('mobile', this.state.username);
            Cache.getUserInfo();
            Cache.getUserScheme();
        }).catch((err) => {
            AlertFunction({title: '错误', msg: err.errorMsg});
        })
    }
}