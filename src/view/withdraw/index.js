import React ,{Component} from 'react'
import {NavLink} from 'react-router-dom'
import RouteWithSubRoutes from './../../routes/routeWithSubRoutes'
import Header from './../../view/common/header';
import AlertFunction from "../../lib/AlertFunction";

export default class App extends Component {
    constructor(props){
        super(props)
    }

    render(){
        const a = this.props.routes;
        return(
            <div className='withdraw'>
                <Header title={"提款"} {...this.props} back={'/mine'} right={'提款记录'} goTo={'/history'}/>
                <ul>
                    <NavLink to={'/withdraw/card'} activeClassName={'selected'}>银行卡</NavLink>
                    {/*<NavLink to={'/withdraw/alipay'} activeClassName={'selected'}>支付宝</NavLink>*/}
                    <a onClick={()=>AlertFunction({title:'提示',msg:'该渠道维护中，请使用银行卡提款！'})}>支付宝</a>
                </ul>
                {a.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
            </div>
        );
    }
}