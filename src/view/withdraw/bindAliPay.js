import React,{Component} from 'react'
import Header from './../common/header'
import {encode} from './../../lib/qrcode'
import {getObjectURL} from './../../lib/tool'
import AlertFunction from '../../lib/AlertFunction'
import {Req} from './../../lib/index'

export default class App extends Component{
    constructor(props){
        super(props);

        this.state = {
            content:null,
            aliQR:null,
            accountNumber:null,
            id:null
        }
    }

    componentDidMount(){
        this.updateQR()
    }

    async updateQR(){
        try {
            let result = await Req({
                url:'/api/user/alipayOper.htm',
                data:{
                    action:'info',
                },
                animate:true
            });

            let accountNumber = (result.Accounts && result.Accounts[0] && result.Accounts[0].account)||null;
            let id = (result.Accounts && result.Accounts[0] && result.Accounts[0].id)||null;
            if (accountNumber && id){
                this.setState({
                    accountNumber:accountNumber,
                    id:id,

                });
            }
        }catch (e) {
            console.log(e);
        }
    }

    renderQR(){
        if (this.state.aliQR){
            return (
                <img src={`/api/vf/tdCode.gif?type=tj&url=${this.state.aliQR}`}/>
            );
        } else{
            return(
                <div>{`点击上传收钱码`}
                    <input type={'file'} accept={'image/*'} onChange={this.onChangeContent.bind(this)}/>
                </div>
            );
        }
    }

    render(){
        return(
            <div className={'bindAliPay'}>
                <Header title={'绑定支付宝'} {...this.props}/>
                <div className={'bindAliPay-content'}>
                    {this.renderQR()}
                    <h6>请上传你的收钱码</h6>
                    <form>
                        <label>支付宝账号</label>
                        <input type="text" placeholder={'填写付款支付宝账号'} value={this.state.accountNumber} onChange={this.onChangeAccountText.bind(this)}/>
                    </form>
                    <div onClick={this.submit.bind(this)} className={'submitButton'}>立即提现</div>
                </div>
            </div>
        );
    }

    onChangeContent(e){
        try {
            let url = e.currentTarget.files[0];
            console.log(url);

            url = getObjectURL(url);
            let func = encode();
            func.decode(url);
            func.callback = (imgMsg)=>{
                if (imgMsg.indexOf('QR.ALIPAY') === -1) {
                    //showAlert({msg:'上传的图片不是有效的收钱码'})
                    return AlertFunction({title:'提示', msg:'上传的图片不是有效的收钱码'});
                } else {
                    // self.link = imgMsg;
                    // self.alipayQR = imgMsg;
                    console.log(imgMsg);
                    this.setState({
                        aliQR:imgMsg
                    });
                }
            }
        }catch (e) {
            return AlertFunction({title:'提示', msg:'上传的图片不是有效的收钱码'});
        }
    }

    onChangeAccountText(e){
        let text = e.target.value;
        this.setState({
            accountNumber:text
        });
    }

    submit(){

        if(this.state.accountNumber === ''){
            return AlertFunction({title:'提示', msg:'请输入正确的支付宝账号'});
        }

        if(this.state.aliQR === null || this.state.aliQR.indexOf('QR.ALIPAY') === -1){
            return AlertFunction({title:'提示', msg:'请上传正确的支付宝收钱码'});
        }

        if (this.state.id){
            //更新
            (
                async ()=>{
                    try {
                        let result = await Req({
                            url:'api/user/alipayOper.htm',
                            data:{
                                action:'update',
                                account:this.state.accountNumber,
                                qrcodeUrl:this.state.aliQR,
                                defaultFlag:true,
                                id:this.state.id
                            }
                        });
                        console.log(result);
                        AlertFunction({title:'提示', msg:result.resultMsg ||result.errorMsg,confirm(){
                            if (result && result.success){
                                window.history.back()
                            }
                            }});
                    }catch (e) {
                        console.log(e);
                }
                }
            )();
        } else{
            //新创
            (
                async ()=>{
                    try {
                        let result = await Req({
                            url:'api/user/alipayOper.htm',
                            data:{
                                action:'binding',
                                account:this.state.accountNumber,
                                qrcodeUrl:this.state.aliQR
                            },
                            animate:true
                        });
                        console.log(result);
                        AlertFunction({title:'提示', msg:result.resultMsg || result.errorMsg,confirm(){
                                if (result && result.success){
                                    window.history.back()
                                }
                            }});
                    }catch (e) {
                        console.log(e);
                    }
                }
            )();
        }
    }
}

