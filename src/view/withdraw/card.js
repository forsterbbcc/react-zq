import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Req} from './../../lib/index';
import AlertFunction from '../../lib/AlertFunction';
import {Redirect} from 'react-router-dom';
import {Cs} from "../common";

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            bankCards: [],
            asset: null,
            showBankCardSelection: 'hideSelection',
            selectedCard: null,
            goBindBankCard: false,
            goIdentityAuth: false,
            password: '',
            money:''
        }
    }

    componentDidMount() {
        this.update().catch()
    }

    async update() {
        try {
            let result = await Req({
                url: '/api/pay/withdraw.htm'
            });

            if (result.identityAuth === false) {
                return AlertFunction({title:'提示', msg:'您当前还未实名认证，为保障您的账户安全，请先实名认证',hasCancel:true,confirm:()=>{this.setState({goIdentityAuth:true})},cancel(){window.history.back()}})
            }

            if (result.bankCards && result.bankCards.length === 0) {
                return AlertFunction({title:'提示', msg:'您当前还未添加提款银行卡，请先添加提款银行卡',hasCancel:true,confirm:()=>{this.setState({goBindBankCard:true})},cancel(){window.history.back()}})
            }

            this.setState({
                user: result.user,
                bankCards: result.bankCards,
                asset: result.asset,
                selectedCard: result.bankCards && result.bankCards.length !== 0 ? result.bankCards[0] : null,
            });
        } catch (e) {}
    }

    async withdrawMoney() {
        try {
            let result = await Req({
                url: '/api/pay/withdraw.htm',
                type: 'POST',
                data: {
                    type: 0,
                    action: 'apply',
                    money: this.state.money,
                    bankCard: this.state.selectedCard.id,
                    password: this.state.password
                },
                animate: true
            });

            AlertFunction({
                title: '提示', msg: result.resultMsg || result.errorMsg, confirm() {
                    if (result && result.success) {window.history.back()}
                }
            });

        } catch (e) {
            AlertFunction({title:'错误',msg:e.resultMsg||e.errorMsg||'未知错误'})
        }
    }

    renderBankSelectionItem() {
        return this.state.bankCards.map((item, i) => {
            return (
                <li key={i}
                    onClick={() => this.selectBankCard(i)}>{this.getBankNameFrom(i) + ' ' + this.getCardNumberFrom(i)}</li>
            );
        });
    }

    render() {

        if (this.state.goIdentityAuth) {
            return (
                <Redirect to={'/realName'}/>
            );
        }

        if (this.state.goBindBankCard) {
            return (
                <Redirect to={'/bankCardList'}/>
            );
        }

        let bankName = (this.state.selectedCard && this.state.selectedCard.bank) || '';
        let accountNumber = (this.state.selectedCard && this.state.selectedCard.card) || '';
        let userCredit = this.getMoney();

        return (
            <div className={'withdraw-card'}>
                <div>
                    <form>
                        <ul>
                            <li>
                                <label>提款银行卡</label>
                                <input value={bankName + ' ' + accountNumber} readOnly={'readonly'}
                                       onClick={() => this.showBankCardSelection()}/>
                            </li>
                            <li>
                                <label>提款金额</label>
                                <input className={'fixError'} type={'number'} placeholder={'请输入金额'} value={this.state.money}
                                       onChange={this.onChangeMoney.bind(this)}/>
                            </li>
                            <li>
                                <label>账户余额</label>
                                <input type={'text'} readOnly={'readonly'}
                                       value={userCredit !== '' ? userCredit.toFixed(2) : ''}/>
                            </li>
                            <li>
                                <label>提款密码</label>
                                <input type={'password'} placeholder={'请输入密码'} value={this.state.password}
                                       onChange={(e)=>this.setState({password:e.target.value})}/>
                                <span className="recover-pw-withdraw-box">
                                    <Link to={{pathname: '/recoverWithdrawPassword'}} className="recover-pw">忘记密码?</Link>
                                </span>
                            </li>
                        </ul>
                    </form>
                    {
                        this.checkTimeAvailable()?(
                            <div onClick={() => this.submit()} className={'submitButton'}>立即提款</div>
                        ):(
                            <div className={'submitButton'} style={{'background':'#999999'}}>立即提款</div>
                        )
                    }

                </div>
                <ul>
                    <p>温馨提示：</p>
                    <li>* 单笔最低提款100元，最高无上限，每日限提2次</li>
                    <li>* 提款处理时间：周一至周五09:00-23:00, 到账时间：以实际银行为准</li>
                    <li>* 如充值后未交易提款，则需收取1.5%手续费</li>
                    <li>* 如需帮助请 <Link to={'/cs'}>联系客服</Link></li>
                </ul>
                <div className={this.state.showBankCardSelection}>
                    <ul>
                        {this.renderBankSelectionItem()}
                    </ul>
                </div>
            </div>
        );
    }
    checkTimeAvailable() {
        let start = new Date();
        let end = new Date();
        let now = new Date();
        //每日提现时间 09:00-23:00
        start.setHours(9);
        start.setMinutes(0);
        end.setHours(23);
        end.setMinutes(0);
        return now.getTime() - start.getTime() > 0 && now.getTime() - end.getTime() < 0;
    }


    selectBankCard(index) {
        console.log('selectBankCard');
        let element = this.state.bankCards[index];
        this.setState({
            selectedCard: element
        }, () => this.hideBankCardSelection());
    }

    showBankCardSelection() {
        this.setState({
            showBankCardSelection: 'showSelection'
        });
    }

    hideBankCardSelection() {
        this.setState({
            showBankCardSelection: 'hideSelection'
        });
    }

    submit() {
        if (this.state.money < 100) {
            return AlertFunction({
                title: '提示',
                msg: "最低提款金额为100元"
            });
        }
        // if (this.state.money > 10000) {
        //     return AlertFunction({
        //         title: '提示',
        //         msg: " 单次提款最高金额为10000元"
        //     });
        // }
        this.withdrawMoney()
    }

    onChangeMoney(e) {
        let value = e.target.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
        this.setState({
            money: value
        });
        if(value > this.getMoney()){
            this.setState({
                money: this.getMoney()
            });
        }
    }


    getBankNameFrom(index) {
        return (this.state.bankCards.length !== 0 && this.state.bankCards[index].bank) || '';
    }

    getCardNumberFrom(index) {
        return (this.state.bankCards.length !== 0 && this.state.bankCards[index].card) || '';
    }

    getMoney() {
        return (this.state.asset && this.state.asset.money && this.state.asset.money) || 0;
    }
}