import React,{Component} from 'react'
import Header from './../common/header'
import {Req} from './../../lib/index'
import {formatDate} from './../../lib/tool'

export default class App extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            items:[]
        }
    }

    componentDidMount(){
        this.update();
    }

    async update(){
        try {
            let result = await Req({
                url:'/api/pay/withdrawHistory.htm',
                animate:true
            });

            this.setState({
                items:result.inouts
            });
        }catch (e) {
            console.log(e);
        }
    }

    statusText(status) {
        switch (status) {
            case 0:
                return '未处理';
            case 1:
                return '处理成功';
            case 2:
                return '处理失败';
            case 3:
                return '已取消';
            case 4:
                return '处理中';
            case 5:
                return '汇款中';
        }
    }

    renderItem(){
        return this.state.items.map((item,i)=>{

            let date = new Date(item.disposeTime && item.disposeTime.time);
            let dateString = formatDate('y-m-d h:i:s',{date})

            if (item.bankCard === null) {
                return(
                    <span></span>
                );
            }

            let bankName;
            let lastFour;
            if (!(item.bankCard.indexOf('QR.ALIPAY') === -1)) {
                bankName = '支付宝';
                let bankCardArray = item.bankCard.split(',');
                lastFour = bankCardArray[0].substring(bankCardArray[0].length-4)+')'
            }else{
                let bankCardArray = item.bankCard.split('(');
                bankName = bankCardArray[0];
                lastFour = (bankCardArray[1].substring(bankCardArray[1].length - 5));
            }

            return(
                <li key={i}>
                    <div>
                        <div>{bankName + ' (尾号' + lastFour}</div>
                        <div>-{item.money.toFixed(2)}</div>
                    </div>
                    <div>
                        <div>{this.statusText(item.status)}</div>
                        <div>{dateString}</div>
                    </div>
                </li>
            );
        });
    }

    render(){
        return(
            <div className={'history'}>
                <Header title={'提款历史'} {...this.props}/>
                <ul>
                    {this.state.items.length !== 0?this.renderItem():(<div className={'placeholder'}>暂无记录</div>)}
                </ul>
            </div>
        );
    }
}