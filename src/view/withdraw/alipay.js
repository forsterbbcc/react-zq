import React ,{Component} from 'react'
import {Req} from '../../lib/index'
import {Link} from 'react-router-dom'
import AlertFunction from '../../lib/AlertFunction'

export default class App extends Component {
    constructor(props){
        super(props)

        this.state = {
            money:0,
            qrURL:null,
            showUploadQR:false,
            account:null,
            password:'',
            withdrawMoney:''
        }
    }

    componentDidMount(){
        this.updateQR();
        this.updateMoney();
    }

    async updateQR(){
        try {
            let result = await Req({
                url:'/api/user/alipayOper.htm',
                data:{
                    action:'info',
                }
            });
            let qrURL = result.Accounts && result.Accounts[0] && result.Accounts[0].qrcodeUrl;
            let accountNumber = (result.Accounts && result.Accounts[0] && result.Accounts[0].account)||null;
            let id = (result.Accounts && result.Accounts[0] && result.Accounts[0].id)||null;
            if (qrURL){
                this.setState({
                    qrURL:qrURL,
                    accountNumber:accountNumber,
                    id:id
                });
            } else{
                this.setState({
                    showUploadQR:true
                });
            }
        }catch (e) {
            console.log(e);
        }
    }

    async submitWithdraw(){
        try {
            let result = await Req({
                url:'/api/pay/withdraw.htm',
                data:{
                    type:1,
                    action:'apply',
                    money:this.state.money,
                    bankCard:this.state.id,
                    password:this.state.password
                },
                type:'POST',
                animate:true
            });

            AlertFunction({title:'提示', msg:result.resultMsg ||result.errorMsg,confirm(){
                    if (result && result.success){
                        window.history.back()
                    }
                }});
        }catch (e) {
            console.log(e);
        }
    }

    async updateMoney(){
        try {
            let result = await Req({
                url:'/api/pay/withdraw.htm'
            })

            let money = result.asset && result.asset.money

            this.setState({
                money:money
            });

            console.log(result);
        }catch (e) {

        }
    }

    renderQRImage(){
        if (this.state.qrURL) {
            return(
                <img src={`/api/vf/tdCode.gif?type=tj&url=${this.state.qrURL}`}/>
            );
        }
        return(<div></div>);
    }

    render(){

        if (this.state.showUploadQR){
            return(
                <div className={'withdraw-bindalipay'}>
                    <label>请前往</label>
                    <Link to={{pathname:'/bindAlipay'}}>绑定支付宝收款码</Link>
                </div>
            );
        }

        return(
            <div className={'withdraw-alipay'}>
               <div className={'qrWrapper'}>
                    {/*<img src={`/api/vf/tdCode.gif?type=tj&url=${this.state.qrURL}`}/>*/}
                   {this.renderQRImage()}
               </div>
                <form>
                    <ul>
                        <li>
                            <label>提款金额</label>
                            <input type={'tel'} placeholder={'请输入金额'} value={this.state.withdrawMoney} onChange={this.onChangeWithdrawMoney.bind(this)}/>
                        </li>
                        <li>
                            <label>账户余额</label>
                            <input readOnly={'readonly'} value={this.state.money.toFixed(2)}/>
                        </li>
                        <li>
                            <label>提款密码</label>
                            <input type={'password'} placeholder={'请输入提款密码'} value={this.state.password} onChange={this.onChangePassword.bind(this)}/>
                        </li>
                    </ul>
                    <div className={'submitButton'} onClick={this.submit.bind(this)}>立即提现</div>
                </form>
                <ul>
                    <p>温馨提示：</p>
                    <li>* 绑定支付宝账户即可提款</li>
                    <li>* 单笔最低提款100元，最高10000元，每日限提2次</li>
                    <li>* 提款处理时间：周一至周五07:00-23:00, 到账时间：以实际银行为准</li>
                    <li>* 如充值后未交易提款，则需收取1.5%手续费</li>
                </ul>
            </div>
        );
    }

    onChangePassword(e){
        let pw = e.target.value;
        this.setState({
            password:pw
        });
    }

    onChangeWithdrawMoney(e){
        let pw = e.target.value;
        this.setState({
            withdrawMoney:pw
        });
    }

    submit(){

        if (!this.checkTimeAvailable()) {
            return AlertFunction({title:'提示', msg:'提款时间：周一至周五7:00-23:00'});
        }


        // if (this.state.withdrawMoney < 100) {
        //     return AlertFunction({title:'提示', msg:'最低提款金额为100元'});
        // }
        //
        // if (this.state.withdrawMoney > 10000) {
        //     return AlertFunction({title:'提示', msg:'单次提款最高金额为10000元'});
        // }

        this.submitWithdraw();
    }

    checkTimeAvailable() {
        var start = new Date();
        var end = new Date();
        var now = new Date();
        //每日提现时间 07:00-22:59
        start.setHours(7);
        start.setMinutes(0);
        end.setHours(22);
        end.setMinutes(59);
        if (now.getTime() - start.getTime() > 0 && now.getTime() - end.getTime() < 0) {
            return true;
        }
        return false;
    }
}