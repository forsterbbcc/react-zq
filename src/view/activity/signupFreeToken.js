import React ,{Component} from 'react'
import {Contracts, Cache} from "../../module";
import {Schedule} from "../../lib";

import signupFreetoken from '../../images/simulateM.png'
export default class App extends Component{

    constructor(props){
        super(props);
        this.state={
            contract: null
        }
        if (Contracts.initial) {
            let [o] = Contracts.foreignArray;
            this.state.contract = o.contract;
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }

    render(){
        return(
            <div className="signupFreetoken" onClick={()=>this.go()}>
                <img src={signupFreetoken}/>
            </div>
        );
    }
    go(){
        if(Cache.isLogin()){
            this.props.history.push({
                pathname:'/trade',
                state:{
                    simulate: true,
                    contract: this.state.contract
                }
            })
        }else{
            this.props.history.push({
                pathname:'/LR',
                state:{
                    which:1
                },
                where:this.props.location.where
            })
        }
    }
    updateContracts() {
        let [o] = Contracts.foreignArray;
        this.setState({
            contract: o.contract,
        });
    }
}