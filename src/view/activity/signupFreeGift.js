import React ,{Component} from 'react'

import signupFreegift from '../../images/registerM.png'
import {Cache, Contracts} from "../../module";
import {Schedule} from "../../lib";
export default class App extends Component{

    constructor(props){
        super(props);
        this.state={
            contract: null
        }
        if (Contracts.initial) {
            let [o] = Contracts.foreignArray;
            this.state.contract = o.contract;
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }

    render(){
        return(
            <div className="signupFreegift">
                <div className="goSign" onClick={()=>this.goSign()}></div>
                <div className="goTrade"  onClick={()=>this.goTrade()}></div>
                <img src={signupFreegift}/>
            </div>
        );
    }
    goSign(){
        if(Cache.isLogin()){
            return
        }else{
            this.props.history.push({
                pathname:'/register',
                where:this.props.location.where?this.props.location.where:null,
            })
        }
    }
    goTrade(){
        // if(Cache.isLogin()){
            this.props.history.push({
                pathname:'/trade',
                state:{
                    simulate: true,
                    contract: this.state.contract
                }
            })
        // }else{
        //     this.props.history.push({
        //         pathname:'/register',
        //         where:this.props.location.where
        //     })
        // }
    }
    updateContracts() {
        let [o] = Contracts.foreignArray;
        this.setState({
            contract: o.contract,
        });
    }
}