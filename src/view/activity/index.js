import React,{Component} from 'react'
import Header from '../common/header'
import {NavLink} from 'react-router-dom'
import RouteWithSubRoutes from '../../routes/routeWithSubRoutes'
export default class App extends Component{
    constructor(props){
        super(props);
        this.state={
            darkType:false
        }
    }
    componentWillMount(){
        this.setState({
            darkType:JSON.parse(localStorage.getItem('bgc'))
        })
    }

    render(){
        const a = this.props.routes;
        const where = this.props.location.where?this.props.location.where:null;
        return(
            <div className={`activity ${this.state.darkType?'dark':''}`}>
                <Header title={'活动专区'}{...this.props} back={where?(where==='broadcast'?('/broadcast'):(where==='home'?('/'):(null))):(null)}/>
                {a.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
                <div className={'footer'}>
                    <div>更多精彩活动，敬请期待</div>
                    <ul>
                        <NavLink
                            to={{
                                pathname:'/activity/freeGift',
                                where:where
                            }}
                            activeClassName={'selected'}>
                            <div>注册好礼</div>
                            {/*<div>体验好礼</div>*/}
                        </NavLink>
                        {/* <NavLink
                            to={{
                                pathname:'/activity/guideRule',
                                where:where
                            }}
                            activeClassName={'selected'}>
                            <div>积分规则</div>
                        </NavLink> */}
                        {/*<NavLink to={'/activity/renmai'} activeClassName={'selected'}>*/}
                            {/*<div>称霸</div>*/}
                            {/*<div>人脉奖</div>*/}
                        {/*</NavLink>*/}
                    </ul>
                </div>
            </div>
        );
    }

    selectActivity(index){
        this.setState({
            selectedIndex:index
        });
    }
}