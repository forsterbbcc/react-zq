import React, { Component } from 'react'
import Header from '../common/header';

import Guide from '../../images/guide.png';

export default class guiderule extends Component {
    constructor(props){
        super(props);
    }
    render() {
        const where = this.props.location.where;
        return (
            <div className='guiderule'>
                <Header title={'积分规则'} {...this.props}  back={where?(where==='broadcast'?('/broadcast'):(where==='home'?('/'):(null))):(null)}
                />
                <img src={Guide} className={'guide'} alt="" width="100%"/>
            </div>
        )
    }
}
