import React, {Component} from 'react';
import {Cache, Contracts, Data} from "../../module";
// import {Cache,Contracts,Data} from 'fcg-data'
import SetSLSP from './config';
import {Link} from 'react-router-dom';
import {Header} from "../common";
import LoadingFunction from "../../lib/loading";
import AlertFunction from "../../lib/AlertFunction";
import {Req, Schedule} from "../../lib";
import empty from '../../images/empty.png';
import {formatDate, getMoneyType} from "../../lib/tool";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rule: false,
            list: null,
            income: 0,
            config: !!this.props.location.config,
            target: this.props.location.target?this.props.location.target:null,
            type: this.props.location.state.type?2:1,
            settlement: null
        };
    }

    render() {
        return (
            <div className={'position'}>
                <Header title={this.props.location.state.simulate ? '模拟持仓' : '实盘持仓'} {...this.props}/>
                <div className={'statistics'}>
                    <div>
                        <div className={'des'}>浮动总盈亏</div>
                        <div
                            className={this.state.income >= 0 ? 'raise money' : 'fall money'}>{this.state.income >= 0 ? `+${this.state.income }` : this.state.income}<span>元</span>
                        </div>
                    </div>
                    <div className={'btn'} onClick={() => this.closeAll()}>一键清仓</div>
                </div>
                <div className={'nav'}>
                    <div className={this.state.type === 1 ? 'active' : ''} onClick={() => {
                        this.setState({type: 1})
                    }}>
                        持仓
                    </div>
                    <div className={this.state.type === 2 ? 'active' : ''} onClick={() => {
                        this.setState({type: 2},()=>{this.updateStore()})
                    }}>
                        结算
                    </div>
                </div>
                {
                    this.state.type === 1 ?
                        (
                            <div className={'warp'}>
                                {
                                    this.state.list === null ? '' : (
                                        this.state.list.length === 0 ? (
                                            <div className={'imgWrap'}>
                                                <img src={empty} alt=""/>
                                                <p>暂无数据</p>
                                            </div>
                                        ) : (
                                            <div>
                                                <ul className={'flat'}>
                                                    {
                                                        this.state.list.map((item) => {
                                                            return (
                                                                <li>
                                                                    <div className={'right'}>
                                                                        <div className={'title'}>
                                                                            <div>{item.commodity} ({Contracts.total[item.contract].code})</div>
                                                                            <div className={'iconBox'}>
                                                                                <div className={'circle deep'}>{getMoneyType(item.moneyType)}</div>&nbsp;
                                                                                <div className={`${item.isBuy ? 'redBg' : 'greenBg'} circle`}>{item.isBuy ? '涨' : '跌'}</div>&nbsp;
                                                                                <div className={'handNum'}>×{item.volume}手</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className={'idTitle'}>
                                                                            <div>订单编号:{item.id}</div>
                                                                            <div>开仓时间:{formatDate('y-m-d',{date:item.tradeTime.time})}</div>
                                                                        </div>
                                                                        <div className={'infoBox'}>
                                                                            <div className={'info'}>
                                                                                <div>
                                                                                    <div>买入{item.opPrice.toFixed(item.priceDigit)}</div>
                                                                                    <div className={'lastInfo'}>止盈{item.stopProfit}</div>
                                                                                </div>
                                                                                <div>
                                                                                    <div>当前{Data.total[item.contract].price}</div>
                                                                                    <div className={'lastInfo'}>止损{item.stopLoss}</div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                className={item.income >= 0 ? 'red money' : 'green money'}>
                                                                                {item.income > 0 ? `+${item.income}` : item.income}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className={'left'}>
                                                                        <div onClick={() => {
                                                                            this.setStopLoss(item.id)
                                                                        }}>
                                                                            <div className={'first'}>设置止盈止损</div>
                                                                        </div>
                                                                        <div className={'last'}
                                                                             onClick={() => this.close(item.id)}>
                                                                            <div>平仓</div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            )
                                                        })
                                                    }
                                                </ul>
                                            </div>
                                        )
                                    )
                                }
                                <SetSLSP show={this.state.config} target={this.searchTarget(this.state.target)}
                                         close={() => this.setState({config: false, target: null})} {...this.props}/>
                            </div>
                        ) : (
                            <div className={'warp'}>
                                {
                                    this.state.list === null ||this.state.settlement === null? '' : (
                                        this.state.settlement.length === 0 ? (
                                            <div className={'imgWrap'}>
                                                <img src={empty} className={'empty'}/>
                                                <p>暂时没有数据</p>
                                            </div>
                                        ) : (
                                            <ul className={'flat'}>
                                                {
                                                    this.state.settlement.map((item,index) => {
                                                        return (
                                                            <li key={index}>
                                                                <Link to={{
                                                                    pathname: '/listInfo',
                                                                    state: {simulate: this.props.location.state.simulate, info: item},
                                                                    from:'trade',
                                                                    name:this.props.location.name,
                                                                    id:this.props.location.id,
                                                                    code:this.props.location.code,
                                                                    // setType:true
                                                                }} className={'right'}>
                                                                    <div className={'title'}>
                                                                        <div>{item.commodity} ({item.contract.match(/[a-zA-Z]+/)})</div>
                                                                        <div className={'iconBox'}>
                                                                            <div className={'circle deep'}>{getMoneyType(item.moneyType)}</div>&nbsp;
                                                                            <div className={`${item.isBuy ? 'redBg' : 'greenBg'} circle`}>{item.isBuy ? '涨' : '跌'}</div>&nbsp;
                                                                            <div className={'handNum'}>×{item.volume}手</div>
                                                                        </div>
                                                                    </div>
                                                                    <div className={'infoBox'}>
                                                                        <div className={'info'}>
                                                                            <div
                                                                                className={'time'}>{formatDate('y-m-d h:i:s', {date: item.tradeTime.time})}</div>
                                                                            <div>
                                                                                <div>买入{item.opPrice.toFixed(item.priceDigit)}</div>
                                                                                <div
                                                                                    className={'lastInfo'}>止盈{item.stopProfit}</div>
                                                                            </div>
                                                                            <div>
                                                                                <div>卖出{item.cpPrice.toFixed(item.priceDigit)}</div>
                                                                                <div
                                                                                    className={'lastInfo'}>止损{item.stopLoss}</div>
                                                                            </div>
                                                                        </div>
                                                                        <div className={'money'}>

                                                                        </div>
                                                                    </div>
                                                                </Link>
                                                                <div className={'left'}>
                                                                    <div>
                                                                        <div
                                                                            className={'first'}>已结算({getMoneyType(item.moneyType)})
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        className={item.income >= 0 ? 'red last' : 'green last'}>{item.income > 0 ? `+${item.income }` : item.income}</div>
                                                                </div>
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        )
                                    )
                                }
                            </div>
                        )
                }
            </div>
        )
    }

    componentDidMount() {
        this.updateStore();
        this.mounted = true;
        if (Contracts.initial) {
            Data.start('position');
        } else {
            Schedule.addEventListener('contractsInitial', () => {
                Data.start('position');
            }, this)
        }
        if (!!Cache.tradeList) {
            this._keepUpdate = true;
            this.updatePosition(true);
        } else {
            Schedule.addEventListener('getSchemeInfo', () => {
                this._keepUpdate = true;
                this.updatePosition(true);
            }, this)
        }
    }

    componentWillUnmount() {
        this.mounted = false;
        Data.end('position');
        Schedule.removeEventListeners(this);
        clearTimeout(this._keepUpdate);
        this._keepUpdate = null;
    }

    async close(id) {
        try {
            await Req({
                url: '/api/trade/close.htm',
                type: 'POST',
                data: {
                    bettingId: id,
                    tradeType: this.props.location.state.simulate ? 2 : 1,
                    source: '下单'
                },
                animate: true
            });
            AlertFunction({
                title: '提示', msg: '卖出委托已提交', confirm: () => {
                    Cache.getUserInfo();
                }
            })
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg || err})
        }
    }

    async closeAll() {
        try {
            let t = this.state.list.map((o) => {
                return Req({
                    url: '/api/trade/close.htm',
                    type: 'POST',
                    data: {
                        bettingId: o.id,
                        tradeType: this.props.location.state.simulate ? 2 : 1,
                        source: '下单'
                    },
                    ignore: true
                });
            });
            LoadingFunction();

            Promise.all(t).then((o) => {
                Schedule.dispatchEvent({event: 'loadingEnd'});
                let success = 0;
                let failure = 0;
                o.forEach((e) => {
                    if (e.success) {
                        success++;
                    } else {
                        failure++;
                    }
                });
                AlertFunction({title: '提示', msg: `平仓委托已提交,成功${success}单,失败${failure}单`});
                Cache.getUserInfo();
            });
        } catch (err) {
            AlertFunction({title: '错误', msg: err.errorMsg || err});
        }
    }

    setStopLoss(id) {
        this.setState({
            config: true,
            target: id
        })
    }

    /**
     * 更新持仓
     */
    async updatePosition(init) {
        try {
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 1,
                    tradeType: this.props.location.state.simulate ? 2 : 1,
                    beginTime: '',
                    _: new Date().getTime(),
                },
                animate: init
            });
            if (data) {
                this.dealPosition(data);
            }
        } catch (err) {
            AlertFunction({title: '错误', msg: err['errorMsg'] || err});
        } finally {
            if (this._keepUpdate !== null) {
                this._keepUpdate = setTimeout(() => this.updatePosition(), 1000);
            }
        }
    }

    /**
     * 更新持仓数据
     */
    dealPosition(data) {
        let quote, scheme;
        let income = 0;
        let position = data.map((e) => {
            quote = Data.total[e.contract];
            scheme = Cache.tradeList[e.contract];
            if (quote) {
                e.unit = scheme.priceUnit.mul(scheme.priceChange).mul(e.moneyType === 0 ? 1 : e.moneyType === 1 ? 0.1 : 0.01);
                e.current = Number(quote.price) || 0;
                if (!!quote.price && !!e.opPrice) {
                    if (e.isBuy) {
                        e.income = e.current.sub(e.opPrice).mul(e.volume).mul(scheme.priceUnit).mul(e.moneyType === 0 ? 1 : e.moneyType === 1 ? 0.1 : 0.01);
                    } else {
                        e.income = e.opPrice.sub(e.current).mul(e.volume).mul(scheme.priceUnit).mul(e.moneyType === 0 ? 1 : e.moneyType === 1 ? 0.1 : 0.01);
                    }
                    income = income.add(e.income);
                    return e;
                } else {
                    e.income = 0;
                    return e;
                }
            } else {
                return null;
            }
        });
        position.remove(null);
        if (position.findIndex((e) => e === null) === -1) {
            if (this.mounted) {
                this.setState({list: position, income: income});
            }
        }
    }

    searchTarget(id) {
        if (!!id&&this.state.list) {
            return this.state.list.find((e) => e.id === id)
        } else {
            return {};
        }
    }

    //todo 获取结算
    async updateStore() {
        try {
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 2,
                    tradeType: this.props.location.state.simulate ? 2 : 1,
                    beginTime: '',
                    _: new Date().getTime()
                },
                animate: true
            });
            if (this.mounted) {
                this.setState({settlement: data});
            }
        } catch (err) {
            AlertFunction({title: '错误', msg: err['errorMsg'] || err});
        }
    }
}