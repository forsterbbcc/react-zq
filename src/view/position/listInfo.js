import React, {Component} from 'react';
import {Header} from "../common";
import {formatDate} from "../../lib/tool";
import {Contracts} from "../../module";

export default class App extends Component {
     info= null;
     simulate=null;
    constructor(props) {
        super(props);
        this.info  =this.props.location.state.info;
        this.simulate= this.props.location.state.simulate;
    }

    render() {
        const type = this.info.moneyType === 0? '元模式':(this.info.moneyType === 1? '角模式':'分模式');
        return (
            <div className={'listInfo'}>
                <Header title={this.simulate? '订单信息(模拟)':'订单信息'} {...this.props}/>
                <div className={'main'}>
                    <div className={'title'}>
                        <div>结算盈亏</div>
                        <div className={this.info.income > 0 ? 'money red':'money green'}>{this.info.income > 0 ? `+${this.info.income}`:this.info.income}<span>元</span></div>
                    </div>
                    <div className={'info'}>
                        {/*<h4>订单信息</h4>*/}
                        <div>
                            <p>交易品种</p>
                            <div>{this.info.commodity}</div>
                        </div>
                        <div>
                            <p>交易数量</p>
                            <div>{this.info.cpVolume}手</div>
                        </div>
                        <div>
                            <p>交易方向</p>
                            <div>{this.info.isBuy ?'涨':'跌'}</div>
                        </div>
                        <div>
                            <p>保证金</p>
                            <div>{Math.abs(this.info.stopLoss)}元</div>
                        </div>
                        <div>
                            <p>交易综合费</p>
                            <div>0元</div>
                        </div>
                        <div>
                            <p>交易模式</p>
                            <div>{type}</div>
                        </div>
                        <div>
                            <p>开仓均价</p>
                            <div>{this.info.opPrice}</div>
                        </div>
                        <div>
                            <p>平仓均价</p>
                            <div>{this.info.cpPrice}</div>
                        </div>
                        {/*<div>*/}
                        {/*<p>平仓类型</p>*/}
                        {/*<div>CL1805</div>*/}
                        {/*</div>*/}
                        <div>
                            <p>开仓时间</p>
                            <div>{formatDate('y-m-d h:i:s',{date:this.info.time.time})}</div>
                        </div>
                        <div>
                            <p>平仓时间</p>
                            <div>{formatDate('y-m-d h:i:s',{date:this.info.tradeTime.time})}</div>
                        </div>
                        <div>
                            <p>订单编号</p>
                            <div>{this.info.id}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}