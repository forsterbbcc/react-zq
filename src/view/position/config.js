import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Header} from "../common";
import {Req} from "../../lib";
import {Data} from "../../module";
import {getMoneyType} from "../../lib/tool";
import AlertFunction from "../../lib/AlertFunction";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stopProfit: 1,
            stopLoss: 1,
            unit: 1,
            update:true,
            lock:false
        };
        this.stopProfit = this.stopProfit.bind(this);
        this.addStopProfit = this.addStopProfit.bind(this);
        this.reduceStopProfit = this.reduceStopProfit.bind(this);
        this.stopLoss = this.stopLoss.bind(this);
        this.addStopLoss = this.addStopLoss.bind(this);
        this.reduceStopLoss = this.reduceStopLoss.bind(this);
        this.submit = this.submit.bind(this);
    }

    render() {
        if(!this.props.target){
            if(this.state.update){
                this.setState({
                    update:false
                });
                AlertFunction({title: '提示', msg: '已自动平仓', confirm: () => {
                        this.props.close();
                    }})
            }
            return null
        }else{
        return (
            <div className={`stopSPSL ${this.props.show ? 'come' : ''}`}>
                <Header title={'设置止盈止损'} back={() => {
                    this.props.close();
                    this.setState({
                        stopProfit: 1,
                        stopLoss: 1,
                        unit: 1
                    })
                }}/>
                <div className={'information'}>
                    <div className={'name'}>
                        <div>{this.props.target.commodity}</div>
                        <div className={`mid ${this.props.target.income >= 0 ? 'raise' : 'fall'}`}>{this.props.target.income}</div>

                    </div>
                    <div className={'info'}>
                        <div className={`mid ${this.props.target.isBuy ? 'raise' : 'fall'}`}>买{this.props.target.isBuy ? '涨' : '跌 '}{this.props.target.volume}手</div>
                        <div className={'mid'}>
                            {getMoneyType(this.props.target.moneyType)}
                        </div>
                    </div>
                    <div className={'newPrice'}>
                        <div>最新价{this.props.target.contract && Data.total[this.props.target.contract].price}</div>
                    </div>
                </div>
                <div className={'body'}>

                    <div className={'board'}>
                        <div className={'between'}>
                            <span>止盈金额</span>
                            <span>{this.props.target.unit && this.state.stopProfit.mul(this.props.target.unit).mul(this.props.target.volume)}</span>
                        </div>
                        <div className={'range-controller'}>
                            <input type="range" className={'input-range raise'} step={1} min={1}
                                   max={this.props.target.stopProfitBegin && (this.props.target.stopProfitBegin.div(this.state.unit))}
                                   onChange={this.stopProfit} value={this.state.stopProfit}/>
                        </div>

                        <div className={'between'}>
                            <span>止损金额</span>
                            <span>-{this.state.stopLoss.mul(this.state.unit)}</span>
                        </div>
                        <div className={'range-controller'}>
                            <input type="range" className={'input-range fall'} step={1} min={1}
                                   max={this.props.target.stopLossBegin && (Math.abs(this.props.target.stopLossBegin).div(this.state.unit))}
                                   onChange={this.stopLoss} value={this.state.stopLoss}/>
                        </div>

                        <div className={'submit'} onClick={this.submit}>
                            确定
                        </div>
                    </div>
                </div>
                <div className={'remark'}>
                    <h4> 温馨提示</h4>
                    <ul>
                        <li>止盈金额：当盈利多于等于该金额之后，系统自动平仓</li>
                        <li>止损金额：当亏损多于等于该金额之后，系统自动平仓</li>
                        <li>如需帮助，请 <Link to={{pathname:'/cs',
                            state:{simulate:this.props.location.state.simulate},
                            from:'trade',
                            name:this.props.location.name,
                            id:this.props.location.id,
                            code:this.props.location.code,
                            openConfig:true,target:this.props.target}}>联系客服</Link></li>
                    </ul>
                </div>
            </div>
        )
    }}

    stopProfit(e) {
        this.setState({
            stopProfit: e.currentTarget.valueAsNumber
        })
    }

    addStopProfit() {
        const max = this.props.target.stopProfitBegin && (this.props.target.stopProfitBegin.div(this.state.unit));
        this.setState({
            stopProfit: Math.min(this.state.stopProfit + 1, max)
        })
    }

    reduceStopProfit() {
        this.setState({
            stopProfit: Math.max(this.state.stopProfit - 1, 1)
        })
    }

    stopLoss(e) {
        this.setState({
            stopLoss: e.currentTarget.valueAsNumber
        })
    }

    addStopLoss() {
        const max = this.props.target.stopLossBegin && (Math.abs(this.props.target.stopLossBegin).div(this.state.unit));
        this.setState({
            stopLoss: Math.min(this.state.stopLoss + 1, max)
        })
    }

    reduceStopLoss(){
        this.setState({
            stopLoss: Math.max(this.state.stopLoss - 1, 1)
        })
    }

    async submit() {
        const {unit,volume} = this.props.target;

        if(this.props.target.income>this.state.stopProfit.mul(unit).mul(volume)){
            AlertFunction({title: '提示', msg: '止盈金额不能低于当前已盈利金额'});
        }else {
            try {
                await Req({
                    url: '/api/trade/spsl.htm',
                    type: 'POST',
                    data: {
                        bettingId: this.props.target.id,
                        tradeType: this.props.location.state.simulate ? 2 : 1,
                        stopProfit: this.state.stopProfit.mul(this.state.unit),
                        stopLoss: -this.state.stopLoss.mul(this.state.unit),
                        source: '设置止盈止损'
                    },
                    animate: true
                });
                AlertFunction({title: '提示', msg: '设置止盈止损成功'});
                this.props.close();
                this.setState({
                    stopProfit: 1,
                    stopLoss: 1,
                    unit: 1
                })
            } catch (err) {
                AlertFunction({title: '错误', msg: err.errorMsg || err});
            }
        }
    }

    componentWillReceiveProps({show, target}) {
        if (this.props.show !== show) {
            if (show === true) {
                const n = target.unit.mul(target.volume);
                this.setState({
                    unit: n,
                    stopProfit: target.stopProfit.div(n),
                    stopLoss: Math.abs(target.stopLoss).div(n)
                })
            }
        }
        if (this.props.show === true && this.state.lock===false && this.props.location.where === 'comeBack' && JSON.stringify(target)!=='{}') {
            const n = target.unit.mul(target.volume);
            this.setState({
                unit: n,
                stopProfit: target.stopProfit.div(n),
                stopLoss: Math.abs(target.stopLoss).div(n),
                lock:true
            })
        }
    }
}