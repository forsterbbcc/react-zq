import React, {Component} from 'react';
import ReactSVG from 'react-svg';
import SetSLSP from './config';
import {Req, Schedule} from "../../lib/index";
import AlertFunction from "../../lib/AlertFunction";
import {Data, Cache, Contracts} from "../../module/index";
import LoadingFunction from "../../lib/loading";

import empty from '../../images/svg/empty.svg';

import {Link} from "react-router-dom";
export default class App extends Component {

    _keepUpdate = null;

    constructor(props) {
        super(props);
        this.state = {
            list: null,
            income: 0,
            config: false,
            target: null,
            contract: null,
            simulate:this.props.location.state?this.props.location.state.simulate:null
        }
        if (Contracts.initial) {
            let [o] = Contracts.foreignArray;
            this.state.contract = o.contract;
            Data.start('updateBrief');
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }
    updateContracts() {
        let [o] = Contracts.foreignArray;
        this.setState({
            contract: o.contract,
        });
    }
    render() {
        return (
            <div className={'warp'}>
                {this.state.list === null||this.state.list.length===0 ?(
                    <div className={'statistics'}>
                        <div className={'des des0'}>已平仓&nbsp;&nbsp;</div>
                            <Link to={{pathname: '/trade', state: {simulate:this.state.simulate, contract: this.state.contract}}}>
                            去交易
                            </Link>
                        </div>
                        ):(
                        <div className={'statistics'}>
                            <div className={'des'}>持仓盈亏&nbsp;&nbsp;
                                <span
                                className={this.state.income >= 0 ? 'raise' : 'fall'}>
                                    {this.state.income>=0?'+':''}{this.state.income.toFixed(2)}
                                </span>&nbsp;
                                <span className="yuan">(元)</span>
                            </div>
                        <div className={'btn'} onClick={() => this.closeAll()}>一键平仓</div>
                    </div>
                )}
                {this.state.list === null ? '' : (
                    this.state.list.length === 0 ? (
                        <div className={'imgWrap'}>
                            <ReactSVG path={empty} className={'placeholder'}/>
                        </div>
                    ) : (
                        this.state.list?(
                            <ul className={'flat'}>
                                {
                                    this.state.list.map((e, index) => {
                                        return (
                                            <li>
                                                    <div className="upTop">
                                                        <span>{e.commodity}&nbsp;({e.contract})&nbsp;ID{e.id}</span>
                                                        <p className={'config'} onClick={() => {
                                                            this.setStopLoss(e.id)
                                                        }}>
                                                            设置止盈止损
                                                        </p>
                                                    </div>
                                                <div className={'wrap'}>
                                                    <div className={'left'}>
                                                        <div className="top">
                                                            <span className={e.income >0 ? 'raise' : 'fall'}>
                                                            {/*<span>*/}
                                                                {e.isBuy ? '买涨' : '买跌'}
                                                            </span>
                                                            <span className={e.income >0 ?'raise' : 'fall'}>{e.volume}手</span>
                                                            <span
                                                                className={`${e.income >0 ?'raise' : 'fall'}`}>
                                                            ({e.moneyType === 0 ?('元'):(e.moneyType === 1 ?'角' : '分')})
                                                            </span>&nbsp;&nbsp;&nbsp;
                                                            <span
                                                                className={e.income >0 ? 'raise' : 'fall'}>
                                                            &nbsp;{e.income >0 ? '+' : ''}{e.income}</span>
                                                        </div>
                                                        <div className="mid">
                                                            <span>止盈&nbsp;<span> {e.stopProfit}元</span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span>买入&nbsp;{e.opPrice.toFixed(e.priceDigit)}</span>
                                                        </div>
                                                        <div className="bot">
                                                            <span>止损&nbsp;{e.stopLoss}元</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                             <span>当前&nbsp;{Number(Data.total[e.contract].price).toFixed(e.priceDigit)}</span>
                                                        </div>
                                                    </div>
                                                    <div className={'show'}>
                                                        <p className={'apply'}
                                                           onClick={() => this.close(e.id)}>
                                                            卖出
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        ):(
                            <div className={'imgWrap'}>
                                <ReactSVG path={empty} className={'placeholder'}/>
                            </div>
                        )

                    )
                )
                }
                {
                    this.state.config?<SetSLSP target={this.searchTarget(this.state.target)}
                    close={() => this.setState({config: false, target: null})}
                                               show={this.state.config}
                                               {...this.props}/>:null
                }

            </div>
        )
    }

    componentDidMount() {
        this.mounted = true;
        if (Contracts.initial) {
            Data.start('position');
        } else {
            Schedule.addEventListener('contractsInitial',() => {
                Data.start('position');
            }, this)
        }
        if (!!Cache.tradeList) {
            this._keepUpdate = true;
            this.updatePosition(true);
        } else {
            Schedule.addEventListener('getSchemeInfo', () => {
                this._keepUpdate = true;
                this.updatePosition(true);
            }, this)
        }
    }

    componentWillUnmount() {
        this.mounted = false;
        Data.end('position');
        Schedule.removeEventListeners(this);
        clearTimeout(this._keepUpdate);
        this._keepUpdate = null;
    }

    async close(id) {
        try {
            await Req({
                url: '/api/trade/close.htm',
                type: 'POST',
                data: {
                    bettingId: id,
                    tradeType: this.props.location.state.simulate ? 2 : 1,
                    source: '下单'
                },
                animate: true
            });
            AlertFunction({title: '提示', msg: '卖出委托已提交'})
            Cache.getUserInfo();
        } catch (err) {
            console.log('222');
            AlertFunction({title: '错误', msg: err.errorMsg || err})
        }
    }


    async closeAll() {
        try {
            let t = this.state.list.map((o) => {
                return Req({
                    url: '/api/trade/close.htm',
                    type: 'POST',
                    data: {
                        bettingId: o.id,
                        tradeType: this.props.location.state.simulate ? 2 : 1,
                        source: '下单'
                    },
                    ignore: true
                });
            });
            LoadingFunction();
            Promise.all(t).then((o) => {
                Schedule.dispatchEvent({event: 'loadingEnd'});
                let success = 0;
                let failure = 0;
                o.forEach((e) => {
                    if (e.success) {
                        success++;
                    } else {
                        failure++;
                    }
                });
                AlertFunction({title: '提示', msg: `平仓委托已提交,成功${success}单,失败${failure}单`});
                Cache.getUserInfo();
            });
        } catch (err) {
            console.log('000');
            AlertFunction({title: '错误', msg: err.errorMsg || err});
        }
    }

    setStopLoss(id) {
        this.setState({
            config: true,
            target: id
        })
    }

    /**
     * 更新持仓
     */
    async updatePosition(init) {
        try {
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 1,
                    tradeType: this.props.location.state.simulate ? 2 : 1,
                    beginTime: '',
                    _: new Date().getTime(),
                },
                animate: init
            });
            if (data) {
                this.dealPosition(data);
            }
        } catch (err) {
            console.log('111');
            AlertFunction({title: '错误', msg: err['errorMsg'] || err});
        } finally {
            if (this._keepUpdate !== null) {
                this._keepUpdate = setTimeout(() => this.updatePosition(), 1000);
            }
        }
    }

    /**
     * 更新持仓数据
     */
    dealPosition(data) {
        let quote, scheme;
        let income = 0;
        let position = data.map((e) => {
            quote = Data.total[e.contract];
            scheme = Cache.tradeList[e.contract];
            if (quote) {
                e.unit = scheme.priceUnit.mul(scheme.priceChange).mul(e.moneyType === 0 ? 1 : (e.moneyType === 1 ? 0.1 : 0.01));
                e.current = Number(quote.price) || 0;
                if (!!quote.price && !!e.opPrice) {
                    if (e.isBuy) {
                        e.income = e.current.sub(e.opPrice).mul(e.volume).mul(scheme.priceUnit).mul(e.moneyType === 0 ? 1 : (e.moneyType === 1 ? 0.1 : 0.01));
                    } else {
                        e.income = e.opPrice.sub(e.current).mul(e.volume).mul(scheme.priceUnit).mul(e.moneyType === 0 ? 1 : (e.moneyType === 1 ? 0.1 : 0.01));
                    }
                    income = income.add(e.income);
                    return e;
                } else {
                    e.income = 0;
                    return e;
                }
            } else {
                return null;
            }
        });
        position.remove(null);
        if (position.findIndex((e) => e === null) === -1) {
            if (this.mounted) {
                this.setState({list: position, income: income});
            }
        }
    }

    searchTarget(id) {
        if (!!id) {
            return this.state.list.find((e) => e.id === id)
        } else {
            return {};
        }
    }
}