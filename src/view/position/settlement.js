import React, {Component} from 'react';
import ReactSVG from 'react-svg';
import empty from '../../images/svg/empty.svg';
import {Req} from "../../lib/index";
import AlertFunction from "../../lib/AlertFunction";
import {formatDate} from "../../lib/tool";
import {Link} from "react-router-dom";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: null,
            sum:null
        }
    }

    render() {
        return (
            <div className={'warp'}>
                <div className="statistics">
                    <div className={'des'}>结算盈亏:&nbsp;&nbsp;
                        <span className={this.state.sum<0?'fall':'raise'}>
                            {this.state.sum>0?'+':''}
                            {this.state.list!==null&&this.state.list.length!==0?(this.state.sum?this.state.sum.toFixed(2):null):(0)}
                        </span>&nbsp;
                        <span className="yuan">(元)</span>
                    </div>
                </div>
                {
                    this.state.list === null ? '' : (
                        this.state.list.length === 0 ? (
                            <div className={'imgWrap'}>
                                <ReactSVG path={empty} className={'placeholder'}/>

                            </div>
                        ) : (
                            <ul className={'flat'}>
                                {
                                    this.state.list.map((e,i) => {
                                        return (
                                            <Link to={{
                                                pathname: '/position_detail',
                                                state: {simulate:this.props.location.state.simulate,
                                                    list:this.state.list?this.state.list[i]:null,
                                                    where:this.props.location.where
                                                }
                                            }}>
                                            <li>
                                                <div className="upTop">
                                                    <span>{e.commodity}&nbsp;({e.contract})&nbsp;ID{e.id}</span>
                                                    <span>{formatDate('m-d h:i:s', {date: e.tradeTime.time})}</span>
                                                </div>
                                                <div className="wrap wrap0">
                                                    <div className="left">
                                                        <div className="top special">
                                                            <div>
                                                                <span className={e.income >0 ? 'up' : 'down'}>
                                                                    {e.isBuy ? '买涨' : '买跌'}
                                                                </span>
                                                                <span className={e.income >0 ? 'up' : 'down'}>{e.volume}手</span>
                                                                <span className={e.income >0 ? 'up' : 'down'}>
                                                                    ({e.moneyType === 0 ?('元'):(e.moneyType === 1 ?'角' : '分')})
                                                                </span>&nbsp;&nbsp;&nbsp;
                                                                <span className={e.income >0 ? 'raise' : 'fall'}>
                                                                    {e.income>0?'+'+e.income:e.income}元
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div className="mid">
                                                            <span>止盈&nbsp;{e.stopProfit}元</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span>买入&nbsp;{e.opPrice.toFixed(e.priceDigit)}</span>
                                                        </div>
                                                        <div className="bot">
                                                            <span>止损&nbsp;{e.stopLoss}元</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span>卖出&nbsp;{e.cpPrice.toFixed(e.priceDigit)}</span>
                                                        </div>
                                                    </div>
                                                    <div className="right">
                                                        <div className={'status'}>
                                                            {e.tradeStatus === 14 ? '结算成功' : '等待结算'}
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </Link>
                                        )
                                    })
                                }
                            </ul>
                        )
                    )
                }
            </div>
        )
    }

    componentDidMount() {
        this.mounted = true;
        this.updateStore();
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    async updateStore() {
        console.log(111);
        try {
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 2,
                    tradeType: this.props.location.state.simulate ? 2 : 1,
                    beginTime: '',
                    _: new Date().getTime()
                },
                animate: true
            });
            if (this.mounted) {
                if (data)
                    this.setState({list: data});
                const sumArr = data.map((item,index)=>{
                    return item.income
                });
                const sum = eval(sumArr.join("+"));
                this.setState({sum});
            }
        } catch (err) {
            AlertFunction({title: '错误', msg: err['errorMsg'] || err});
        }
    }
}