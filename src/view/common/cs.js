import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Schedule} from "../../lib/index";
import {Cache} from "../../module/index";

export default class App extends Component {

    state = {
        link: `http://chat6.livechatvalue.com/chat/chatClient/chatbox.jsp?companyID=1006450&configID=59169&jid=8704485092&info=${window.encodeURIComponent(`userId=$ {Cache.userId}&name=${Cache.username}&memo=${Cache.realName}`)}`
    };

    render() {
        // return (
        //     <a {...this.props} style={{color:'#085AAD'}} href={this.state.link} target={'_blank'}>
        //         {this.props.children}
        //     </a>
        // )
        return (
            <Link to={'/customerService'} style={{color:'#0d265e'}}>
                {this.props.children}
            </Link>
        )
    }

    componentDidMount() {
        Schedule.addEventListener('loginCallback', this.callback, this);
        Schedule.addEventListener('getUserInfo', this.callback, this);
    }

    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }

    callback(){
        this.setState({
            link: `http://chat6.livechatvalue.com/chat/chatClient/chatbox.jsp?companyID=1006450&configID=59169&jid=8704485092&info=${window.encodeURIComponent(`userId=$ {Cache.userId}&name=${Cache.username}&memo=${Cache.realName}`)}`
        })
    }
}