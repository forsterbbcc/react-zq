import React, {Component} from 'react';
import {Cache} from "../../module/index";
import {Schedule} from "../../lib/index";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            balance:Cache.realBalance
        }
    }

    render() {
        return (
            <div className={'allbalance'}>
                <b>{this.state.balance.toFixed(2)}</b>
                <span>账户余额(元)</span>
            </div>
        )
    }

    componentDidMount(){
        if(Cache.userId !== ''){
            this.setState({
                balance:Cache.realBalance
            })
        }else{
            Schedule.addEventListener('getUserInfo',()=>{
                this.setState({
                    balance:Cache.realBalance
                })
            },this)
        }
    }

    componentWillUnmount(){
        Schedule.removeEventListeners(this)
    }

}