import footer from './footer';
import header from './header';
import alert from './alert';
import loading from './loading.js';
import selectBank from './selectBank';
import balance from './balance';
import drawer from './drawer';
import svg from './svg';
import cs from './cs'
import select from './select'

export const Footer = footer;
export const Header = header;
export const Alert = alert;
export const Loading = loading;
export const SelectBank = selectBank;
export const Balance = balance;
export const Drawer = drawer;
export const Svg = svg;
export const Cs = cs;
export const Select = select;
