import React, {Component} from 'react';
import ReactSvg from 'react-svg';

import arrow from '../../images/svg/right-arrow.svg';
import back from '../../images/svg/back.svg'

export default class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className={'select'}>
                <div className={'selectHeader'}>
                    <div className={'backTag'} onClick={()=>this.props.selectBank('',0)}>
                        <ReactSvg path={back} className={'arrow'} wrapperClassName={'out'}/>
                    </div>
                    <div className={'title'}>请选择充值银行</div>
                </div>
                <div className={'selectMain'}>
                    <ul>
                        <li onClick={() => this.props.selectBank('工商银行',0)}><span>工商银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('建设银行',0)}><span>建设银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('农业银行',0)}><span>农业银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('招商银行',0)}><span>招商银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('中国银行',0)}><span>中国银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('交通银行',0)}><span>交通银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('邮政储蓄',0)}><span>邮政储蓄</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('民生银行',0)}><span>民生银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('浦发银行',0)}><span>浦发银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('兴业银行',0)}><span>兴业银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('华夏银行',0)}><span>华夏银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('光大银行',0)}><span>光大银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('广发银行',0)}><span>广发银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('中信银行',0)}><span>中信银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                        <li onClick={() => this.props.selectBank('平安银行',0)}><span>平安银行</span> <ReactSvg path={arrow} className={'arrow'} wrapperClassName={'out'}/></li>
                    </ul>
                </div>
            </div>
        )
    }
}