import React from 'react';
import ReactSvg from 'react-svg';

const App = props =>{
    return(
        <ReactSvg path={props.path} className={`${props.className} ${props.status||''}`} wrapperClassName={`${props.className} ${props.position||''} svg`}/>
    )
};


export default App;