import React, {Component} from 'react';
import ReactSvg from 'react-svg';
import {Link} from 'react-router-dom';

import back from '../../images/svg/back.svg'


export default class App extends Component {    
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'header4'}>
                <div onClick={this.props.history.goBack} className={'backTag'}>
                    <ReactSvg path={back} className={'arrow'} wrapperClassName={'out'}/>
                </div>
                <div className={'title'}>
                    <p>{this.props.title}</p>
                    <p>(服务时间：8:30-23:30)</p>
                </div>
                {
                    this.props.right ?
                        (<Link to={this.props.goTo} className={'link'}>{this.props.right}</Link>) : ''
                }
                {
                    this.props.rightText ?
                        (<span onClick={this.props.goTo} className={'link'}>{this.props.rightText}</span>) : ''
                }
            </div>
        );
    }
}