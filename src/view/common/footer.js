import React, {Component} from 'react';
import {NavLink, Link} from 'react-router-dom';
import {Svg} from "./index";


import home from '../../images/svg/home.svg';
import news from '../../images/svg/news.svg';
import service from '../../images/svg/service.svg';
import mine from '../../images/svg/mine.svg';
import transfer from '../../images/svg/transfer.svg';

import {Contracts, Cache} from "../../module";
import {Schedule} from "../../lib";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: null,
            name:null,
            unRead:Cache.unRead||0
        };
        if (Contracts.initial) {
            let [o] = Contracts.foreignArray;
            this.state.code = o.contract;
            this.state.name = o.name;
            this.state.id = o.code
        } else {
            Schedule.addEventListener('contractsInitial', this.updateContracts, this);
        }
    }

    render() {
        // console.log(this.state.unRead,Cache.unRead)
        return (
            <footer>
                <NavLink to={'/'} activeClassName={'footActive'}
                         isActive={(match, location) => location.pathname === '/'}>
                    <Svg path={home} className={'iconStyle news'}/>
                    <span className={'text'}>首页</span>
                </NavLink>
                <NavLink to={'/news/live'}
                         isActive={(match, location) => location.pathname.indexOf('/news/') !== -1}
                         activeClassName={'footActive'}>
                    <Svg path={news} className={'iconStyle news'}/>
                    <span className={'text'}>资讯</span>
                </NavLink>
                <NavLink to={{pathname: '/trade',
                    state: {
                        simulate: false,
                        // id: this.state.id,
                        name:this.state.name,
                        contract: this.state.code,
                        code:this.state.code}}}
                         activeClassName={'footActive'}>
                    <Svg path={transfer} className={'iconStyle transfer'}/>
                    <span className={'text'}>交易</span>
                </NavLink>
                <NavLink to={'/cs'}
                         activeClassName={'footActive'}>
                    <Svg path={service} className={'iconStyle news'}/>
                    <span className={'text'}>客服</span>
                    <div className={'unRead'}>{this.state.unRead>99?'99+':this.state.unRead}</div>
                </NavLink>
                <NavLink to={'/mine'}
                         activeClassName={'footActive'}>
                    <Svg path={mine} className={'iconStyle mine'}/>
                    <span className={'text'}>我的</span>
                </NavLink>
            </footer>
        )
    }

    componentDidMount() {
        Schedule.addEventListener('getUserInfo', this.getUserCallback, this);
    }
    getUserCallback() {
        this.setState({
            unRead: Cache.unRead,
        });
    }
    componentWillUnmount() {
        Schedule.removeEventListeners(this);
    }
    updateContracts() {
        let [o] = Contracts.foreignArray;
        this.setState({
            code: o.contract,
            name: o.name,
            id:o.code
        })
    }
}