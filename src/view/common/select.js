import React, {Component} from 'react';
import ReactSvg from 'react-svg';
import {Svg} from "./index";
import arrow from '../../images/svg/right-arrow.svg';
import back from '../../images/svg/back.svg'

export default class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className={'select'}>
                <div className={'selectHeader'}>
                    <div className={'backTag'} onClick={() => this.props.close()}>
                        <Svg path={back} className={'arrow'}/>
                    </div>
                    <div className={'title'}>{this.props.title}</div>
                </div>
                <div className={'selectMain'}>
                    <ul>
                        {
                            Object.entries(this.props.list).map(([key,value]) => {
                                return (
                                    <li onClick={() => this.props.callback(key,value)}>{key} <ReactSvg path={arrow}
                                                                                                        className={'arrow'}
                                                                                                        wrapperClassName={'out'}/>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }
}