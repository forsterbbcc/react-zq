import React, {Component} from 'react';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            out: false
        }
    }

    render() {
        return (
            <div className={'alertModel'}>
                <div className={'alertCell'}>
                    <div className={`alertBody transfer ${this.state.out ? 'move-out' : ''}`}>
                        <div className={'alertTitle'}>{this.props.title || 'GS'}</div>
                        <div className={'alertMain'} dangerouslySetInnerHTML={{__html: this.props.msg}}/>
                        {
                            <div className={'alertFoot'}>
                                <div className={'alertButton'} onClick={() => {
                                    this.setState({out: true});
                                    if (this.props.confirm) {
                                        this.props.confirm();
                                    }
                                    setTimeout(() => this.props.close(), 200)
                                }}>{this.props.yes || '确 定'}
                                </div>
                                {
                                    this.props.hasCancel ? <div className={'alertButton cancel'} onClick={() => {
                                        this.setState({out: true});
                                        if (this.props.cancel) {
                                            this.props.cancel();
                                        }
                                        setTimeout(() => this.props.close(), 200)
                                    }}>取 消</div> : ''
                                }
                            </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}
