import React, {Component} from 'react';
// import ReactSvg from 'react-svg';
import {Link} from 'react-router-dom';
import {Svg} from "./index";

import arrow from '../../images/svg/back.svg'

export default class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'header'}>
                {
                    this.props.back ? (
                        typeof this.props.back === 'string' ? (
                            <Link to={this.props.back} className={'backTag'}>
                                <Svg path={arrow} className={'arrow'}/>
                            </Link>
                        ) : (
                            <div onClick={() => this.props.back()} className={'backTag'}>
                                <Svg path={arrow} className={'arrow'}/>
                            </div>
                        )
                    ) : (
                        this.props.location.setType?(
                            <Link to={{pathname:'/position',
                                state:{simulate: this.props.location.state.simulate,type:2},
                                from:'trade',
                                name:this.props.location.name,
                                id:this.props.location.id,
                                code:this.props.location.code
                            }} className={'backTag'}>
                                <Svg path={arrow} className={'arrow'}/>
                            </Link>
                            ):(
                                this.props.location.openConfig?(
                                    <Link to={{pathname:'/position',
                                        state:{simulate: this.props.location.state.simulate},
                                        config:true,
                                        target:this.props.location.target.id,
                                        where:'comeBack',
                                        from:'trade',
                                        name:this.props.location.name,
                                        id:this.props.location.id,
                                        code:this.props.location.code
                                    }} className={'backTag'}>
                                        <Svg path={arrow} className={'arrow'}/>
                                    </Link>
                                    ):(
                                    this.props.location.pathname === '/position'?(
                                        !!this.props.location.from&&this.props.location.from === 'trade'?(
                                            <Link to={{
                                                pathname:'/trade',
                                                state:{
                                                    simulate:!!this.props.location.state.simulate,
                                                    name:!!this.props.location.name&&this.props.location.name,
                                                    id:!!this.props.location.id&&this.props.location.id,
                                                    code:!!this.props.location.code&&this.props.location.code
                                                }
                                            }} className={'backTag backTag0'}>
                                                <Svg path={arrow} className={'arrow'}/>
                                            </Link>
                                        ):(
                                            <Link to={{pathname:'/'}} className={'backTag'}>
                                                <Svg path={arrow} className={'arrow'}/>
                                            </Link>
                                        )
                                    ):(
                                        <div onClick={this.props.history.goBack} className={'backTag'}>
                                            <Svg path={arrow} className={'arrow'}/>
                                        </div>
                                    )

                                )
                        )
                    )
                }
                <div className={'title'}>{this.props.title}</div>
                {
                    this.props.right ?
                        (<Link to={{pathname:this.props.goTo,state:{simulate:this.props.simulate}}} className={'link'}>{this.props.right}</Link>) : ''
                }
            </div>
        );
    }
}