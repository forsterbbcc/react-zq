import React, {Component} from 'react';
// import index from '../images/404.png';
import {Link} from 'react-router-dom';

export default class App extends Component {
    render() {
        return (
            <div className={'redirectPage'}>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            {/*<img src={index} alt=""/>*/}
                            <Link to={'/'}>回到首页</Link>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}