import React from 'react';
import {Route, Redirect} from 'react-router-dom';

const App = route => {
    const isLogin = localStorage.getItem('isLogin');
    if (route.exact) {
        return (<Route exact path={route.path} render={props => {
            return(
                <route.component {...props} routes={route.routes}/>
            );
        }}/>)
    } else {
        return (<Route path={route.path} render={props => {
            if(route.auth && isLogin === null){
                return(
                    <Redirect to={{pathname:'/login',state:{from:props.location}}}/>
                )
            }else{
                return(
                    <route.component {...props} routes={route.routes}/>
                )
            }
        }}/>)
    }
};

export default App
