import home from '../view/home';
import login from '../view/login';
import register from '../view/register';
import mine from '../view/mine';
import realNameVerification from '../view/mine/realNameVerification';
import changePassword from '../view/mine/changePassword';
import changeWithdrawPassword from '../view/mine/changeWithdrawPasswd';
import changePhoneNumber from '../view/mine/changePhoneNumber';
import bankCardList from '../view/mine/bankCardList';
import addBankCard from '../view/mine/addBankCard';
import changeBankInfo from '../view/mine/changeBankInfo';
import cs from '../view/cs';
import trade from '../view/trade';
import position from '../view/position';
import position_live from '../view/position/live';
import position_settlement from '../view/position/settlement';
import position_detail from '../view/position_detail/index';
import position_failure from '../view/position/failure';

import listInfo from '../view/position/listInfo';
import info from '../view/info';
import gold from '../view/info/gold';
import live from '../view/info/live';
import oil from '../view/info/oil';
import finance from '../view/info/finance';
import recommendFriend from '../view/mine/recommendFriends';
import myUser from '../view/mine/myUser';
import list from '../view/moneyList';
import list_all from '../view/moneyList/all';
import promotionCommission from '../view/moneyList/promotionCommission';
import transaction from '../view/moneyList/transaction';
import transactionDetail from '../view/moneyList/transactionDetail';
import moneyListDetail from './../view/moneyList/listDetail';
import promotion from './../view/mine/promotion';
import notice from './../view/info/notice';
import recoverPassword from '../view/recoverPassword';
import recoverWithdrawPassword from '../view/mine/recoverWithdrawPassword';
import live800 from '../view/mine/live800';

// import order from '../view/order';
// import guide from '../view/cs/guide';
import detail from '../view/info/detail';
//活动
import activity from '../view/activity'
import guideRule from '../view/activity/guideRule';
import renmai from '../view/activity/prizeRenMai'
import freeGift from '../view/activity/signupFreeGift'
import freeToken from '../view/activity/signupFreeToken'

// import accountSet from '../view/account/accountSet';
// import realName from '../view/account/realNameVerification';

import withdraw from './../view/withdraw';
import card from './../view/withdraw/card';
import alipay from './../view/withdraw/alipay';
import history from './../view/withdraw/history';
// import registerActivity from './../view/activity/registerActivity'
// import shareActivity from './../view/activity/shareActivity'
// import quotation from './../view/quotation'


//todo 存款
import recharge from '../view/recharge';
import rechargeDpBank from '../view/recharge/rechargeDpBank';
import rechargeXXPayDpBank from '../view/recharge/rechargeXXPayDpBank';
import rechargeXXPayWind from '../view/recharge/rechargeXXPayWind';
import rechargeAlipayBank from '../view/recharge/rechargeAlipayBank';
import rechargeXXPayAlipayBank from '../view/recharge/rechargeXXPayAlipayBank';
import rechargeWalletPay from '../view/recharge/rechargeWalletPay';
import rechargeDora from '../view/recharge/rechargeDora';
import rechargeWapAlipay from '../view/recharge/rechargeWapAlipay';
import rechargeWXPay from '../view/recharge/rechargeWXPay';
import rechargeWX2Pay from '../view/recharge/rechargeWX2Pay';
import rechargeXXPay from '../view/recharge/rechargeXXPay';
import rechargeXXPayHFT from '../view/recharge/rechargeXXPayHFT';
import rechargeXXPayHuyun from '../view/recharge/rechargeXXPayHuyun';
import rechargeXXPayQQ from '../view/recharge/rechargeXXPayQQ';
import rechargeXXPayBaidu from '../view/recharge/rechargeXXPayBaidu';
import rechargeXXPayQuick from '../view/recharge/rechargeXXPayQuick';
import rechargeXXPayAlipay from '../view/recharge/rechargeXXPayAlipay';
import rechargeXXPayDora from '../view/recharge/rechargeXXPayDora';
import rechargeXXPayWXPay from '../view/recharge/rechargeXXPayWXPay';
import rechargeXXPayNovaPay from '../view/recharge/rechargeXXPayNovaPay';
import rechargeXXPayWechatPay from '../view/recharge/rechargeXXPayWechatPay';
import rechargeXXPayWXQR from '../view/recharge/rechargeXXPayWXQR';
import rechargeXXPayYL from '../view/recharge/rechargeXXPayYL';
import rechargeXXPayXD from '../view/recharge/rechargeXXPayXD';
import rechargeXXPayCHYL from '../view/recharge/rechargeXXPayCHYL';
import rechargeXXPayCP from '../view/recharge/rechargeXXPayCP';
import coinPay from '../view/recharge/coinPay';
import records from '../view/recharge/record';
import rechargeXXPayOnePointPay from '../view/recharge/opp/rechargeXXPayOnePointPay';
import bindOppCardAdd from '../view/recharge/opp/bindOppCardAdd';
import bindOppCardList from '../view/recharge/opp/bindOppCardList';
import rechargeXXPaySpay from '../view/recharge/rechargeXXPaySpay';
import rechargeXXPayDpBankTeching from '../view/recharge/rechargeXXPayDpBankTeching';
import rechargeXXPayFlashPay from '../view/recharge/rechargeXXPayFlashPay';
import rechargeXXPayDLB from '../view/recharge/rechargeXXPayDLB';
import rechargeXXPaySQPay from '../view/recharge/sq/rechargeXXPaySQPay';
import bindSQCardAdd from '../view/recharge/sq/bindSQCardAdd';
import bindSQCardList from '../view/recharge/sq/bindSQCardList';
import rechargeXXPayCPBank from '../view/recharge/rechargeXXPayCPBank';
import rechargeXXPayRainbowAlipay from '../view/recharge/rechargeXXPayRainbowAlipay';
import rechargeXXPayRainbowWX from '../view/recharge/rechargeXXPayRainbowWX';


export default [
    {
        path: '/',
        exact: true,
        component: home
    },
    {
        path: '/login',
        component: login
    },
    {
        path: '/detail/:id',
        component: detail,
    },
    {
        path: '/register',
        component: register
    },
    {
        path: '/trade',
        component: trade
    },
    {
        path: '/cs',
        component: cs
    },
    {
        path: '/mine',
        auth: true,
        component: mine
    },
    {
        path: '/realName',
        component: realNameVerification
    },
    {
        path: '/bankCardList',
        component: bankCardList
    },
    {
        path: '/addBankCard',
        component: addBankCard
    },
    {
        path: '/changeBankInfo',
        component: changeBankInfo
    },
    {
        path: '/changePassword',
        component: changePassword
    },
    {
        path: '/changeWithdrawPassword',
        component: changeWithdrawPassword
    },
    {
        path: '/changePhoneNumber',
        component: changePhoneNumber
    },
    {
        path: '/position',
        component: position,
        auth: true,
        routes: [
            {
                path: '/position/live',
                component: position_live,
                auth: true,
                exact: true
            },
            {
                path: '/position/settlement',
                component: position_settlement,
                auth: true
            },
            {
                path: '/position/failure',
                component: position_failure,
                auth: true
            }
        ]
    },
    {
        path: '/position_detail',
        component: position_detail,
        auth: true
    },
    {
        path: '/listInfo',
        component: listInfo,
    },
    {
        path: '/activity',
        component: activity,
        routes: [
            {
                path: '/activity/guideRule',
                component: guideRule
            },
            {
                path: '/activity/renmai',
                component: renmai
            },
            {
                path: '/activity/freeGift',
                component: freeGift,
            },
            {
                path: '/activity/freeToken',
                component: freeToken
            }
        ]

    },
    {
        path: '/news',
        component: info,
        routes: [
            {
                path: '/news/live',
                component: live
            },
            {
                path: '/news/gold',
                component: gold
            },
            {
                path: '/news/finance',
                component: finance
            },
            {
                path: '/news/oil',
                component: oil
            },
            {
                path: '/news/notice',
                component: notice
            }
        ]
    },
    {
        path: '/recommendFriend',
        component: recommendFriend,
    },
    {
        path: '/myUser',
        component: myUser,
    },
    {
        path: '/promotion',
        component: promotion,
    },
    {
        path: '/customerService',
        component: live800
    },
    {
        path: '/list',
        component: list,
        auth: true,
        routes: [
            {
                path: '/list/all',
                component: list_all
            },
            {
                path: '/list/transaction',
                component: transaction
            },
            {
                path: '/list/transactionDetail',
                component: transactionDetail
            },
            {
                path: '/list/promotionCommission',
                component: promotionCommission
            },
        ]
    },
    {
        path: '/moneyListDetail',
        component: moneyListDetail,
    },
    {
        path: '/recoverPassword',
        component: recoverPassword,
    },
    //todo 存款
    {
        path: '/recharge',
        component: recharge,
        auth: true
    },
    {
        path: '/coinPay',
        component: coinPay
    },
    {
        path: '/rechargeDpBank',
        component: rechargeDpBank
    },
    {
        path: '/rechargeXXPayDpBank',
        component: rechargeXXPayDpBank
    },
    {
        path: '/rechargeXXPayWind',
        component: rechargeXXPayWind
    },
    {
        path: '/rechargeAlipayBank',
        component: rechargeAlipayBank
    },
    {
        path: '/rechargeXXPayAlipayBank',
        component: rechargeXXPayAlipayBank
    },
    {
        path: '/rechargeWalletPay',
        component: rechargeWalletPay
    },
    {
        path: '/rechargeDora',
        component: rechargeDora
    },
    {
        path: '/rechargeWapAlipay',
        component: rechargeWapAlipay
    },
    {
        path: '/rechargeXXPayHuyun',
        component: rechargeXXPayHuyun
    },
    {
        path: '/rechargeWXPay',
        component: rechargeWXPay
    },
    {
        path: '/rechargeWX2Pay',
        component: rechargeWX2Pay
    },
    {
        path: '/rechargeXXPay',
        component: rechargeXXPay
    },
    {
        path: '/rechargeXXPayHFT',
        component: rechargeXXPayHFT
    },
    {
        path: '/rechargeXXPayQQ',
        component: rechargeXXPayQQ
    },
    {
        path: '/rechargeXXPayBaidu',
        component: rechargeXXPayBaidu
    },
    {
        path: '/rechargeXXPayQuick',
        component: rechargeXXPayQuick
    },
    {
        path: '/rechargeXXPayAlipay',
        component: rechargeXXPayAlipay
    },
    {
        path: '/rechargeXXPayDora',
        component: rechargeXXPayDora
    },
    {
        path: '/rechargeXXPayWXPay',
        component: rechargeXXPayWXPay
    },
    {
        path: '/rechargeXXPayNovaPay',
        component: rechargeXXPayNovaPay
    },
    {
        path: '/rechargeXXPayWechatPay',
        component: rechargeXXPayWechatPay
    },
    {
        path: '/rechargeXXPayWXQR',
        component: rechargeXXPayWXQR
    },
    {
        path: '/rechargeXXPayYL',
        component: rechargeXXPayYL
    },
    {
        path: '/rechargeXXPayDpBankTeching',
        component: rechargeXXPayDpBankTeching
    },
    {
        path: '/rechargeXXPayOnePointPay',
        component: rechargeXXPayOnePointPay
    },
    {
        path: '/bindOppCardList',
        component: bindOppCardList
    },
    {
        path: '/bindOppCardAdd',
        component: bindOppCardAdd
    },
    {
        path: '/rechargeXXPaySpay',
        component: rechargeXXPaySpay
    },
    {
        path: '/rechargeXXPayXD',
        component: rechargeXXPayXD
    },
    {
        path: '/rechargeXXPayCHYL',
        component: rechargeXXPayCHYL
    },
    {
        path: '/rechargeXXPayCP',
        component: rechargeXXPayCP
    },
    {
        path: '/rechargeXXPayFlashPay',
        component: rechargeXXPayFlashPay
    },
    {
        path: '/rechargeXXPayDLB',
        component: rechargeXXPayDLB
    },
    {
        path: '/rechargeXXPaySQPay',
        component: rechargeXXPaySQPay
    },
    {
        path: '/bindSQCardList',
        component: bindSQCardList
    },
    {
        path: '/bindSQCardAdd',
        component: bindSQCardAdd
    },
    {
        path: '/rechargeXXPayCPBank',
        component: rechargeXXPayCPBank
    },
    {
        path: '/rechargeXXPayRainbowWX',
        component: rechargeXXPayRainbowWX
    },
    {
        path: '/rechargeXXPayRainbowAlipay',
        component: rechargeXXPayRainbowAlipay
    },
    
    {
        path: '/records',
        component: records,
    },
    {
        path: '/withdraw',
        component: withdraw,
        routes: [
            {
                path: '/withdraw/card',
                component: card
            },
            {
                path: '/withdraw/alipay',
                component: alipay
            },
        ]
    },
    {
        path: '/history',
        component: history,
    },
    {
        path: '/recoverWithdrawPassword',
        component: recoverWithdrawPassword,
    },
]