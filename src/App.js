import React, {Component} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import RouteWithSubRoutes from './routes/routeWithSubRoutes';
import './lib/tool';
import routes from './routes';
import noMatch from './routes/404';
import './css/index.scss';
import {Contracts, Cache} from './module';
import config from './config';
import {encode} from "./lib/blog";
import {setBenchmark} from "./lib/benchmark";
import {getSearch, setCookie} from "./lib/tool";
import {nativeCallback, GetDeviceInfo} from "./lib/native";

Contracts.init();
Cache.init();
document.getElementById('imgBox').style.display = 'none';
document.addEventListener('message', nativeCallback);


export default class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <div>
                    <Switch>
                        {
                            routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)
                        }
                        <Route component={noMatch}/>
                    </Switch>
                    <section style={{display: 'none'}} id={'branch'}>
                        {encode(config.branch)}
                    </section>
                </div>
            </Router>
        );
    }

    componentDidMount() {
        let o = getSearch();
        if (o.ru) {
            setCookie('ru', o.ru, 365);
            setCookie('F-U', o.ru, 365);
        }
        if (o.f) {
            setBenchmark(o.f);
            setCookie('f', o.f, 365);
        }
        setTimeout(() => {
            if (window.isSuperman) {
                GetDeviceInfo().then((result) => {
                    if (result.uniqueId) {
                        setCookie('uuid', result.uniqueId)
                    }
                })
            }
        }, 1000);
    }
}
