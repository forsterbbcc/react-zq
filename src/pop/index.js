import React from 'react';
import ReactDOM from 'react-dom';
import ActivityRegister from './activityRegister'

const Select = (props) =>{
    switch (props.type){
        case 'register':
            return <ActivityRegister {...props}/>;
    }
};

const PopFunction = (env,activity) => {
    const holder = document.createElement('div');
    holder.className = 'popWrap';
    const body = document.createElement('div');
    body.className = 'popBody';
    holder.appendChild(body);
    holder.addEventListener('click',function () {
        close();
    });
    document.body.appendChild(holder);

    const close = () => {
        document.body.removeChild(holder)
    };
    ReactDOM.render(
        <Select type={activity}
            {...env.props}
            close={close}
        />,
        body
    )
};

export default PopFunction